/**
 * Student name: NGUYEN TRUONG LONG
 */

grammar BKOOL;

@lexer::header{
	package bkool.parser;
}

@parser::header{
	package bkool.parser;
}

options{
	language=Java;
}

program : (class_declaration)+ EOF;

class_declaration  : CLASS IDENTIFIER (EXTENDS IDENTIFIER)? LP class_body_declaration RP ;	// khai bao lop

class_body_declaration : (method_declaration | constant_declaration | variable_declaration | constructor_class | class_declaration)* ;

constructor_class : IDENTIFIER LB parameter_list? RB block_statement ;

constant_declaration : STATIC? FINAL type_declaration IDENTIFIER IS_EQUAL_TO expression SC ;	// khai bao hang

variable_declaration : STATIC? parameter SC ;	// khai bao bien

method_declaration : type_declaration STATIC? IDENTIFIER LB parameter_list? RB  block_statement  ;	// khai bao phuong thuc

parameter_list : parameter
	|	parameter_list SC parameter ;

parameter : identifier_list COLON type_declaration;

identifier_list :	IDENTIFIER
	|	identifier_list COMMA IDENTIFIER ;

type_declaration :	INT
	|	FLOAT
	|	BOOLEAN
	|	STRING
	|	VOID
	|	array
	|	class_type ;

//  ARRAY TYPE

element_type: INT | FLOAT | BOOLEAN | STRING | VOID | class_type ;

array: element_type LSB INTEGER_LITERAL RSB;

class_type: IDENTIFIER ;

//  STATEMENT
statement : assignment_statement | if_statement | for_statement | break_statement | continue_statement | return_statement | method_statement | block_statement ;	// cac lenh co ban

block_statement : LP declaration_list statement_list RP ;

declaration_list : (constant_declaration | variable_declaration )* ;

statement_list : (statement )* ;

if_statement : IF expression THEN (statement | block_statement ) (ELSE (statement | block_statement ))? ;	// cau truc lenh if

for_statement : FOR IDENTIFIER ASSIGNMENT_OPERATOR expression (TO | DOWNTO) expression DO (statement | block_statement ) ;	// cau truc lenh for

break_statement : BREAK  SC;

continue_statement : CONTINUE SC;

return_statement : RETURN expression SC;

assignment_statement : lhs ASSIGNMENT_OPERATOR expression SC ;

method_statement : expression DOT IDENTIFIER (LB (expression (COMMA expression)*)? RB)? SC ;

lhs : expression DOT IDENTIFIER
| IDENTIFIER
| expression LSB expression RSB;


// 5 EXPRESSION
expression : NEW IDENTIFIER LB (expression (COMMA expression)*)? RB					# new_expression
	|	THIS																	# self_expression
	|	expression DOT IDENTIFIER (LB (expression (COMMA expression)*)? RB)? 	# member_access
	|	expression LSB expression RSB											# index_operator
	|	<assoc=right> (ADDITION | SUBTRACTION) expression	# unary_expression
	|	<assoc=right> LOGICAL_NOT expression	# negative_expression
	|	expression CONCATENATION expression		# string_expression
	|	expression (MODULUS | INTEGER_DIVISION | FLOAT_DIVISION | MULTIPLICATION) expression	# arithmetic_expression
	|	expression (ADDITION | SUBTRACTION) expression			# binary_expression
	|	expression (LOGICAL_AND | LOGICAL_OR) expression		# logic_expression
	|	expression (EQUAL | NOT_EQUAL) expression				# condition_expression
	|	expression (LESS_THAN | GREATER_THAN | GREATER_THAN_OR_EQUAL | LESS_THAN_OR_EQUAL) expression	# relational_expression
	|	LB expression RB																				# term_expression
	|	(IDENTIFIER | INTEGER_LITERAL | FLOAT_LITERAL | BOOLEAN_LITERAL | STRING_LITERAL | NIL)		# literal_expression
	;

LINE_COMMENT : '%%' .*? (~('\n'))* -> skip ;

BLOCK_COMMENT : '/''*' (('*'('*')* ~('*'|'/'))|~('*'))* ('*'('*')*'/') -> skip ;

// KEYWORD : BOOLEAN | EXTENDS | THEN NIL | BREAK | FLOAT | FOR | THIS | CLASS | IF | RETURN | FINAL | CONTINUE | INT | BOOLEAN_LITERAL | STATIC | DO | NEW | ELSE | STRING | VOID | TO | DOWNTO ;

BOOLEAN : 'boolean';
EXTENDS : 'extends';
THEN : 'then';
NIL : 'nil';
BREAK : 'break';
FLOAT : 'float';
FOR : 'for';
THIS : 'this';
CLASS : 'class';
IF : 'if';
RETURN : 'return';
FINAL : 'final';
CONTINUE : 'continue';
INT : 'int';
BOOLEAN_LITERAL : ('true')|('false') ;
STATIC : 'static';
DO : 'do';
NEW : 'new';
ELSE: 'else';
STRING : 'string';
VOID : 'void';
TO : 'to' ;
DOWNTO : 'downto' ;

OBJECT_CREATION: 'new';

IDENTIFIER : (('a'..'z')|('A'..'Z')|'_')+(('a'..'z')|('A'..'Z')|'_'|('0'..'9'))* ;

// OPERATOR

NOT_EQUAL : '!=' ;
EQUAL : '==';
LESS_THAN_OR_EQUAL : '<=';
GREATER_THAN_OR_EQUAL : '>=';
LOGICAL_OR : '||';
LOGICAL_AND: '&&';
CONCATENATION: '^';
ADDITION : '+';
SUBTRACTION : '-';
MULTIPLICATION : '*';
FLOAT_DIVISION : '/';
INTEGER_DIVISION : '\u005C' ;
MODULUS : '%' ;
LESS_THAN : '<';
GREATER_THAN : '>';
LOGICAL_NOT: '!';
ASSIGNMENT_OPERATOR: ':=';
IS_EQUAL_TO: '=';

// SEPARATOR

LP: '{';

RP: '}';

LSB: '[';

RSB: ']';

LB: '(';

RB: ')';

SC: ';';

COLON : ':';

DOT : '.';

COMMA : ',';


INTEGER_LITERAL : ([0-9])+ ;

FLOAT_LITERAL : [0-9]+((('e'|'E')(('-'|'+')?)[0-9]+) | ('.'[0-9]*) | ('.'[0-9]*('e'|'E')(('-'|'+')?)[0-9]+)) ;

STRING_LITERAL : '"'(~('"'|'\\'|'\n')|'\\'('\\'|'b'|'f'|'r'|'n'|'t'|'"'))*'"';

WS : [ \t\r\n]+ -> skip ; // skip spaces, tabs, newlines

UNCLOSE_STRING: '"'(~('"'|'\\'|'\n')|'\\'('\\'|'b'|'f'|'r'|'n'|'t'|'"'))* ('\n'|EOF)?;

ILLEGAL_ESCAPE: '"'  (('\\'('\\'|'b'|'f'|'r'|'n'|'t'|'"'))|(~('"'|'\\'|'\n')))* ('\\'(~('\\'|'b'|'f'|'r'|'n'|'t'|'"')));

ERROR_CHAR: . ;




