// Generated from BKOOL.g4 by ANTLR 4.5.3

	package bkool.parser;

import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link BKOOLParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface BKOOLVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by {@link BKOOLParser#program}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitProgram(BKOOLParser.ProgramContext ctx);
	/**
	 * Visit a parse tree produced by {@link BKOOLParser#class_declaration}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitClass_declaration(BKOOLParser.Class_declarationContext ctx);
	/**
	 * Visit a parse tree produced by {@link BKOOLParser#class_body_declaration}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitClass_body_declaration(BKOOLParser.Class_body_declarationContext ctx);
	/**
	 * Visit a parse tree produced by {@link BKOOLParser#constructor_class}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitConstructor_class(BKOOLParser.Constructor_classContext ctx);
	/**
	 * Visit a parse tree produced by {@link BKOOLParser#constant_declaration}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitConstant_declaration(BKOOLParser.Constant_declarationContext ctx);
	/**
	 * Visit a parse tree produced by {@link BKOOLParser#variable_declaration}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVariable_declaration(BKOOLParser.Variable_declarationContext ctx);
	/**
	 * Visit a parse tree produced by {@link BKOOLParser#method_declaration}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMethod_declaration(BKOOLParser.Method_declarationContext ctx);
	/**
	 * Visit a parse tree produced by {@link BKOOLParser#parameter_list}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitParameter_list(BKOOLParser.Parameter_listContext ctx);
	/**
	 * Visit a parse tree produced by {@link BKOOLParser#parameter}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitParameter(BKOOLParser.ParameterContext ctx);
	/**
	 * Visit a parse tree produced by {@link BKOOLParser#identifier_list}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIdentifier_list(BKOOLParser.Identifier_listContext ctx);
	/**
	 * Visit a parse tree produced by {@link BKOOLParser#type_declaration}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitType_declaration(BKOOLParser.Type_declarationContext ctx);
	/**
	 * Visit a parse tree produced by {@link BKOOLParser#element_type}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitElement_type(BKOOLParser.Element_typeContext ctx);
	/**
	 * Visit a parse tree produced by {@link BKOOLParser#array}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitArray(BKOOLParser.ArrayContext ctx);
	/**
	 * Visit a parse tree produced by {@link BKOOLParser#class_type}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitClass_type(BKOOLParser.Class_typeContext ctx);
	/**
	 * Visit a parse tree produced by {@link BKOOLParser#statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStatement(BKOOLParser.StatementContext ctx);
	/**
	 * Visit a parse tree produced by {@link BKOOLParser#block_statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBlock_statement(BKOOLParser.Block_statementContext ctx);
	/**
	 * Visit a parse tree produced by {@link BKOOLParser#declaration_list}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDeclaration_list(BKOOLParser.Declaration_listContext ctx);
	/**
	 * Visit a parse tree produced by {@link BKOOLParser#statement_list}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStatement_list(BKOOLParser.Statement_listContext ctx);
	/**
	 * Visit a parse tree produced by {@link BKOOLParser#if_statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIf_statement(BKOOLParser.If_statementContext ctx);
	/**
	 * Visit a parse tree produced by {@link BKOOLParser#for_statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFor_statement(BKOOLParser.For_statementContext ctx);
	/**
	 * Visit a parse tree produced by {@link BKOOLParser#break_statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBreak_statement(BKOOLParser.Break_statementContext ctx);
	/**
	 * Visit a parse tree produced by {@link BKOOLParser#continue_statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitContinue_statement(BKOOLParser.Continue_statementContext ctx);
	/**
	 * Visit a parse tree produced by {@link BKOOLParser#return_statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitReturn_statement(BKOOLParser.Return_statementContext ctx);
	/**
	 * Visit a parse tree produced by {@link BKOOLParser#assignment_statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAssignment_statement(BKOOLParser.Assignment_statementContext ctx);
	/**
	 * Visit a parse tree produced by {@link BKOOLParser#method_statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMethod_statement(BKOOLParser.Method_statementContext ctx);
	/**
	 * Visit a parse tree produced by {@link BKOOLParser#lhs}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLhs(BKOOLParser.LhsContext ctx);
	/**
	 * Visit a parse tree produced by the {@code binary_expression}
	 * labeled alternative in {@link BKOOLParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBinary_expression(BKOOLParser.Binary_expressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code term_expression}
	 * labeled alternative in {@link BKOOLParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTerm_expression(BKOOLParser.Term_expressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code logic_expression}
	 * labeled alternative in {@link BKOOLParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLogic_expression(BKOOLParser.Logic_expressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code condition_expression}
	 * labeled alternative in {@link BKOOLParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCondition_expression(BKOOLParser.Condition_expressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code unary_expression}
	 * labeled alternative in {@link BKOOLParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitUnary_expression(BKOOLParser.Unary_expressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code index_operator}
	 * labeled alternative in {@link BKOOLParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIndex_operator(BKOOLParser.Index_operatorContext ctx);
	/**
	 * Visit a parse tree produced by the {@code self_expression}
	 * labeled alternative in {@link BKOOLParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSelf_expression(BKOOLParser.Self_expressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code string_expression}
	 * labeled alternative in {@link BKOOLParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitString_expression(BKOOLParser.String_expressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code member_access}
	 * labeled alternative in {@link BKOOLParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMember_access(BKOOLParser.Member_accessContext ctx);
	/**
	 * Visit a parse tree produced by the {@code arithmetic_expression}
	 * labeled alternative in {@link BKOOLParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitArithmetic_expression(BKOOLParser.Arithmetic_expressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code negative_expression}
	 * labeled alternative in {@link BKOOLParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNegative_expression(BKOOLParser.Negative_expressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code new_expression}
	 * labeled alternative in {@link BKOOLParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNew_expression(BKOOLParser.New_expressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code relational_expression}
	 * labeled alternative in {@link BKOOLParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRelational_expression(BKOOLParser.Relational_expressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code literal_expression}
	 * labeled alternative in {@link BKOOLParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLiteral_expression(BKOOLParser.Literal_expressionContext ctx);
}