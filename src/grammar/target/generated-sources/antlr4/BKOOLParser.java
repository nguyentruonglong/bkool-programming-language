// Generated from BKOOL.g4 by ANTLR 4.5.3

	package bkool.parser;

import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class BKOOLParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.5.3", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		LINE_COMMENT=1, BLOCK_COMMENT=2, BOOLEAN=3, EXTENDS=4, THEN=5, NIL=6, 
		BREAK=7, FLOAT=8, FOR=9, THIS=10, CLASS=11, IF=12, RETURN=13, FINAL=14, 
		CONTINUE=15, INT=16, BOOLEAN_LITERAL=17, STATIC=18, DO=19, NEW=20, ELSE=21, 
		STRING=22, VOID=23, TO=24, DOWNTO=25, OBJECT_CREATION=26, IDENTIFIER=27, 
		NOT_EQUAL=28, EQUAL=29, LESS_THAN_OR_EQUAL=30, GREATER_THAN_OR_EQUAL=31, 
		LOGICAL_OR=32, LOGICAL_AND=33, CONCATENATION=34, ADDITION=35, SUBTRACTION=36, 
		MULTIPLICATION=37, FLOAT_DIVISION=38, INTEGER_DIVISION=39, MODULUS=40, 
		LESS_THAN=41, GREATER_THAN=42, LOGICAL_NOT=43, ASSIGNMENT_OPERATOR=44, 
		IS_EQUAL_TO=45, LP=46, RP=47, LSB=48, RSB=49, LB=50, RB=51, SC=52, COLON=53, 
		DOT=54, COMMA=55, INTEGER_LITERAL=56, FLOAT_LITERAL=57, STRING_LITERAL=58, 
		WS=59, UNCLOSE_STRING=60, ILLEGAL_ESCAPE=61, ERROR_CHAR=62;
	public static final int
		RULE_program = 0, RULE_class_declaration = 1, RULE_class_body_declaration = 2, 
		RULE_constructor_class = 3, RULE_constant_declaration = 4, RULE_variable_declaration = 5, 
		RULE_method_declaration = 6, RULE_parameter_list = 7, RULE_parameter = 8, 
		RULE_identifier_list = 9, RULE_type_declaration = 10, RULE_element_type = 11, 
		RULE_array = 12, RULE_class_type = 13, RULE_statement = 14, RULE_block_statement = 15, 
		RULE_declaration_list = 16, RULE_statement_list = 17, RULE_if_statement = 18, 
		RULE_for_statement = 19, RULE_break_statement = 20, RULE_continue_statement = 21, 
		RULE_return_statement = 22, RULE_assignment_statement = 23, RULE_method_statement = 24, 
		RULE_lhs = 25, RULE_expression = 26;
	public static final String[] ruleNames = {
		"program", "class_declaration", "class_body_declaration", "constructor_class", 
		"constant_declaration", "variable_declaration", "method_declaration", 
		"parameter_list", "parameter", "identifier_list", "type_declaration", 
		"element_type", "array", "class_type", "statement", "block_statement", 
		"declaration_list", "statement_list", "if_statement", "for_statement", 
		"break_statement", "continue_statement", "return_statement", "assignment_statement", 
		"method_statement", "lhs", "expression"
	};

	private static final String[] _LITERAL_NAMES = {
		null, null, null, "'boolean'", "'extends'", "'then'", "'nil'", "'break'", 
		"'float'", "'for'", "'this'", "'class'", "'if'", "'return'", "'final'", 
		"'continue'", "'int'", null, "'static'", "'do'", null, "'else'", "'string'", 
		"'void'", "'to'", "'downto'", null, null, "'!='", "'=='", "'<='", "'>='", 
		"'||'", "'&&'", "'^'", "'+'", "'-'", "'*'", "'/'", "'\\u005C'", "'%'", 
		"'<'", "'>'", "'!'", "':='", "'='", "'{'", "'}'", "'['", "']'", "'('", 
		"')'", "';'", "':'", "'.'", "','"
	};
	private static final String[] _SYMBOLIC_NAMES = {
		null, "LINE_COMMENT", "BLOCK_COMMENT", "BOOLEAN", "EXTENDS", "THEN", "NIL", 
		"BREAK", "FLOAT", "FOR", "THIS", "CLASS", "IF", "RETURN", "FINAL", "CONTINUE", 
		"INT", "BOOLEAN_LITERAL", "STATIC", "DO", "NEW", "ELSE", "STRING", "VOID", 
		"TO", "DOWNTO", "OBJECT_CREATION", "IDENTIFIER", "NOT_EQUAL", "EQUAL", 
		"LESS_THAN_OR_EQUAL", "GREATER_THAN_OR_EQUAL", "LOGICAL_OR", "LOGICAL_AND", 
		"CONCATENATION", "ADDITION", "SUBTRACTION", "MULTIPLICATION", "FLOAT_DIVISION", 
		"INTEGER_DIVISION", "MODULUS", "LESS_THAN", "GREATER_THAN", "LOGICAL_NOT", 
		"ASSIGNMENT_OPERATOR", "IS_EQUAL_TO", "LP", "RP", "LSB", "RSB", "LB", 
		"RB", "SC", "COLON", "DOT", "COMMA", "INTEGER_LITERAL", "FLOAT_LITERAL", 
		"STRING_LITERAL", "WS", "UNCLOSE_STRING", "ILLEGAL_ESCAPE", "ERROR_CHAR"
	};
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "BKOOL.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public BKOOLParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}
	public static class ProgramContext extends ParserRuleContext {
		public TerminalNode EOF() { return getToken(BKOOLParser.EOF, 0); }
		public List<Class_declarationContext> class_declaration() {
			return getRuleContexts(Class_declarationContext.class);
		}
		public Class_declarationContext class_declaration(int i) {
			return getRuleContext(Class_declarationContext.class,i);
		}
		public ProgramContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_program; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BKOOLVisitor ) return ((BKOOLVisitor<? extends T>)visitor).visitProgram(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ProgramContext program() throws RecognitionException {
		ProgramContext _localctx = new ProgramContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_program);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(55); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				{
				setState(54);
				class_declaration();
				}
				}
				setState(57); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( _la==CLASS );
			setState(59);
			match(EOF);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Class_declarationContext extends ParserRuleContext {
		public TerminalNode CLASS() { return getToken(BKOOLParser.CLASS, 0); }
		public List<TerminalNode> IDENTIFIER() { return getTokens(BKOOLParser.IDENTIFIER); }
		public TerminalNode IDENTIFIER(int i) {
			return getToken(BKOOLParser.IDENTIFIER, i);
		}
		public TerminalNode LP() { return getToken(BKOOLParser.LP, 0); }
		public Class_body_declarationContext class_body_declaration() {
			return getRuleContext(Class_body_declarationContext.class,0);
		}
		public TerminalNode RP() { return getToken(BKOOLParser.RP, 0); }
		public TerminalNode EXTENDS() { return getToken(BKOOLParser.EXTENDS, 0); }
		public Class_declarationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_class_declaration; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BKOOLVisitor ) return ((BKOOLVisitor<? extends T>)visitor).visitClass_declaration(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Class_declarationContext class_declaration() throws RecognitionException {
		Class_declarationContext _localctx = new Class_declarationContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_class_declaration);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(61);
			match(CLASS);
			setState(62);
			match(IDENTIFIER);
			setState(65);
			_la = _input.LA(1);
			if (_la==EXTENDS) {
				{
				setState(63);
				match(EXTENDS);
				setState(64);
				match(IDENTIFIER);
				}
			}

			setState(67);
			match(LP);
			setState(68);
			class_body_declaration();
			setState(69);
			match(RP);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Class_body_declarationContext extends ParserRuleContext {
		public List<Method_declarationContext> method_declaration() {
			return getRuleContexts(Method_declarationContext.class);
		}
		public Method_declarationContext method_declaration(int i) {
			return getRuleContext(Method_declarationContext.class,i);
		}
		public List<Constant_declarationContext> constant_declaration() {
			return getRuleContexts(Constant_declarationContext.class);
		}
		public Constant_declarationContext constant_declaration(int i) {
			return getRuleContext(Constant_declarationContext.class,i);
		}
		public List<Variable_declarationContext> variable_declaration() {
			return getRuleContexts(Variable_declarationContext.class);
		}
		public Variable_declarationContext variable_declaration(int i) {
			return getRuleContext(Variable_declarationContext.class,i);
		}
		public List<Constructor_classContext> constructor_class() {
			return getRuleContexts(Constructor_classContext.class);
		}
		public Constructor_classContext constructor_class(int i) {
			return getRuleContext(Constructor_classContext.class,i);
		}
		public List<Class_declarationContext> class_declaration() {
			return getRuleContexts(Class_declarationContext.class);
		}
		public Class_declarationContext class_declaration(int i) {
			return getRuleContext(Class_declarationContext.class,i);
		}
		public Class_body_declarationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_class_body_declaration; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BKOOLVisitor ) return ((BKOOLVisitor<? extends T>)visitor).visitClass_body_declaration(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Class_body_declarationContext class_body_declaration() throws RecognitionException {
		Class_body_declarationContext _localctx = new Class_body_declarationContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_class_body_declaration);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(78);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << BOOLEAN) | (1L << FLOAT) | (1L << CLASS) | (1L << FINAL) | (1L << INT) | (1L << STATIC) | (1L << STRING) | (1L << VOID) | (1L << IDENTIFIER))) != 0)) {
				{
				setState(76);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,2,_ctx) ) {
				case 1:
					{
					setState(71);
					method_declaration();
					}
					break;
				case 2:
					{
					setState(72);
					constant_declaration();
					}
					break;
				case 3:
					{
					setState(73);
					variable_declaration();
					}
					break;
				case 4:
					{
					setState(74);
					constructor_class();
					}
					break;
				case 5:
					{
					setState(75);
					class_declaration();
					}
					break;
				}
				}
				setState(80);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Constructor_classContext extends ParserRuleContext {
		public TerminalNode IDENTIFIER() { return getToken(BKOOLParser.IDENTIFIER, 0); }
		public TerminalNode LB() { return getToken(BKOOLParser.LB, 0); }
		public TerminalNode RB() { return getToken(BKOOLParser.RB, 0); }
		public Block_statementContext block_statement() {
			return getRuleContext(Block_statementContext.class,0);
		}
		public Parameter_listContext parameter_list() {
			return getRuleContext(Parameter_listContext.class,0);
		}
		public Constructor_classContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_constructor_class; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BKOOLVisitor ) return ((BKOOLVisitor<? extends T>)visitor).visitConstructor_class(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Constructor_classContext constructor_class() throws RecognitionException {
		Constructor_classContext _localctx = new Constructor_classContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_constructor_class);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(81);
			match(IDENTIFIER);
			setState(82);
			match(LB);
			setState(84);
			_la = _input.LA(1);
			if (_la==IDENTIFIER) {
				{
				setState(83);
				parameter_list(0);
				}
			}

			setState(86);
			match(RB);
			setState(87);
			block_statement();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Constant_declarationContext extends ParserRuleContext {
		public TerminalNode FINAL() { return getToken(BKOOLParser.FINAL, 0); }
		public Type_declarationContext type_declaration() {
			return getRuleContext(Type_declarationContext.class,0);
		}
		public TerminalNode IDENTIFIER() { return getToken(BKOOLParser.IDENTIFIER, 0); }
		public TerminalNode IS_EQUAL_TO() { return getToken(BKOOLParser.IS_EQUAL_TO, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public TerminalNode SC() { return getToken(BKOOLParser.SC, 0); }
		public TerminalNode STATIC() { return getToken(BKOOLParser.STATIC, 0); }
		public Constant_declarationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_constant_declaration; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BKOOLVisitor ) return ((BKOOLVisitor<? extends T>)visitor).visitConstant_declaration(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Constant_declarationContext constant_declaration() throws RecognitionException {
		Constant_declarationContext _localctx = new Constant_declarationContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_constant_declaration);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(90);
			_la = _input.LA(1);
			if (_la==STATIC) {
				{
				setState(89);
				match(STATIC);
				}
			}

			setState(92);
			match(FINAL);
			setState(93);
			type_declaration();
			setState(94);
			match(IDENTIFIER);
			setState(95);
			match(IS_EQUAL_TO);
			setState(96);
			expression(0);
			setState(97);
			match(SC);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Variable_declarationContext extends ParserRuleContext {
		public ParameterContext parameter() {
			return getRuleContext(ParameterContext.class,0);
		}
		public TerminalNode SC() { return getToken(BKOOLParser.SC, 0); }
		public TerminalNode STATIC() { return getToken(BKOOLParser.STATIC, 0); }
		public Variable_declarationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_variable_declaration; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BKOOLVisitor ) return ((BKOOLVisitor<? extends T>)visitor).visitVariable_declaration(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Variable_declarationContext variable_declaration() throws RecognitionException {
		Variable_declarationContext _localctx = new Variable_declarationContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_variable_declaration);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(100);
			_la = _input.LA(1);
			if (_la==STATIC) {
				{
				setState(99);
				match(STATIC);
				}
			}

			setState(102);
			parameter();
			setState(103);
			match(SC);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Method_declarationContext extends ParserRuleContext {
		public Type_declarationContext type_declaration() {
			return getRuleContext(Type_declarationContext.class,0);
		}
		public TerminalNode IDENTIFIER() { return getToken(BKOOLParser.IDENTIFIER, 0); }
		public TerminalNode LB() { return getToken(BKOOLParser.LB, 0); }
		public TerminalNode RB() { return getToken(BKOOLParser.RB, 0); }
		public Block_statementContext block_statement() {
			return getRuleContext(Block_statementContext.class,0);
		}
		public TerminalNode STATIC() { return getToken(BKOOLParser.STATIC, 0); }
		public Parameter_listContext parameter_list() {
			return getRuleContext(Parameter_listContext.class,0);
		}
		public Method_declarationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_method_declaration; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BKOOLVisitor ) return ((BKOOLVisitor<? extends T>)visitor).visitMethod_declaration(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Method_declarationContext method_declaration() throws RecognitionException {
		Method_declarationContext _localctx = new Method_declarationContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_method_declaration);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(105);
			type_declaration();
			setState(107);
			_la = _input.LA(1);
			if (_la==STATIC) {
				{
				setState(106);
				match(STATIC);
				}
			}

			setState(109);
			match(IDENTIFIER);
			setState(110);
			match(LB);
			setState(112);
			_la = _input.LA(1);
			if (_la==IDENTIFIER) {
				{
				setState(111);
				parameter_list(0);
				}
			}

			setState(114);
			match(RB);
			setState(115);
			block_statement();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Parameter_listContext extends ParserRuleContext {
		public ParameterContext parameter() {
			return getRuleContext(ParameterContext.class,0);
		}
		public Parameter_listContext parameter_list() {
			return getRuleContext(Parameter_listContext.class,0);
		}
		public TerminalNode SC() { return getToken(BKOOLParser.SC, 0); }
		public Parameter_listContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_parameter_list; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BKOOLVisitor ) return ((BKOOLVisitor<? extends T>)visitor).visitParameter_list(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Parameter_listContext parameter_list() throws RecognitionException {
		return parameter_list(0);
	}

	private Parameter_listContext parameter_list(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		Parameter_listContext _localctx = new Parameter_listContext(_ctx, _parentState);
		Parameter_listContext _prevctx = _localctx;
		int _startState = 14;
		enterRecursionRule(_localctx, 14, RULE_parameter_list, _p);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			{
			setState(118);
			parameter();
			}
			_ctx.stop = _input.LT(-1);
			setState(125);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,9,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					{
					_localctx = new Parameter_listContext(_parentctx, _parentState);
					pushNewRecursionContext(_localctx, _startState, RULE_parameter_list);
					setState(120);
					if (!(precpred(_ctx, 1))) throw new FailedPredicateException(this, "precpred(_ctx, 1)");
					setState(121);
					match(SC);
					setState(122);
					parameter();
					}
					} 
				}
				setState(127);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,9,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class ParameterContext extends ParserRuleContext {
		public Identifier_listContext identifier_list() {
			return getRuleContext(Identifier_listContext.class,0);
		}
		public TerminalNode COLON() { return getToken(BKOOLParser.COLON, 0); }
		public Type_declarationContext type_declaration() {
			return getRuleContext(Type_declarationContext.class,0);
		}
		public ParameterContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_parameter; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BKOOLVisitor ) return ((BKOOLVisitor<? extends T>)visitor).visitParameter(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ParameterContext parameter() throws RecognitionException {
		ParameterContext _localctx = new ParameterContext(_ctx, getState());
		enterRule(_localctx, 16, RULE_parameter);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(128);
			identifier_list(0);
			setState(129);
			match(COLON);
			setState(130);
			type_declaration();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Identifier_listContext extends ParserRuleContext {
		public TerminalNode IDENTIFIER() { return getToken(BKOOLParser.IDENTIFIER, 0); }
		public Identifier_listContext identifier_list() {
			return getRuleContext(Identifier_listContext.class,0);
		}
		public TerminalNode COMMA() { return getToken(BKOOLParser.COMMA, 0); }
		public Identifier_listContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_identifier_list; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BKOOLVisitor ) return ((BKOOLVisitor<? extends T>)visitor).visitIdentifier_list(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Identifier_listContext identifier_list() throws RecognitionException {
		return identifier_list(0);
	}

	private Identifier_listContext identifier_list(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		Identifier_listContext _localctx = new Identifier_listContext(_ctx, _parentState);
		Identifier_listContext _prevctx = _localctx;
		int _startState = 18;
		enterRecursionRule(_localctx, 18, RULE_identifier_list, _p);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			{
			setState(133);
			match(IDENTIFIER);
			}
			_ctx.stop = _input.LT(-1);
			setState(140);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,10,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					{
					_localctx = new Identifier_listContext(_parentctx, _parentState);
					pushNewRecursionContext(_localctx, _startState, RULE_identifier_list);
					setState(135);
					if (!(precpred(_ctx, 1))) throw new FailedPredicateException(this, "precpred(_ctx, 1)");
					setState(136);
					match(COMMA);
					setState(137);
					match(IDENTIFIER);
					}
					} 
				}
				setState(142);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,10,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class Type_declarationContext extends ParserRuleContext {
		public TerminalNode INT() { return getToken(BKOOLParser.INT, 0); }
		public TerminalNode FLOAT() { return getToken(BKOOLParser.FLOAT, 0); }
		public TerminalNode BOOLEAN() { return getToken(BKOOLParser.BOOLEAN, 0); }
		public TerminalNode STRING() { return getToken(BKOOLParser.STRING, 0); }
		public TerminalNode VOID() { return getToken(BKOOLParser.VOID, 0); }
		public ArrayContext array() {
			return getRuleContext(ArrayContext.class,0);
		}
		public Class_typeContext class_type() {
			return getRuleContext(Class_typeContext.class,0);
		}
		public Type_declarationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_type_declaration; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BKOOLVisitor ) return ((BKOOLVisitor<? extends T>)visitor).visitType_declaration(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Type_declarationContext type_declaration() throws RecognitionException {
		Type_declarationContext _localctx = new Type_declarationContext(_ctx, getState());
		enterRule(_localctx, 20, RULE_type_declaration);
		try {
			setState(150);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,11,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(143);
				match(INT);
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(144);
				match(FLOAT);
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(145);
				match(BOOLEAN);
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(146);
				match(STRING);
				}
				break;
			case 5:
				enterOuterAlt(_localctx, 5);
				{
				setState(147);
				match(VOID);
				}
				break;
			case 6:
				enterOuterAlt(_localctx, 6);
				{
				setState(148);
				array();
				}
				break;
			case 7:
				enterOuterAlt(_localctx, 7);
				{
				setState(149);
				class_type();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Element_typeContext extends ParserRuleContext {
		public TerminalNode INT() { return getToken(BKOOLParser.INT, 0); }
		public TerminalNode FLOAT() { return getToken(BKOOLParser.FLOAT, 0); }
		public TerminalNode BOOLEAN() { return getToken(BKOOLParser.BOOLEAN, 0); }
		public TerminalNode STRING() { return getToken(BKOOLParser.STRING, 0); }
		public TerminalNode VOID() { return getToken(BKOOLParser.VOID, 0); }
		public Class_typeContext class_type() {
			return getRuleContext(Class_typeContext.class,0);
		}
		public Element_typeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_element_type; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BKOOLVisitor ) return ((BKOOLVisitor<? extends T>)visitor).visitElement_type(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Element_typeContext element_type() throws RecognitionException {
		Element_typeContext _localctx = new Element_typeContext(_ctx, getState());
		enterRule(_localctx, 22, RULE_element_type);
		try {
			setState(158);
			switch (_input.LA(1)) {
			case INT:
				enterOuterAlt(_localctx, 1);
				{
				setState(152);
				match(INT);
				}
				break;
			case FLOAT:
				enterOuterAlt(_localctx, 2);
				{
				setState(153);
				match(FLOAT);
				}
				break;
			case BOOLEAN:
				enterOuterAlt(_localctx, 3);
				{
				setState(154);
				match(BOOLEAN);
				}
				break;
			case STRING:
				enterOuterAlt(_localctx, 4);
				{
				setState(155);
				match(STRING);
				}
				break;
			case VOID:
				enterOuterAlt(_localctx, 5);
				{
				setState(156);
				match(VOID);
				}
				break;
			case IDENTIFIER:
				enterOuterAlt(_localctx, 6);
				{
				setState(157);
				class_type();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ArrayContext extends ParserRuleContext {
		public Element_typeContext element_type() {
			return getRuleContext(Element_typeContext.class,0);
		}
		public TerminalNode LSB() { return getToken(BKOOLParser.LSB, 0); }
		public TerminalNode INTEGER_LITERAL() { return getToken(BKOOLParser.INTEGER_LITERAL, 0); }
		public TerminalNode RSB() { return getToken(BKOOLParser.RSB, 0); }
		public ArrayContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_array; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BKOOLVisitor ) return ((BKOOLVisitor<? extends T>)visitor).visitArray(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ArrayContext array() throws RecognitionException {
		ArrayContext _localctx = new ArrayContext(_ctx, getState());
		enterRule(_localctx, 24, RULE_array);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(160);
			element_type();
			setState(161);
			match(LSB);
			setState(162);
			match(INTEGER_LITERAL);
			setState(163);
			match(RSB);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Class_typeContext extends ParserRuleContext {
		public TerminalNode IDENTIFIER() { return getToken(BKOOLParser.IDENTIFIER, 0); }
		public Class_typeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_class_type; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BKOOLVisitor ) return ((BKOOLVisitor<? extends T>)visitor).visitClass_type(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Class_typeContext class_type() throws RecognitionException {
		Class_typeContext _localctx = new Class_typeContext(_ctx, getState());
		enterRule(_localctx, 26, RULE_class_type);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(165);
			match(IDENTIFIER);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class StatementContext extends ParserRuleContext {
		public Assignment_statementContext assignment_statement() {
			return getRuleContext(Assignment_statementContext.class,0);
		}
		public If_statementContext if_statement() {
			return getRuleContext(If_statementContext.class,0);
		}
		public For_statementContext for_statement() {
			return getRuleContext(For_statementContext.class,0);
		}
		public Break_statementContext break_statement() {
			return getRuleContext(Break_statementContext.class,0);
		}
		public Continue_statementContext continue_statement() {
			return getRuleContext(Continue_statementContext.class,0);
		}
		public Return_statementContext return_statement() {
			return getRuleContext(Return_statementContext.class,0);
		}
		public Method_statementContext method_statement() {
			return getRuleContext(Method_statementContext.class,0);
		}
		public Block_statementContext block_statement() {
			return getRuleContext(Block_statementContext.class,0);
		}
		public StatementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_statement; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BKOOLVisitor ) return ((BKOOLVisitor<? extends T>)visitor).visitStatement(this);
			else return visitor.visitChildren(this);
		}
	}

	public final StatementContext statement() throws RecognitionException {
		StatementContext _localctx = new StatementContext(_ctx, getState());
		enterRule(_localctx, 28, RULE_statement);
		try {
			setState(175);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,13,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(167);
				assignment_statement();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(168);
				if_statement();
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(169);
				for_statement();
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(170);
				break_statement();
				}
				break;
			case 5:
				enterOuterAlt(_localctx, 5);
				{
				setState(171);
				continue_statement();
				}
				break;
			case 6:
				enterOuterAlt(_localctx, 6);
				{
				setState(172);
				return_statement();
				}
				break;
			case 7:
				enterOuterAlt(_localctx, 7);
				{
				setState(173);
				method_statement();
				}
				break;
			case 8:
				enterOuterAlt(_localctx, 8);
				{
				setState(174);
				block_statement();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Block_statementContext extends ParserRuleContext {
		public TerminalNode LP() { return getToken(BKOOLParser.LP, 0); }
		public Declaration_listContext declaration_list() {
			return getRuleContext(Declaration_listContext.class,0);
		}
		public Statement_listContext statement_list() {
			return getRuleContext(Statement_listContext.class,0);
		}
		public TerminalNode RP() { return getToken(BKOOLParser.RP, 0); }
		public Block_statementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_block_statement; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BKOOLVisitor ) return ((BKOOLVisitor<? extends T>)visitor).visitBlock_statement(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Block_statementContext block_statement() throws RecognitionException {
		Block_statementContext _localctx = new Block_statementContext(_ctx, getState());
		enterRule(_localctx, 30, RULE_block_statement);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(177);
			match(LP);
			setState(178);
			declaration_list();
			setState(179);
			statement_list();
			setState(180);
			match(RP);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Declaration_listContext extends ParserRuleContext {
		public List<Constant_declarationContext> constant_declaration() {
			return getRuleContexts(Constant_declarationContext.class);
		}
		public Constant_declarationContext constant_declaration(int i) {
			return getRuleContext(Constant_declarationContext.class,i);
		}
		public List<Variable_declarationContext> variable_declaration() {
			return getRuleContexts(Variable_declarationContext.class);
		}
		public Variable_declarationContext variable_declaration(int i) {
			return getRuleContext(Variable_declarationContext.class,i);
		}
		public Declaration_listContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_declaration_list; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BKOOLVisitor ) return ((BKOOLVisitor<? extends T>)visitor).visitDeclaration_list(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Declaration_listContext declaration_list() throws RecognitionException {
		Declaration_listContext _localctx = new Declaration_listContext(_ctx, getState());
		enterRule(_localctx, 32, RULE_declaration_list);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(186);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,15,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					setState(184);
					_errHandler.sync(this);
					switch ( getInterpreter().adaptivePredict(_input,14,_ctx) ) {
					case 1:
						{
						setState(182);
						constant_declaration();
						}
						break;
					case 2:
						{
						setState(183);
						variable_declaration();
						}
						break;
					}
					} 
				}
				setState(188);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,15,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Statement_listContext extends ParserRuleContext {
		public List<StatementContext> statement() {
			return getRuleContexts(StatementContext.class);
		}
		public StatementContext statement(int i) {
			return getRuleContext(StatementContext.class,i);
		}
		public Statement_listContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_statement_list; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BKOOLVisitor ) return ((BKOOLVisitor<? extends T>)visitor).visitStatement_list(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Statement_listContext statement_list() throws RecognitionException {
		Statement_listContext _localctx = new Statement_listContext(_ctx, getState());
		enterRule(_localctx, 34, RULE_statement_list);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(192);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << NIL) | (1L << BREAK) | (1L << FOR) | (1L << THIS) | (1L << IF) | (1L << RETURN) | (1L << CONTINUE) | (1L << BOOLEAN_LITERAL) | (1L << NEW) | (1L << IDENTIFIER) | (1L << ADDITION) | (1L << SUBTRACTION) | (1L << LOGICAL_NOT) | (1L << LP) | (1L << LB) | (1L << INTEGER_LITERAL) | (1L << FLOAT_LITERAL) | (1L << STRING_LITERAL))) != 0)) {
				{
				{
				setState(189);
				statement();
				}
				}
				setState(194);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class If_statementContext extends ParserRuleContext {
		public TerminalNode IF() { return getToken(BKOOLParser.IF, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public TerminalNode THEN() { return getToken(BKOOLParser.THEN, 0); }
		public List<StatementContext> statement() {
			return getRuleContexts(StatementContext.class);
		}
		public StatementContext statement(int i) {
			return getRuleContext(StatementContext.class,i);
		}
		public List<Block_statementContext> block_statement() {
			return getRuleContexts(Block_statementContext.class);
		}
		public Block_statementContext block_statement(int i) {
			return getRuleContext(Block_statementContext.class,i);
		}
		public TerminalNode ELSE() { return getToken(BKOOLParser.ELSE, 0); }
		public If_statementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_if_statement; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BKOOLVisitor ) return ((BKOOLVisitor<? extends T>)visitor).visitIf_statement(this);
			else return visitor.visitChildren(this);
		}
	}

	public final If_statementContext if_statement() throws RecognitionException {
		If_statementContext _localctx = new If_statementContext(_ctx, getState());
		enterRule(_localctx, 36, RULE_if_statement);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(195);
			match(IF);
			setState(196);
			expression(0);
			setState(197);
			match(THEN);
			setState(200);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,17,_ctx) ) {
			case 1:
				{
				setState(198);
				statement();
				}
				break;
			case 2:
				{
				setState(199);
				block_statement();
				}
				break;
			}
			setState(207);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,19,_ctx) ) {
			case 1:
				{
				setState(202);
				match(ELSE);
				setState(205);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,18,_ctx) ) {
				case 1:
					{
					setState(203);
					statement();
					}
					break;
				case 2:
					{
					setState(204);
					block_statement();
					}
					break;
				}
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class For_statementContext extends ParserRuleContext {
		public TerminalNode FOR() { return getToken(BKOOLParser.FOR, 0); }
		public TerminalNode IDENTIFIER() { return getToken(BKOOLParser.IDENTIFIER, 0); }
		public TerminalNode ASSIGNMENT_OPERATOR() { return getToken(BKOOLParser.ASSIGNMENT_OPERATOR, 0); }
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public TerminalNode DO() { return getToken(BKOOLParser.DO, 0); }
		public TerminalNode TO() { return getToken(BKOOLParser.TO, 0); }
		public TerminalNode DOWNTO() { return getToken(BKOOLParser.DOWNTO, 0); }
		public StatementContext statement() {
			return getRuleContext(StatementContext.class,0);
		}
		public Block_statementContext block_statement() {
			return getRuleContext(Block_statementContext.class,0);
		}
		public For_statementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_for_statement; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BKOOLVisitor ) return ((BKOOLVisitor<? extends T>)visitor).visitFor_statement(this);
			else return visitor.visitChildren(this);
		}
	}

	public final For_statementContext for_statement() throws RecognitionException {
		For_statementContext _localctx = new For_statementContext(_ctx, getState());
		enterRule(_localctx, 38, RULE_for_statement);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(209);
			match(FOR);
			setState(210);
			match(IDENTIFIER);
			setState(211);
			match(ASSIGNMENT_OPERATOR);
			setState(212);
			expression(0);
			setState(213);
			_la = _input.LA(1);
			if ( !(_la==TO || _la==DOWNTO) ) {
			_errHandler.recoverInline(this);
			} else {
				consume();
			}
			setState(214);
			expression(0);
			setState(215);
			match(DO);
			setState(218);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,20,_ctx) ) {
			case 1:
				{
				setState(216);
				statement();
				}
				break;
			case 2:
				{
				setState(217);
				block_statement();
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Break_statementContext extends ParserRuleContext {
		public TerminalNode BREAK() { return getToken(BKOOLParser.BREAK, 0); }
		public TerminalNode SC() { return getToken(BKOOLParser.SC, 0); }
		public Break_statementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_break_statement; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BKOOLVisitor ) return ((BKOOLVisitor<? extends T>)visitor).visitBreak_statement(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Break_statementContext break_statement() throws RecognitionException {
		Break_statementContext _localctx = new Break_statementContext(_ctx, getState());
		enterRule(_localctx, 40, RULE_break_statement);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(220);
			match(BREAK);
			setState(221);
			match(SC);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Continue_statementContext extends ParserRuleContext {
		public TerminalNode CONTINUE() { return getToken(BKOOLParser.CONTINUE, 0); }
		public TerminalNode SC() { return getToken(BKOOLParser.SC, 0); }
		public Continue_statementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_continue_statement; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BKOOLVisitor ) return ((BKOOLVisitor<? extends T>)visitor).visitContinue_statement(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Continue_statementContext continue_statement() throws RecognitionException {
		Continue_statementContext _localctx = new Continue_statementContext(_ctx, getState());
		enterRule(_localctx, 42, RULE_continue_statement);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(223);
			match(CONTINUE);
			setState(224);
			match(SC);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Return_statementContext extends ParserRuleContext {
		public TerminalNode RETURN() { return getToken(BKOOLParser.RETURN, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public TerminalNode SC() { return getToken(BKOOLParser.SC, 0); }
		public Return_statementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_return_statement; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BKOOLVisitor ) return ((BKOOLVisitor<? extends T>)visitor).visitReturn_statement(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Return_statementContext return_statement() throws RecognitionException {
		Return_statementContext _localctx = new Return_statementContext(_ctx, getState());
		enterRule(_localctx, 44, RULE_return_statement);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(226);
			match(RETURN);
			setState(227);
			expression(0);
			setState(228);
			match(SC);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Assignment_statementContext extends ParserRuleContext {
		public LhsContext lhs() {
			return getRuleContext(LhsContext.class,0);
		}
		public TerminalNode ASSIGNMENT_OPERATOR() { return getToken(BKOOLParser.ASSIGNMENT_OPERATOR, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public TerminalNode SC() { return getToken(BKOOLParser.SC, 0); }
		public Assignment_statementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_assignment_statement; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BKOOLVisitor ) return ((BKOOLVisitor<? extends T>)visitor).visitAssignment_statement(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Assignment_statementContext assignment_statement() throws RecognitionException {
		Assignment_statementContext _localctx = new Assignment_statementContext(_ctx, getState());
		enterRule(_localctx, 46, RULE_assignment_statement);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(230);
			lhs();
			setState(231);
			match(ASSIGNMENT_OPERATOR);
			setState(232);
			expression(0);
			setState(233);
			match(SC);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Method_statementContext extends ParserRuleContext {
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public TerminalNode DOT() { return getToken(BKOOLParser.DOT, 0); }
		public TerminalNode IDENTIFIER() { return getToken(BKOOLParser.IDENTIFIER, 0); }
		public TerminalNode SC() { return getToken(BKOOLParser.SC, 0); }
		public TerminalNode LB() { return getToken(BKOOLParser.LB, 0); }
		public TerminalNode RB() { return getToken(BKOOLParser.RB, 0); }
		public List<TerminalNode> COMMA() { return getTokens(BKOOLParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(BKOOLParser.COMMA, i);
		}
		public Method_statementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_method_statement; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BKOOLVisitor ) return ((BKOOLVisitor<? extends T>)visitor).visitMethod_statement(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Method_statementContext method_statement() throws RecognitionException {
		Method_statementContext _localctx = new Method_statementContext(_ctx, getState());
		enterRule(_localctx, 48, RULE_method_statement);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(235);
			expression(0);
			setState(236);
			match(DOT);
			setState(237);
			match(IDENTIFIER);
			setState(250);
			_la = _input.LA(1);
			if (_la==LB) {
				{
				setState(238);
				match(LB);
				setState(247);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << NIL) | (1L << THIS) | (1L << BOOLEAN_LITERAL) | (1L << NEW) | (1L << IDENTIFIER) | (1L << ADDITION) | (1L << SUBTRACTION) | (1L << LOGICAL_NOT) | (1L << LB) | (1L << INTEGER_LITERAL) | (1L << FLOAT_LITERAL) | (1L << STRING_LITERAL))) != 0)) {
					{
					setState(239);
					expression(0);
					setState(244);
					_errHandler.sync(this);
					_la = _input.LA(1);
					while (_la==COMMA) {
						{
						{
						setState(240);
						match(COMMA);
						setState(241);
						expression(0);
						}
						}
						setState(246);
						_errHandler.sync(this);
						_la = _input.LA(1);
					}
					}
				}

				setState(249);
				match(RB);
				}
			}

			setState(252);
			match(SC);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class LhsContext extends ParserRuleContext {
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public TerminalNode DOT() { return getToken(BKOOLParser.DOT, 0); }
		public TerminalNode IDENTIFIER() { return getToken(BKOOLParser.IDENTIFIER, 0); }
		public TerminalNode LSB() { return getToken(BKOOLParser.LSB, 0); }
		public TerminalNode RSB() { return getToken(BKOOLParser.RSB, 0); }
		public LhsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_lhs; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BKOOLVisitor ) return ((BKOOLVisitor<? extends T>)visitor).visitLhs(this);
			else return visitor.visitChildren(this);
		}
	}

	public final LhsContext lhs() throws RecognitionException {
		LhsContext _localctx = new LhsContext(_ctx, getState());
		enterRule(_localctx, 50, RULE_lhs);
		try {
			setState(264);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,24,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(254);
				expression(0);
				setState(255);
				match(DOT);
				setState(256);
				match(IDENTIFIER);
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(258);
				match(IDENTIFIER);
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(259);
				expression(0);
				setState(260);
				match(LSB);
				setState(261);
				expression(0);
				setState(262);
				match(RSB);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ExpressionContext extends ParserRuleContext {
		public ExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_expression; }
	 
		public ExpressionContext() { }
		public void copyFrom(ExpressionContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class Binary_expressionContext extends ExpressionContext {
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public TerminalNode ADDITION() { return getToken(BKOOLParser.ADDITION, 0); }
		public TerminalNode SUBTRACTION() { return getToken(BKOOLParser.SUBTRACTION, 0); }
		public Binary_expressionContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BKOOLVisitor ) return ((BKOOLVisitor<? extends T>)visitor).visitBinary_expression(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class Term_expressionContext extends ExpressionContext {
		public TerminalNode LB() { return getToken(BKOOLParser.LB, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public TerminalNode RB() { return getToken(BKOOLParser.RB, 0); }
		public Term_expressionContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BKOOLVisitor ) return ((BKOOLVisitor<? extends T>)visitor).visitTerm_expression(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class Logic_expressionContext extends ExpressionContext {
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public TerminalNode LOGICAL_AND() { return getToken(BKOOLParser.LOGICAL_AND, 0); }
		public TerminalNode LOGICAL_OR() { return getToken(BKOOLParser.LOGICAL_OR, 0); }
		public Logic_expressionContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BKOOLVisitor ) return ((BKOOLVisitor<? extends T>)visitor).visitLogic_expression(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class Condition_expressionContext extends ExpressionContext {
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public TerminalNode EQUAL() { return getToken(BKOOLParser.EQUAL, 0); }
		public TerminalNode NOT_EQUAL() { return getToken(BKOOLParser.NOT_EQUAL, 0); }
		public Condition_expressionContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BKOOLVisitor ) return ((BKOOLVisitor<? extends T>)visitor).visitCondition_expression(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class Unary_expressionContext extends ExpressionContext {
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public TerminalNode ADDITION() { return getToken(BKOOLParser.ADDITION, 0); }
		public TerminalNode SUBTRACTION() { return getToken(BKOOLParser.SUBTRACTION, 0); }
		public Unary_expressionContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BKOOLVisitor ) return ((BKOOLVisitor<? extends T>)visitor).visitUnary_expression(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class Index_operatorContext extends ExpressionContext {
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public TerminalNode LSB() { return getToken(BKOOLParser.LSB, 0); }
		public TerminalNode RSB() { return getToken(BKOOLParser.RSB, 0); }
		public Index_operatorContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BKOOLVisitor ) return ((BKOOLVisitor<? extends T>)visitor).visitIndex_operator(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class Self_expressionContext extends ExpressionContext {
		public TerminalNode THIS() { return getToken(BKOOLParser.THIS, 0); }
		public Self_expressionContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BKOOLVisitor ) return ((BKOOLVisitor<? extends T>)visitor).visitSelf_expression(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class String_expressionContext extends ExpressionContext {
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public TerminalNode CONCATENATION() { return getToken(BKOOLParser.CONCATENATION, 0); }
		public String_expressionContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BKOOLVisitor ) return ((BKOOLVisitor<? extends T>)visitor).visitString_expression(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class Member_accessContext extends ExpressionContext {
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public TerminalNode DOT() { return getToken(BKOOLParser.DOT, 0); }
		public TerminalNode IDENTIFIER() { return getToken(BKOOLParser.IDENTIFIER, 0); }
		public TerminalNode LB() { return getToken(BKOOLParser.LB, 0); }
		public TerminalNode RB() { return getToken(BKOOLParser.RB, 0); }
		public List<TerminalNode> COMMA() { return getTokens(BKOOLParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(BKOOLParser.COMMA, i);
		}
		public Member_accessContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BKOOLVisitor ) return ((BKOOLVisitor<? extends T>)visitor).visitMember_access(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class Arithmetic_expressionContext extends ExpressionContext {
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public TerminalNode MODULUS() { return getToken(BKOOLParser.MODULUS, 0); }
		public TerminalNode INTEGER_DIVISION() { return getToken(BKOOLParser.INTEGER_DIVISION, 0); }
		public TerminalNode FLOAT_DIVISION() { return getToken(BKOOLParser.FLOAT_DIVISION, 0); }
		public TerminalNode MULTIPLICATION() { return getToken(BKOOLParser.MULTIPLICATION, 0); }
		public Arithmetic_expressionContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BKOOLVisitor ) return ((BKOOLVisitor<? extends T>)visitor).visitArithmetic_expression(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class Negative_expressionContext extends ExpressionContext {
		public TerminalNode LOGICAL_NOT() { return getToken(BKOOLParser.LOGICAL_NOT, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public Negative_expressionContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BKOOLVisitor ) return ((BKOOLVisitor<? extends T>)visitor).visitNegative_expression(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class New_expressionContext extends ExpressionContext {
		public TerminalNode NEW() { return getToken(BKOOLParser.NEW, 0); }
		public TerminalNode IDENTIFIER() { return getToken(BKOOLParser.IDENTIFIER, 0); }
		public TerminalNode LB() { return getToken(BKOOLParser.LB, 0); }
		public TerminalNode RB() { return getToken(BKOOLParser.RB, 0); }
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public List<TerminalNode> COMMA() { return getTokens(BKOOLParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(BKOOLParser.COMMA, i);
		}
		public New_expressionContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BKOOLVisitor ) return ((BKOOLVisitor<? extends T>)visitor).visitNew_expression(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class Relational_expressionContext extends ExpressionContext {
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public TerminalNode LESS_THAN() { return getToken(BKOOLParser.LESS_THAN, 0); }
		public TerminalNode GREATER_THAN() { return getToken(BKOOLParser.GREATER_THAN, 0); }
		public TerminalNode GREATER_THAN_OR_EQUAL() { return getToken(BKOOLParser.GREATER_THAN_OR_EQUAL, 0); }
		public TerminalNode LESS_THAN_OR_EQUAL() { return getToken(BKOOLParser.LESS_THAN_OR_EQUAL, 0); }
		public Relational_expressionContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BKOOLVisitor ) return ((BKOOLVisitor<? extends T>)visitor).visitRelational_expression(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class Literal_expressionContext extends ExpressionContext {
		public TerminalNode IDENTIFIER() { return getToken(BKOOLParser.IDENTIFIER, 0); }
		public TerminalNode INTEGER_LITERAL() { return getToken(BKOOLParser.INTEGER_LITERAL, 0); }
		public TerminalNode FLOAT_LITERAL() { return getToken(BKOOLParser.FLOAT_LITERAL, 0); }
		public TerminalNode BOOLEAN_LITERAL() { return getToken(BKOOLParser.BOOLEAN_LITERAL, 0); }
		public TerminalNode STRING_LITERAL() { return getToken(BKOOLParser.STRING_LITERAL, 0); }
		public TerminalNode NIL() { return getToken(BKOOLParser.NIL, 0); }
		public Literal_expressionContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BKOOLVisitor ) return ((BKOOLVisitor<? extends T>)visitor).visitLiteral_expression(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ExpressionContext expression() throws RecognitionException {
		return expression(0);
	}

	private ExpressionContext expression(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		ExpressionContext _localctx = new ExpressionContext(_ctx, _parentState);
		ExpressionContext _prevctx = _localctx;
		int _startState = 52;
		enterRecursionRule(_localctx, 52, RULE_expression, _p);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(291);
			switch (_input.LA(1)) {
			case NEW:
				{
				_localctx = new New_expressionContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;

				setState(267);
				match(NEW);
				setState(268);
				match(IDENTIFIER);
				setState(269);
				match(LB);
				setState(278);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << NIL) | (1L << THIS) | (1L << BOOLEAN_LITERAL) | (1L << NEW) | (1L << IDENTIFIER) | (1L << ADDITION) | (1L << SUBTRACTION) | (1L << LOGICAL_NOT) | (1L << LB) | (1L << INTEGER_LITERAL) | (1L << FLOAT_LITERAL) | (1L << STRING_LITERAL))) != 0)) {
					{
					setState(270);
					expression(0);
					setState(275);
					_errHandler.sync(this);
					_la = _input.LA(1);
					while (_la==COMMA) {
						{
						{
						setState(271);
						match(COMMA);
						setState(272);
						expression(0);
						}
						}
						setState(277);
						_errHandler.sync(this);
						_la = _input.LA(1);
					}
					}
				}

				setState(280);
				match(RB);
				}
				break;
			case THIS:
				{
				_localctx = new Self_expressionContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(281);
				match(THIS);
				}
				break;
			case ADDITION:
			case SUBTRACTION:
				{
				_localctx = new Unary_expressionContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(282);
				_la = _input.LA(1);
				if ( !(_la==ADDITION || _la==SUBTRACTION) ) {
				_errHandler.recoverInline(this);
				} else {
					consume();
				}
				setState(283);
				expression(10);
				}
				break;
			case LOGICAL_NOT:
				{
				_localctx = new Negative_expressionContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(284);
				match(LOGICAL_NOT);
				setState(285);
				expression(9);
				}
				break;
			case LB:
				{
				_localctx = new Term_expressionContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(286);
				match(LB);
				setState(287);
				expression(0);
				setState(288);
				match(RB);
				}
				break;
			case NIL:
			case BOOLEAN_LITERAL:
			case IDENTIFIER:
			case INTEGER_LITERAL:
			case FLOAT_LITERAL:
			case STRING_LITERAL:
				{
				_localctx = new Literal_expressionContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(290);
				_la = _input.LA(1);
				if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << NIL) | (1L << BOOLEAN_LITERAL) | (1L << IDENTIFIER) | (1L << INTEGER_LITERAL) | (1L << FLOAT_LITERAL) | (1L << STRING_LITERAL))) != 0)) ) {
				_errHandler.recoverInline(this);
				} else {
					consume();
				}
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			_ctx.stop = _input.LT(-1);
			setState(335);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,32,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					setState(333);
					_errHandler.sync(this);
					switch ( getInterpreter().adaptivePredict(_input,31,_ctx) ) {
					case 1:
						{
						_localctx = new String_expressionContext(new ExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(293);
						if (!(precpred(_ctx, 8))) throw new FailedPredicateException(this, "precpred(_ctx, 8)");
						setState(294);
						match(CONCATENATION);
						setState(295);
						expression(9);
						}
						break;
					case 2:
						{
						_localctx = new Arithmetic_expressionContext(new ExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(296);
						if (!(precpred(_ctx, 7))) throw new FailedPredicateException(this, "precpred(_ctx, 7)");
						setState(297);
						_la = _input.LA(1);
						if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << MULTIPLICATION) | (1L << FLOAT_DIVISION) | (1L << INTEGER_DIVISION) | (1L << MODULUS))) != 0)) ) {
						_errHandler.recoverInline(this);
						} else {
							consume();
						}
						setState(298);
						expression(8);
						}
						break;
					case 3:
						{
						_localctx = new Binary_expressionContext(new ExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(299);
						if (!(precpred(_ctx, 6))) throw new FailedPredicateException(this, "precpred(_ctx, 6)");
						setState(300);
						_la = _input.LA(1);
						if ( !(_la==ADDITION || _la==SUBTRACTION) ) {
						_errHandler.recoverInline(this);
						} else {
							consume();
						}
						setState(301);
						expression(7);
						}
						break;
					case 4:
						{
						_localctx = new Logic_expressionContext(new ExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(302);
						if (!(precpred(_ctx, 5))) throw new FailedPredicateException(this, "precpred(_ctx, 5)");
						setState(303);
						_la = _input.LA(1);
						if ( !(_la==LOGICAL_OR || _la==LOGICAL_AND) ) {
						_errHandler.recoverInline(this);
						} else {
							consume();
						}
						setState(304);
						expression(6);
						}
						break;
					case 5:
						{
						_localctx = new Condition_expressionContext(new ExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(305);
						if (!(precpred(_ctx, 4))) throw new FailedPredicateException(this, "precpred(_ctx, 4)");
						setState(306);
						_la = _input.LA(1);
						if ( !(_la==NOT_EQUAL || _la==EQUAL) ) {
						_errHandler.recoverInline(this);
						} else {
							consume();
						}
						setState(307);
						expression(5);
						}
						break;
					case 6:
						{
						_localctx = new Relational_expressionContext(new ExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(308);
						if (!(precpred(_ctx, 3))) throw new FailedPredicateException(this, "precpred(_ctx, 3)");
						setState(309);
						_la = _input.LA(1);
						if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << LESS_THAN_OR_EQUAL) | (1L << GREATER_THAN_OR_EQUAL) | (1L << LESS_THAN) | (1L << GREATER_THAN))) != 0)) ) {
						_errHandler.recoverInline(this);
						} else {
							consume();
						}
						setState(310);
						expression(4);
						}
						break;
					case 7:
						{
						_localctx = new Member_accessContext(new ExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(311);
						if (!(precpred(_ctx, 12))) throw new FailedPredicateException(this, "precpred(_ctx, 12)");
						setState(312);
						match(DOT);
						setState(313);
						match(IDENTIFIER);
						setState(326);
						_errHandler.sync(this);
						switch ( getInterpreter().adaptivePredict(_input,30,_ctx) ) {
						case 1:
							{
							setState(314);
							match(LB);
							setState(323);
							_la = _input.LA(1);
							if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << NIL) | (1L << THIS) | (1L << BOOLEAN_LITERAL) | (1L << NEW) | (1L << IDENTIFIER) | (1L << ADDITION) | (1L << SUBTRACTION) | (1L << LOGICAL_NOT) | (1L << LB) | (1L << INTEGER_LITERAL) | (1L << FLOAT_LITERAL) | (1L << STRING_LITERAL))) != 0)) {
								{
								setState(315);
								expression(0);
								setState(320);
								_errHandler.sync(this);
								_la = _input.LA(1);
								while (_la==COMMA) {
									{
									{
									setState(316);
									match(COMMA);
									setState(317);
									expression(0);
									}
									}
									setState(322);
									_errHandler.sync(this);
									_la = _input.LA(1);
								}
								}
							}

							setState(325);
							match(RB);
							}
							break;
						}
						}
						break;
					case 8:
						{
						_localctx = new Index_operatorContext(new ExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(328);
						if (!(precpred(_ctx, 11))) throw new FailedPredicateException(this, "precpred(_ctx, 11)");
						setState(329);
						match(LSB);
						setState(330);
						expression(0);
						setState(331);
						match(RSB);
						}
						break;
					}
					} 
				}
				setState(337);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,32,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public boolean sempred(RuleContext _localctx, int ruleIndex, int predIndex) {
		switch (ruleIndex) {
		case 7:
			return parameter_list_sempred((Parameter_listContext)_localctx, predIndex);
		case 9:
			return identifier_list_sempred((Identifier_listContext)_localctx, predIndex);
		case 26:
			return expression_sempred((ExpressionContext)_localctx, predIndex);
		}
		return true;
	}
	private boolean parameter_list_sempred(Parameter_listContext _localctx, int predIndex) {
		switch (predIndex) {
		case 0:
			return precpred(_ctx, 1);
		}
		return true;
	}
	private boolean identifier_list_sempred(Identifier_listContext _localctx, int predIndex) {
		switch (predIndex) {
		case 1:
			return precpred(_ctx, 1);
		}
		return true;
	}
	private boolean expression_sempred(ExpressionContext _localctx, int predIndex) {
		switch (predIndex) {
		case 2:
			return precpred(_ctx, 8);
		case 3:
			return precpred(_ctx, 7);
		case 4:
			return precpred(_ctx, 6);
		case 5:
			return precpred(_ctx, 5);
		case 6:
			return precpred(_ctx, 4);
		case 7:
			return precpred(_ctx, 3);
		case 8:
			return precpred(_ctx, 12);
		case 9:
			return precpred(_ctx, 11);
		}
		return true;
	}

	public static final String _serializedATN =
		"\3\u0430\ud6d1\u8206\uad2d\u4417\uaef1\u8d80\uaadd\3@\u0155\4\2\t\2\4"+
		"\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t"+
		"\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22"+
		"\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30\4\31\t\31"+
		"\4\32\t\32\4\33\t\33\4\34\t\34\3\2\6\2:\n\2\r\2\16\2;\3\2\3\2\3\3\3\3"+
		"\3\3\3\3\5\3D\n\3\3\3\3\3\3\3\3\3\3\4\3\4\3\4\3\4\3\4\7\4O\n\4\f\4\16"+
		"\4R\13\4\3\5\3\5\3\5\5\5W\n\5\3\5\3\5\3\5\3\6\5\6]\n\6\3\6\3\6\3\6\3\6"+
		"\3\6\3\6\3\6\3\7\5\7g\n\7\3\7\3\7\3\7\3\b\3\b\5\bn\n\b\3\b\3\b\3\b\5\b"+
		"s\n\b\3\b\3\b\3\b\3\t\3\t\3\t\3\t\3\t\3\t\7\t~\n\t\f\t\16\t\u0081\13\t"+
		"\3\n\3\n\3\n\3\n\3\13\3\13\3\13\3\13\3\13\3\13\7\13\u008d\n\13\f\13\16"+
		"\13\u0090\13\13\3\f\3\f\3\f\3\f\3\f\3\f\3\f\5\f\u0099\n\f\3\r\3\r\3\r"+
		"\3\r\3\r\3\r\5\r\u00a1\n\r\3\16\3\16\3\16\3\16\3\16\3\17\3\17\3\20\3\20"+
		"\3\20\3\20\3\20\3\20\3\20\3\20\5\20\u00b2\n\20\3\21\3\21\3\21\3\21\3\21"+
		"\3\22\3\22\7\22\u00bb\n\22\f\22\16\22\u00be\13\22\3\23\7\23\u00c1\n\23"+
		"\f\23\16\23\u00c4\13\23\3\24\3\24\3\24\3\24\3\24\5\24\u00cb\n\24\3\24"+
		"\3\24\3\24\5\24\u00d0\n\24\5\24\u00d2\n\24\3\25\3\25\3\25\3\25\3\25\3"+
		"\25\3\25\3\25\3\25\5\25\u00dd\n\25\3\26\3\26\3\26\3\27\3\27\3\27\3\30"+
		"\3\30\3\30\3\30\3\31\3\31\3\31\3\31\3\31\3\32\3\32\3\32\3\32\3\32\3\32"+
		"\3\32\7\32\u00f5\n\32\f\32\16\32\u00f8\13\32\5\32\u00fa\n\32\3\32\5\32"+
		"\u00fd\n\32\3\32\3\32\3\33\3\33\3\33\3\33\3\33\3\33\3\33\3\33\3\33\3\33"+
		"\5\33\u010b\n\33\3\34\3\34\3\34\3\34\3\34\3\34\3\34\7\34\u0114\n\34\f"+
		"\34\16\34\u0117\13\34\5\34\u0119\n\34\3\34\3\34\3\34\3\34\3\34\3\34\3"+
		"\34\3\34\3\34\3\34\3\34\5\34\u0126\n\34\3\34\3\34\3\34\3\34\3\34\3\34"+
		"\3\34\3\34\3\34\3\34\3\34\3\34\3\34\3\34\3\34\3\34\3\34\3\34\3\34\3\34"+
		"\3\34\3\34\3\34\3\34\3\34\7\34\u0141\n\34\f\34\16\34\u0144\13\34\5\34"+
		"\u0146\n\34\3\34\5\34\u0149\n\34\3\34\3\34\3\34\3\34\3\34\7\34\u0150\n"+
		"\34\f\34\16\34\u0153\13\34\3\34\2\5\20\24\66\35\2\4\6\b\n\f\16\20\22\24"+
		"\26\30\32\34\36 \"$&(*,.\60\62\64\66\2\t\3\2\32\33\3\2%&\6\2\b\b\23\23"+
		"\35\35:<\3\2\'*\3\2\"#\3\2\36\37\4\2 !+,\u0177\29\3\2\2\2\4?\3\2\2\2\6"+
		"P\3\2\2\2\bS\3\2\2\2\n\\\3\2\2\2\ff\3\2\2\2\16k\3\2\2\2\20w\3\2\2\2\22"+
		"\u0082\3\2\2\2\24\u0086\3\2\2\2\26\u0098\3\2\2\2\30\u00a0\3\2\2\2\32\u00a2"+
		"\3\2\2\2\34\u00a7\3\2\2\2\36\u00b1\3\2\2\2 \u00b3\3\2\2\2\"\u00bc\3\2"+
		"\2\2$\u00c2\3\2\2\2&\u00c5\3\2\2\2(\u00d3\3\2\2\2*\u00de\3\2\2\2,\u00e1"+
		"\3\2\2\2.\u00e4\3\2\2\2\60\u00e8\3\2\2\2\62\u00ed\3\2\2\2\64\u010a\3\2"+
		"\2\2\66\u0125\3\2\2\28:\5\4\3\298\3\2\2\2:;\3\2\2\2;9\3\2\2\2;<\3\2\2"+
		"\2<=\3\2\2\2=>\7\2\2\3>\3\3\2\2\2?@\7\r\2\2@C\7\35\2\2AB\7\6\2\2BD\7\35"+
		"\2\2CA\3\2\2\2CD\3\2\2\2DE\3\2\2\2EF\7\60\2\2FG\5\6\4\2GH\7\61\2\2H\5"+
		"\3\2\2\2IO\5\16\b\2JO\5\n\6\2KO\5\f\7\2LO\5\b\5\2MO\5\4\3\2NI\3\2\2\2"+
		"NJ\3\2\2\2NK\3\2\2\2NL\3\2\2\2NM\3\2\2\2OR\3\2\2\2PN\3\2\2\2PQ\3\2\2\2"+
		"Q\7\3\2\2\2RP\3\2\2\2ST\7\35\2\2TV\7\64\2\2UW\5\20\t\2VU\3\2\2\2VW\3\2"+
		"\2\2WX\3\2\2\2XY\7\65\2\2YZ\5 \21\2Z\t\3\2\2\2[]\7\24\2\2\\[\3\2\2\2\\"+
		"]\3\2\2\2]^\3\2\2\2^_\7\20\2\2_`\5\26\f\2`a\7\35\2\2ab\7/\2\2bc\5\66\34"+
		"\2cd\7\66\2\2d\13\3\2\2\2eg\7\24\2\2fe\3\2\2\2fg\3\2\2\2gh\3\2\2\2hi\5"+
		"\22\n\2ij\7\66\2\2j\r\3\2\2\2km\5\26\f\2ln\7\24\2\2ml\3\2\2\2mn\3\2\2"+
		"\2no\3\2\2\2op\7\35\2\2pr\7\64\2\2qs\5\20\t\2rq\3\2\2\2rs\3\2\2\2st\3"+
		"\2\2\2tu\7\65\2\2uv\5 \21\2v\17\3\2\2\2wx\b\t\1\2xy\5\22\n\2y\177\3\2"+
		"\2\2z{\f\3\2\2{|\7\66\2\2|~\5\22\n\2}z\3\2\2\2~\u0081\3\2\2\2\177}\3\2"+
		"\2\2\177\u0080\3\2\2\2\u0080\21\3\2\2\2\u0081\177\3\2\2\2\u0082\u0083"+
		"\5\24\13\2\u0083\u0084\7\67\2\2\u0084\u0085\5\26\f\2\u0085\23\3\2\2\2"+
		"\u0086\u0087\b\13\1\2\u0087\u0088\7\35\2\2\u0088\u008e\3\2\2\2\u0089\u008a"+
		"\f\3\2\2\u008a\u008b\79\2\2\u008b\u008d\7\35\2\2\u008c\u0089\3\2\2\2\u008d"+
		"\u0090\3\2\2\2\u008e\u008c\3\2\2\2\u008e\u008f\3\2\2\2\u008f\25\3\2\2"+
		"\2\u0090\u008e\3\2\2\2\u0091\u0099\7\22\2\2\u0092\u0099\7\n\2\2\u0093"+
		"\u0099\7\5\2\2\u0094\u0099\7\30\2\2\u0095\u0099\7\31\2\2\u0096\u0099\5"+
		"\32\16\2\u0097\u0099\5\34\17\2\u0098\u0091\3\2\2\2\u0098\u0092\3\2\2\2"+
		"\u0098\u0093\3\2\2\2\u0098\u0094\3\2\2\2\u0098\u0095\3\2\2\2\u0098\u0096"+
		"\3\2\2\2\u0098\u0097\3\2\2\2\u0099\27\3\2\2\2\u009a\u00a1\7\22\2\2\u009b"+
		"\u00a1\7\n\2\2\u009c\u00a1\7\5\2\2\u009d\u00a1\7\30\2\2\u009e\u00a1\7"+
		"\31\2\2\u009f\u00a1\5\34\17\2\u00a0\u009a\3\2\2\2\u00a0\u009b\3\2\2\2"+
		"\u00a0\u009c\3\2\2\2\u00a0\u009d\3\2\2\2\u00a0\u009e\3\2\2\2\u00a0\u009f"+
		"\3\2\2\2\u00a1\31\3\2\2\2\u00a2\u00a3\5\30\r\2\u00a3\u00a4\7\62\2\2\u00a4"+
		"\u00a5\7:\2\2\u00a5\u00a6\7\63\2\2\u00a6\33\3\2\2\2\u00a7\u00a8\7\35\2"+
		"\2\u00a8\35\3\2\2\2\u00a9\u00b2\5\60\31\2\u00aa\u00b2\5&\24\2\u00ab\u00b2"+
		"\5(\25\2\u00ac\u00b2\5*\26\2\u00ad\u00b2\5,\27\2\u00ae\u00b2\5.\30\2\u00af"+
		"\u00b2\5\62\32\2\u00b0\u00b2\5 \21\2\u00b1\u00a9\3\2\2\2\u00b1\u00aa\3"+
		"\2\2\2\u00b1\u00ab\3\2\2\2\u00b1\u00ac\3\2\2\2\u00b1\u00ad\3\2\2\2\u00b1"+
		"\u00ae\3\2\2\2\u00b1\u00af\3\2\2\2\u00b1\u00b0\3\2\2\2\u00b2\37\3\2\2"+
		"\2\u00b3\u00b4\7\60\2\2\u00b4\u00b5\5\"\22\2\u00b5\u00b6\5$\23\2\u00b6"+
		"\u00b7\7\61\2\2\u00b7!\3\2\2\2\u00b8\u00bb\5\n\6\2\u00b9\u00bb\5\f\7\2"+
		"\u00ba\u00b8\3\2\2\2\u00ba\u00b9\3\2\2\2\u00bb\u00be\3\2\2\2\u00bc\u00ba"+
		"\3\2\2\2\u00bc\u00bd\3\2\2\2\u00bd#\3\2\2\2\u00be\u00bc\3\2\2\2\u00bf"+
		"\u00c1\5\36\20\2\u00c0\u00bf\3\2\2\2\u00c1\u00c4\3\2\2\2\u00c2\u00c0\3"+
		"\2\2\2\u00c2\u00c3\3\2\2\2\u00c3%\3\2\2\2\u00c4\u00c2\3\2\2\2\u00c5\u00c6"+
		"\7\16\2\2\u00c6\u00c7\5\66\34\2\u00c7\u00ca\7\7\2\2\u00c8\u00cb\5\36\20"+
		"\2\u00c9\u00cb\5 \21\2\u00ca\u00c8\3\2\2\2\u00ca\u00c9\3\2\2\2\u00cb\u00d1"+
		"\3\2\2\2\u00cc\u00cf\7\27\2\2\u00cd\u00d0\5\36\20\2\u00ce\u00d0\5 \21"+
		"\2\u00cf\u00cd\3\2\2\2\u00cf\u00ce\3\2\2\2\u00d0\u00d2\3\2\2\2\u00d1\u00cc"+
		"\3\2\2\2\u00d1\u00d2\3\2\2\2\u00d2\'\3\2\2\2\u00d3\u00d4\7\13\2\2\u00d4"+
		"\u00d5\7\35\2\2\u00d5\u00d6\7.\2\2\u00d6\u00d7\5\66\34\2\u00d7\u00d8\t"+
		"\2\2\2\u00d8\u00d9\5\66\34\2\u00d9\u00dc\7\25\2\2\u00da\u00dd\5\36\20"+
		"\2\u00db\u00dd\5 \21\2\u00dc\u00da\3\2\2\2\u00dc\u00db\3\2\2\2\u00dd)"+
		"\3\2\2\2\u00de\u00df\7\t\2\2\u00df\u00e0\7\66\2\2\u00e0+\3\2\2\2\u00e1"+
		"\u00e2\7\21\2\2\u00e2\u00e3\7\66\2\2\u00e3-\3\2\2\2\u00e4\u00e5\7\17\2"+
		"\2\u00e5\u00e6\5\66\34\2\u00e6\u00e7\7\66\2\2\u00e7/\3\2\2\2\u00e8\u00e9"+
		"\5\64\33\2\u00e9\u00ea\7.\2\2\u00ea\u00eb\5\66\34\2\u00eb\u00ec\7\66\2"+
		"\2\u00ec\61\3\2\2\2\u00ed\u00ee\5\66\34\2\u00ee\u00ef\78\2\2\u00ef\u00fc"+
		"\7\35\2\2\u00f0\u00f9\7\64\2\2\u00f1\u00f6\5\66\34\2\u00f2\u00f3\79\2"+
		"\2\u00f3\u00f5\5\66\34\2\u00f4\u00f2\3\2\2\2\u00f5\u00f8\3\2\2\2\u00f6"+
		"\u00f4\3\2\2\2\u00f6\u00f7\3\2\2\2\u00f7\u00fa\3\2\2\2\u00f8\u00f6\3\2"+
		"\2\2\u00f9\u00f1\3\2\2\2\u00f9\u00fa\3\2\2\2\u00fa\u00fb\3\2\2\2\u00fb"+
		"\u00fd\7\65\2\2\u00fc\u00f0\3\2\2\2\u00fc\u00fd\3\2\2\2\u00fd\u00fe\3"+
		"\2\2\2\u00fe\u00ff\7\66\2\2\u00ff\63\3\2\2\2\u0100\u0101\5\66\34\2\u0101"+
		"\u0102\78\2\2\u0102\u0103\7\35\2\2\u0103\u010b\3\2\2\2\u0104\u010b\7\35"+
		"\2\2\u0105\u0106\5\66\34\2\u0106\u0107\7\62\2\2\u0107\u0108\5\66\34\2"+
		"\u0108\u0109\7\63\2\2\u0109\u010b\3\2\2\2\u010a\u0100\3\2\2\2\u010a\u0104"+
		"\3\2\2\2\u010a\u0105\3\2\2\2\u010b\65\3\2\2\2\u010c\u010d\b\34\1\2\u010d"+
		"\u010e\7\26\2\2\u010e\u010f\7\35\2\2\u010f\u0118\7\64\2\2\u0110\u0115"+
		"\5\66\34\2\u0111\u0112\79\2\2\u0112\u0114\5\66\34\2\u0113\u0111\3\2\2"+
		"\2\u0114\u0117\3\2\2\2\u0115\u0113\3\2\2\2\u0115\u0116\3\2\2\2\u0116\u0119"+
		"\3\2\2\2\u0117\u0115\3\2\2\2\u0118\u0110\3\2\2\2\u0118\u0119\3\2\2\2\u0119"+
		"\u011a\3\2\2\2\u011a\u0126\7\65\2\2\u011b\u0126\7\f\2\2\u011c\u011d\t"+
		"\3\2\2\u011d\u0126\5\66\34\f\u011e\u011f\7-\2\2\u011f\u0126\5\66\34\13"+
		"\u0120\u0121\7\64\2\2\u0121\u0122\5\66\34\2\u0122\u0123\7\65\2\2\u0123"+
		"\u0126\3\2\2\2\u0124\u0126\t\4\2\2\u0125\u010c\3\2\2\2\u0125\u011b\3\2"+
		"\2\2\u0125\u011c\3\2\2\2\u0125\u011e\3\2\2\2\u0125\u0120\3\2\2\2\u0125"+
		"\u0124\3\2\2\2\u0126\u0151\3\2\2\2\u0127\u0128\f\n\2\2\u0128\u0129\7$"+
		"\2\2\u0129\u0150\5\66\34\13\u012a\u012b\f\t\2\2\u012b\u012c\t\5\2\2\u012c"+
		"\u0150\5\66\34\n\u012d\u012e\f\b\2\2\u012e\u012f\t\3\2\2\u012f\u0150\5"+
		"\66\34\t\u0130\u0131\f\7\2\2\u0131\u0132\t\6\2\2\u0132\u0150\5\66\34\b"+
		"\u0133\u0134\f\6\2\2\u0134\u0135\t\7\2\2\u0135\u0150\5\66\34\7\u0136\u0137"+
		"\f\5\2\2\u0137\u0138\t\b\2\2\u0138\u0150\5\66\34\6\u0139\u013a\f\16\2"+
		"\2\u013a\u013b\78\2\2\u013b\u0148\7\35\2\2\u013c\u0145\7\64\2\2\u013d"+
		"\u0142\5\66\34\2\u013e\u013f\79\2\2\u013f\u0141\5\66\34\2\u0140\u013e"+
		"\3\2\2\2\u0141\u0144\3\2\2\2\u0142\u0140\3\2\2\2\u0142\u0143\3\2\2\2\u0143"+
		"\u0146\3\2\2\2\u0144\u0142\3\2\2\2\u0145\u013d\3\2\2\2\u0145\u0146\3\2"+
		"\2\2\u0146\u0147\3\2\2\2\u0147\u0149\7\65\2\2\u0148\u013c\3\2\2\2\u0148"+
		"\u0149\3\2\2\2\u0149\u0150\3\2\2\2\u014a\u014b\f\r\2\2\u014b\u014c\7\62"+
		"\2\2\u014c\u014d\5\66\34\2\u014d\u014e\7\63\2\2\u014e\u0150\3\2\2\2\u014f"+
		"\u0127\3\2\2\2\u014f\u012a\3\2\2\2\u014f\u012d\3\2\2\2\u014f\u0130\3\2"+
		"\2\2\u014f\u0133\3\2\2\2\u014f\u0136\3\2\2\2\u014f\u0139\3\2\2\2\u014f"+
		"\u014a\3\2\2\2\u0150\u0153\3\2\2\2\u0151\u014f\3\2\2\2\u0151\u0152\3\2"+
		"\2\2\u0152\67\3\2\2\2\u0153\u0151\3\2\2\2#;CNPV\\fmr\177\u008e\u0098\u00a0"+
		"\u00b1\u00ba\u00bc\u00c2\u00ca\u00cf\u00d1\u00dc\u00f6\u00f9\u00fc\u010a"+
		"\u0115\u0118\u0125\u0142\u0145\u0148\u014f\u0151";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}