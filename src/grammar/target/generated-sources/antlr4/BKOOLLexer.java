// Generated from BKOOL.g4 by ANTLR 4.5.3

	package bkool.parser;

import org.antlr.v4.runtime.Lexer;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.TokenStream;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.misc.*;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class BKOOLLexer extends Lexer {
	static { RuntimeMetaData.checkVersion("4.5.3", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		LINE_COMMENT=1, BLOCK_COMMENT=2, BOOLEAN=3, EXTENDS=4, THEN=5, NIL=6, 
		BREAK=7, FLOAT=8, FOR=9, THIS=10, CLASS=11, IF=12, RETURN=13, FINAL=14, 
		CONTINUE=15, INT=16, BOOLEAN_LITERAL=17, STATIC=18, DO=19, NEW=20, ELSE=21, 
		STRING=22, VOID=23, TO=24, DOWNTO=25, OBJECT_CREATION=26, IDENTIFIER=27, 
		NOT_EQUAL=28, EQUAL=29, LESS_THAN_OR_EQUAL=30, GREATER_THAN_OR_EQUAL=31, 
		LOGICAL_OR=32, LOGICAL_AND=33, CONCATENATION=34, ADDITION=35, SUBTRACTION=36, 
		MULTIPLICATION=37, FLOAT_DIVISION=38, INTEGER_DIVISION=39, MODULUS=40, 
		LESS_THAN=41, GREATER_THAN=42, LOGICAL_NOT=43, ASSIGNMENT_OPERATOR=44, 
		IS_EQUAL_TO=45, LP=46, RP=47, LSB=48, RSB=49, LB=50, RB=51, SC=52, COLON=53, 
		DOT=54, COMMA=55, INTEGER_LITERAL=56, FLOAT_LITERAL=57, STRING_LITERAL=58, 
		WS=59, UNCLOSE_STRING=60, ILLEGAL_ESCAPE=61, ERROR_CHAR=62;
	public static String[] modeNames = {
		"DEFAULT_MODE"
	};

	public static final String[] ruleNames = {
		"LINE_COMMENT", "BLOCK_COMMENT", "BOOLEAN", "EXTENDS", "THEN", "NIL", 
		"BREAK", "FLOAT", "FOR", "THIS", "CLASS", "IF", "RETURN", "FINAL", "CONTINUE", 
		"INT", "BOOLEAN_LITERAL", "STATIC", "DO", "NEW", "ELSE", "STRING", "VOID", 
		"TO", "DOWNTO", "OBJECT_CREATION", "IDENTIFIER", "NOT_EQUAL", "EQUAL", 
		"LESS_THAN_OR_EQUAL", "GREATER_THAN_OR_EQUAL", "LOGICAL_OR", "LOGICAL_AND", 
		"CONCATENATION", "ADDITION", "SUBTRACTION", "MULTIPLICATION", "FLOAT_DIVISION", 
		"INTEGER_DIVISION", "MODULUS", "LESS_THAN", "GREATER_THAN", "LOGICAL_NOT", 
		"ASSIGNMENT_OPERATOR", "IS_EQUAL_TO", "LP", "RP", "LSB", "RSB", "LB", 
		"RB", "SC", "COLON", "DOT", "COMMA", "INTEGER_LITERAL", "FLOAT_LITERAL", 
		"STRING_LITERAL", "WS", "UNCLOSE_STRING", "ILLEGAL_ESCAPE", "ERROR_CHAR"
	};

	private static final String[] _LITERAL_NAMES = {
		null, null, null, "'boolean'", "'extends'", "'then'", "'nil'", "'break'", 
		"'float'", "'for'", "'this'", "'class'", "'if'", "'return'", "'final'", 
		"'continue'", "'int'", null, "'static'", "'do'", null, "'else'", "'string'", 
		"'void'", "'to'", "'downto'", null, null, "'!='", "'=='", "'<='", "'>='", 
		"'||'", "'&&'", "'^'", "'+'", "'-'", "'*'", "'/'", "'\\u005C'", "'%'", 
		"'<'", "'>'", "'!'", "':='", "'='", "'{'", "'}'", "'['", "']'", "'('", 
		"')'", "';'", "':'", "'.'", "','"
	};
	private static final String[] _SYMBOLIC_NAMES = {
		null, "LINE_COMMENT", "BLOCK_COMMENT", "BOOLEAN", "EXTENDS", "THEN", "NIL", 
		"BREAK", "FLOAT", "FOR", "THIS", "CLASS", "IF", "RETURN", "FINAL", "CONTINUE", 
		"INT", "BOOLEAN_LITERAL", "STATIC", "DO", "NEW", "ELSE", "STRING", "VOID", 
		"TO", "DOWNTO", "OBJECT_CREATION", "IDENTIFIER", "NOT_EQUAL", "EQUAL", 
		"LESS_THAN_OR_EQUAL", "GREATER_THAN_OR_EQUAL", "LOGICAL_OR", "LOGICAL_AND", 
		"CONCATENATION", "ADDITION", "SUBTRACTION", "MULTIPLICATION", "FLOAT_DIVISION", 
		"INTEGER_DIVISION", "MODULUS", "LESS_THAN", "GREATER_THAN", "LOGICAL_NOT", 
		"ASSIGNMENT_OPERATOR", "IS_EQUAL_TO", "LP", "RP", "LSB", "RSB", "LB", 
		"RB", "SC", "COLON", "DOT", "COMMA", "INTEGER_LITERAL", "FLOAT_LITERAL", 
		"STRING_LITERAL", "WS", "UNCLOSE_STRING", "ILLEGAL_ESCAPE", "ERROR_CHAR"
	};
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}


	public BKOOLLexer(CharStream input) {
		super(input);
		_interp = new LexerATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	@Override
	public String getGrammarFileName() { return "BKOOL.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public String[] getModeNames() { return modeNames; }

	@Override
	public ATN getATN() { return _ATN; }

	public static final String _serializedATN =
		"\3\u0430\ud6d1\u8206\uad2d\u4417\uaef1\u8d80\uaadd\2@\u01d6\b\1\4\2\t"+
		"\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13"+
		"\t\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22"+
		"\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30\4\31\t\31"+
		"\4\32\t\32\4\33\t\33\4\34\t\34\4\35\t\35\4\36\t\36\4\37\t\37\4 \t \4!"+
		"\t!\4\"\t\"\4#\t#\4$\t$\4%\t%\4&\t&\4\'\t\'\4(\t(\4)\t)\4*\t*\4+\t+\4"+
		",\t,\4-\t-\4.\t.\4/\t/\4\60\t\60\4\61\t\61\4\62\t\62\4\63\t\63\4\64\t"+
		"\64\4\65\t\65\4\66\t\66\4\67\t\67\48\t8\49\t9\4:\t:\4;\t;\4<\t<\4=\t="+
		"\4>\t>\4?\t?\3\2\3\2\3\2\3\2\7\2\u0084\n\2\f\2\16\2\u0087\13\2\3\2\7\2"+
		"\u008a\n\2\f\2\16\2\u008d\13\2\3\2\3\2\3\3\3\3\3\3\3\3\7\3\u0095\n\3\f"+
		"\3\16\3\u0098\13\3\3\3\3\3\7\3\u009c\n\3\f\3\16\3\u009f\13\3\3\3\3\3\7"+
		"\3\u00a3\n\3\f\3\16\3\u00a6\13\3\3\3\3\3\3\3\3\3\3\4\3\4\3\4\3\4\3\4\3"+
		"\4\3\4\3\4\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\6\3\6\3\6\3\6\3\6\3\7\3\7"+
		"\3\7\3\7\3\b\3\b\3\b\3\b\3\b\3\b\3\t\3\t\3\t\3\t\3\t\3\t\3\n\3\n\3\n\3"+
		"\n\3\13\3\13\3\13\3\13\3\13\3\f\3\f\3\f\3\f\3\f\3\f\3\r\3\r\3\r\3\16\3"+
		"\16\3\16\3\16\3\16\3\16\3\16\3\17\3\17\3\17\3\17\3\17\3\17\3\20\3\20\3"+
		"\20\3\20\3\20\3\20\3\20\3\20\3\20\3\21\3\21\3\21\3\21\3\22\3\22\3\22\3"+
		"\22\3\22\3\22\3\22\3\22\3\22\5\22\u0106\n\22\3\23\3\23\3\23\3\23\3\23"+
		"\3\23\3\23\3\24\3\24\3\24\3\25\3\25\3\25\3\25\3\26\3\26\3\26\3\26\3\26"+
		"\3\27\3\27\3\27\3\27\3\27\3\27\3\27\3\30\3\30\3\30\3\30\3\30\3\31\3\31"+
		"\3\31\3\32\3\32\3\32\3\32\3\32\3\32\3\32\3\33\3\33\3\33\3\33\3\34\6\34"+
		"\u0136\n\34\r\34\16\34\u0137\3\34\7\34\u013b\n\34\f\34\16\34\u013e\13"+
		"\34\3\35\3\35\3\35\3\36\3\36\3\36\3\37\3\37\3\37\3 \3 \3 \3!\3!\3!\3\""+
		"\3\"\3\"\3#\3#\3$\3$\3%\3%\3&\3&\3\'\3\'\3(\3(\3)\3)\3*\3*\3+\3+\3,\3"+
		",\3-\3-\3-\3.\3.\3/\3/\3\60\3\60\3\61\3\61\3\62\3\62\3\63\3\63\3\64\3"+
		"\64\3\65\3\65\3\66\3\66\3\67\3\67\38\38\39\69\u0180\n9\r9\169\u0181\3"+
		":\6:\u0185\n:\r:\16:\u0186\3:\3:\5:\u018b\n:\3:\6:\u018e\n:\r:\16:\u018f"+
		"\3:\3:\7:\u0194\n:\f:\16:\u0197\13:\3:\3:\7:\u019b\n:\f:\16:\u019e\13"+
		":\3:\3:\5:\u01a2\n:\3:\6:\u01a5\n:\r:\16:\u01a6\5:\u01a9\n:\3;\3;\3;\3"+
		";\7;\u01af\n;\f;\16;\u01b2\13;\3;\3;\3<\6<\u01b7\n<\r<\16<\u01b8\3<\3"+
		"<\3=\3=\3=\3=\7=\u01c1\n=\f=\16=\u01c4\13=\3=\5=\u01c7\n=\3>\3>\3>\3>"+
		"\7>\u01cd\n>\f>\16>\u01d0\13>\3>\3>\3>\3?\3?\3\u0085\2@\3\3\5\4\7\5\t"+
		"\6\13\7\r\b\17\t\21\n\23\13\25\f\27\r\31\16\33\17\35\20\37\21!\22#\23"+
		"%\24\'\25)\26+\27-\30/\31\61\32\63\33\65\34\67\359\36;\37= ?!A\"C#E$G"+
		"%I&K\'M(O)Q*S+U,W-Y.[/]\60_\61a\62c\63e\64g\65i\66k\67m8o9q:s;u<w=y>{"+
		"?}@\3\2\16\3\2\f\f\4\2,,\61\61\3\2,,\5\2C\\aac|\6\2\62;C\\aac|\3\2\62"+
		";\4\2GGgg\4\2--//\5\2\f\f$$^^\t\2$$^^ddhhppttvv\5\2\13\f\17\17\"\"\3\3"+
		"\f\f\u01f0\2\3\3\2\2\2\2\5\3\2\2\2\2\7\3\2\2\2\2\t\3\2\2\2\2\13\3\2\2"+
		"\2\2\r\3\2\2\2\2\17\3\2\2\2\2\21\3\2\2\2\2\23\3\2\2\2\2\25\3\2\2\2\2\27"+
		"\3\2\2\2\2\31\3\2\2\2\2\33\3\2\2\2\2\35\3\2\2\2\2\37\3\2\2\2\2!\3\2\2"+
		"\2\2#\3\2\2\2\2%\3\2\2\2\2\'\3\2\2\2\2)\3\2\2\2\2+\3\2\2\2\2-\3\2\2\2"+
		"\2/\3\2\2\2\2\61\3\2\2\2\2\63\3\2\2\2\2\65\3\2\2\2\2\67\3\2\2\2\29\3\2"+
		"\2\2\2;\3\2\2\2\2=\3\2\2\2\2?\3\2\2\2\2A\3\2\2\2\2C\3\2\2\2\2E\3\2\2\2"+
		"\2G\3\2\2\2\2I\3\2\2\2\2K\3\2\2\2\2M\3\2\2\2\2O\3\2\2\2\2Q\3\2\2\2\2S"+
		"\3\2\2\2\2U\3\2\2\2\2W\3\2\2\2\2Y\3\2\2\2\2[\3\2\2\2\2]\3\2\2\2\2_\3\2"+
		"\2\2\2a\3\2\2\2\2c\3\2\2\2\2e\3\2\2\2\2g\3\2\2\2\2i\3\2\2\2\2k\3\2\2\2"+
		"\2m\3\2\2\2\2o\3\2\2\2\2q\3\2\2\2\2s\3\2\2\2\2u\3\2\2\2\2w\3\2\2\2\2y"+
		"\3\2\2\2\2{\3\2\2\2\2}\3\2\2\2\3\177\3\2\2\2\5\u0090\3\2\2\2\7\u00ab\3"+
		"\2\2\2\t\u00b3\3\2\2\2\13\u00bb\3\2\2\2\r\u00c0\3\2\2\2\17\u00c4\3\2\2"+
		"\2\21\u00ca\3\2\2\2\23\u00d0\3\2\2\2\25\u00d4\3\2\2\2\27\u00d9\3\2\2\2"+
		"\31\u00df\3\2\2\2\33\u00e2\3\2\2\2\35\u00e9\3\2\2\2\37\u00ef\3\2\2\2!"+
		"\u00f8\3\2\2\2#\u0105\3\2\2\2%\u0107\3\2\2\2\'\u010e\3\2\2\2)\u0111\3"+
		"\2\2\2+\u0115\3\2\2\2-\u011a\3\2\2\2/\u0121\3\2\2\2\61\u0126\3\2\2\2\63"+
		"\u0129\3\2\2\2\65\u0130\3\2\2\2\67\u0135\3\2\2\29\u013f\3\2\2\2;\u0142"+
		"\3\2\2\2=\u0145\3\2\2\2?\u0148\3\2\2\2A\u014b\3\2\2\2C\u014e\3\2\2\2E"+
		"\u0151\3\2\2\2G\u0153\3\2\2\2I\u0155\3\2\2\2K\u0157\3\2\2\2M\u0159\3\2"+
		"\2\2O\u015b\3\2\2\2Q\u015d\3\2\2\2S\u015f\3\2\2\2U\u0161\3\2\2\2W\u0163"+
		"\3\2\2\2Y\u0165\3\2\2\2[\u0168\3\2\2\2]\u016a\3\2\2\2_\u016c\3\2\2\2a"+
		"\u016e\3\2\2\2c\u0170\3\2\2\2e\u0172\3\2\2\2g\u0174\3\2\2\2i\u0176\3\2"+
		"\2\2k\u0178\3\2\2\2m\u017a\3\2\2\2o\u017c\3\2\2\2q\u017f\3\2\2\2s\u0184"+
		"\3\2\2\2u\u01aa\3\2\2\2w\u01b6\3\2\2\2y\u01bc\3\2\2\2{\u01c8\3\2\2\2}"+
		"\u01d4\3\2\2\2\177\u0080\7\'\2\2\u0080\u0081\7\'\2\2\u0081\u0085\3\2\2"+
		"\2\u0082\u0084\13\2\2\2\u0083\u0082\3\2\2\2\u0084\u0087\3\2\2\2\u0085"+
		"\u0086\3\2\2\2\u0085\u0083\3\2\2\2\u0086\u008b\3\2\2\2\u0087\u0085\3\2"+
		"\2\2\u0088\u008a\n\2\2\2\u0089\u0088\3\2\2\2\u008a\u008d\3\2\2\2\u008b"+
		"\u0089\3\2\2\2\u008b\u008c\3\2\2\2\u008c\u008e\3\2\2\2\u008d\u008b\3\2"+
		"\2\2\u008e\u008f\b\2\2\2\u008f\4\3\2\2\2\u0090\u0091\7\61\2\2\u0091\u009d"+
		"\7,\2\2\u0092\u0096\7,\2\2\u0093\u0095\7,\2\2\u0094\u0093\3\2\2\2\u0095"+
		"\u0098\3\2\2\2\u0096\u0094\3\2\2\2\u0096\u0097\3\2\2\2\u0097\u0099\3\2"+
		"\2\2\u0098\u0096\3\2\2\2\u0099\u009c\n\3\2\2\u009a\u009c\n\4\2\2\u009b"+
		"\u0092\3\2\2\2\u009b\u009a\3\2\2\2\u009c\u009f\3\2\2\2\u009d\u009b\3\2"+
		"\2\2\u009d\u009e\3\2\2\2\u009e\u00a0\3\2\2\2\u009f\u009d\3\2\2\2\u00a0"+
		"\u00a4\7,\2\2\u00a1\u00a3\7,\2\2\u00a2\u00a1\3\2\2\2\u00a3\u00a6\3\2\2"+
		"\2\u00a4\u00a2\3\2\2\2\u00a4\u00a5\3\2\2\2\u00a5\u00a7\3\2\2\2\u00a6\u00a4"+
		"\3\2\2\2\u00a7\u00a8\7\61\2\2\u00a8\u00a9\3\2\2\2\u00a9\u00aa\b\3\2\2"+
		"\u00aa\6\3\2\2\2\u00ab\u00ac\7d\2\2\u00ac\u00ad\7q\2\2\u00ad\u00ae\7q"+
		"\2\2\u00ae\u00af\7n\2\2\u00af\u00b0\7g\2\2\u00b0\u00b1\7c\2\2\u00b1\u00b2"+
		"\7p\2\2\u00b2\b\3\2\2\2\u00b3\u00b4\7g\2\2\u00b4\u00b5\7z\2\2\u00b5\u00b6"+
		"\7v\2\2\u00b6\u00b7\7g\2\2\u00b7\u00b8\7p\2\2\u00b8\u00b9\7f\2\2\u00b9"+
		"\u00ba\7u\2\2\u00ba\n\3\2\2\2\u00bb\u00bc\7v\2\2\u00bc\u00bd\7j\2\2\u00bd"+
		"\u00be\7g\2\2\u00be\u00bf\7p\2\2\u00bf\f\3\2\2\2\u00c0\u00c1\7p\2\2\u00c1"+
		"\u00c2\7k\2\2\u00c2\u00c3\7n\2\2\u00c3\16\3\2\2\2\u00c4\u00c5\7d\2\2\u00c5"+
		"\u00c6\7t\2\2\u00c6\u00c7\7g\2\2\u00c7\u00c8\7c\2\2\u00c8\u00c9\7m\2\2"+
		"\u00c9\20\3\2\2\2\u00ca\u00cb\7h\2\2\u00cb\u00cc\7n\2\2\u00cc\u00cd\7"+
		"q\2\2\u00cd\u00ce\7c\2\2\u00ce\u00cf\7v\2\2\u00cf\22\3\2\2\2\u00d0\u00d1"+
		"\7h\2\2\u00d1\u00d2\7q\2\2\u00d2\u00d3\7t\2\2\u00d3\24\3\2\2\2\u00d4\u00d5"+
		"\7v\2\2\u00d5\u00d6\7j\2\2\u00d6\u00d7\7k\2\2\u00d7\u00d8\7u\2\2\u00d8"+
		"\26\3\2\2\2\u00d9\u00da\7e\2\2\u00da\u00db\7n\2\2\u00db\u00dc\7c\2\2\u00dc"+
		"\u00dd\7u\2\2\u00dd\u00de\7u\2\2\u00de\30\3\2\2\2\u00df\u00e0\7k\2\2\u00e0"+
		"\u00e1\7h\2\2\u00e1\32\3\2\2\2\u00e2\u00e3\7t\2\2\u00e3\u00e4\7g\2\2\u00e4"+
		"\u00e5\7v\2\2\u00e5\u00e6\7w\2\2\u00e6\u00e7\7t\2\2\u00e7\u00e8\7p\2\2"+
		"\u00e8\34\3\2\2\2\u00e9\u00ea\7h\2\2\u00ea\u00eb\7k\2\2\u00eb\u00ec\7"+
		"p\2\2\u00ec\u00ed\7c\2\2\u00ed\u00ee\7n\2\2\u00ee\36\3\2\2\2\u00ef\u00f0"+
		"\7e\2\2\u00f0\u00f1\7q\2\2\u00f1\u00f2\7p\2\2\u00f2\u00f3\7v\2\2\u00f3"+
		"\u00f4\7k\2\2\u00f4\u00f5\7p\2\2\u00f5\u00f6\7w\2\2\u00f6\u00f7\7g\2\2"+
		"\u00f7 \3\2\2\2\u00f8\u00f9\7k\2\2\u00f9\u00fa\7p\2\2\u00fa\u00fb\7v\2"+
		"\2\u00fb\"\3\2\2\2\u00fc\u00fd\7v\2\2\u00fd\u00fe\7t\2\2\u00fe\u00ff\7"+
		"w\2\2\u00ff\u0106\7g\2\2\u0100\u0101\7h\2\2\u0101\u0102\7c\2\2\u0102\u0103"+
		"\7n\2\2\u0103\u0104\7u\2\2\u0104\u0106\7g\2\2\u0105\u00fc\3\2\2\2\u0105"+
		"\u0100\3\2\2\2\u0106$\3\2\2\2\u0107\u0108\7u\2\2\u0108\u0109\7v\2\2\u0109"+
		"\u010a\7c\2\2\u010a\u010b\7v\2\2\u010b\u010c\7k\2\2\u010c\u010d\7e\2\2"+
		"\u010d&\3\2\2\2\u010e\u010f\7f\2\2\u010f\u0110\7q\2\2\u0110(\3\2\2\2\u0111"+
		"\u0112\7p\2\2\u0112\u0113\7g\2\2\u0113\u0114\7y\2\2\u0114*\3\2\2\2\u0115"+
		"\u0116\7g\2\2\u0116\u0117\7n\2\2\u0117\u0118\7u\2\2\u0118\u0119\7g\2\2"+
		"\u0119,\3\2\2\2\u011a\u011b\7u\2\2\u011b\u011c\7v\2\2\u011c\u011d\7t\2"+
		"\2\u011d\u011e\7k\2\2\u011e\u011f\7p\2\2\u011f\u0120\7i\2\2\u0120.\3\2"+
		"\2\2\u0121\u0122\7x\2\2\u0122\u0123\7q\2\2\u0123\u0124\7k\2\2\u0124\u0125"+
		"\7f\2\2\u0125\60\3\2\2\2\u0126\u0127\7v\2\2\u0127\u0128\7q\2\2\u0128\62"+
		"\3\2\2\2\u0129\u012a\7f\2\2\u012a\u012b\7q\2\2\u012b\u012c\7y\2\2\u012c"+
		"\u012d\7p\2\2\u012d\u012e\7v\2\2\u012e\u012f\7q\2\2\u012f\64\3\2\2\2\u0130"+
		"\u0131\7p\2\2\u0131\u0132\7g\2\2\u0132\u0133\7y\2\2\u0133\66\3\2\2\2\u0134"+
		"\u0136\t\5\2\2\u0135\u0134\3\2\2\2\u0136\u0137\3\2\2\2\u0137\u0135\3\2"+
		"\2\2\u0137\u0138\3\2\2\2\u0138\u013c\3\2\2\2\u0139\u013b\t\6\2\2\u013a"+
		"\u0139\3\2\2\2\u013b\u013e\3\2\2\2\u013c\u013a\3\2\2\2\u013c\u013d\3\2"+
		"\2\2\u013d8\3\2\2\2\u013e\u013c\3\2\2\2\u013f\u0140\7#\2\2\u0140\u0141"+
		"\7?\2\2\u0141:\3\2\2\2\u0142\u0143\7?\2\2\u0143\u0144\7?\2\2\u0144<\3"+
		"\2\2\2\u0145\u0146\7>\2\2\u0146\u0147\7?\2\2\u0147>\3\2\2\2\u0148\u0149"+
		"\7@\2\2\u0149\u014a\7?\2\2\u014a@\3\2\2\2\u014b\u014c\7~\2\2\u014c\u014d"+
		"\7~\2\2\u014dB\3\2\2\2\u014e\u014f\7(\2\2\u014f\u0150\7(\2\2\u0150D\3"+
		"\2\2\2\u0151\u0152\7`\2\2\u0152F\3\2\2\2\u0153\u0154\7-\2\2\u0154H\3\2"+
		"\2\2\u0155\u0156\7/\2\2\u0156J\3\2\2\2\u0157\u0158\7,\2\2\u0158L\3\2\2"+
		"\2\u0159\u015a\7\61\2\2\u015aN\3\2\2\2\u015b\u015c\7^\2\2\u015cP\3\2\2"+
		"\2\u015d\u015e\7\'\2\2\u015eR\3\2\2\2\u015f\u0160\7>\2\2\u0160T\3\2\2"+
		"\2\u0161\u0162\7@\2\2\u0162V\3\2\2\2\u0163\u0164\7#\2\2\u0164X\3\2\2\2"+
		"\u0165\u0166\7<\2\2\u0166\u0167\7?\2\2\u0167Z\3\2\2\2\u0168\u0169\7?\2"+
		"\2\u0169\\\3\2\2\2\u016a\u016b\7}\2\2\u016b^\3\2\2\2\u016c\u016d\7\177"+
		"\2\2\u016d`\3\2\2\2\u016e\u016f\7]\2\2\u016fb\3\2\2\2\u0170\u0171\7_\2"+
		"\2\u0171d\3\2\2\2\u0172\u0173\7*\2\2\u0173f\3\2\2\2\u0174\u0175\7+\2\2"+
		"\u0175h\3\2\2\2\u0176\u0177\7=\2\2\u0177j\3\2\2\2\u0178\u0179\7<\2\2\u0179"+
		"l\3\2\2\2\u017a\u017b\7\60\2\2\u017bn\3\2\2\2\u017c\u017d\7.\2\2\u017d"+
		"p\3\2\2\2\u017e\u0180\t\7\2\2\u017f\u017e\3\2\2\2\u0180\u0181\3\2\2\2"+
		"\u0181\u017f\3\2\2\2\u0181\u0182\3\2\2\2\u0182r\3\2\2\2\u0183\u0185\t"+
		"\7\2\2\u0184\u0183\3\2\2\2\u0185\u0186\3\2\2\2\u0186\u0184\3\2\2\2\u0186"+
		"\u0187\3\2\2\2\u0187\u01a8\3\2\2\2\u0188\u018a\t\b\2\2\u0189\u018b\t\t"+
		"\2\2\u018a\u0189\3\2\2\2\u018a\u018b\3\2\2\2\u018b\u018d\3\2\2\2\u018c"+
		"\u018e\t\7\2\2\u018d\u018c\3\2\2\2\u018e\u018f\3\2\2\2\u018f\u018d\3\2"+
		"\2\2\u018f\u0190\3\2\2\2\u0190\u01a9\3\2\2\2\u0191\u0195\7\60\2\2\u0192"+
		"\u0194\t\7\2\2\u0193\u0192\3\2\2\2\u0194\u0197\3\2\2\2\u0195\u0193\3\2"+
		"\2\2\u0195\u0196\3\2\2\2\u0196\u01a9\3\2\2\2\u0197\u0195\3\2\2\2\u0198"+
		"\u019c\7\60\2\2\u0199\u019b\t\7\2\2\u019a\u0199\3\2\2\2\u019b\u019e\3"+
		"\2\2\2\u019c\u019a\3\2\2\2\u019c\u019d\3\2\2\2\u019d\u019f\3\2\2\2\u019e"+
		"\u019c\3\2\2\2\u019f\u01a1\t\b\2\2\u01a0\u01a2\t\t\2\2\u01a1\u01a0\3\2"+
		"\2\2\u01a1\u01a2\3\2\2\2\u01a2\u01a4\3\2\2\2\u01a3\u01a5\t\7\2\2\u01a4"+
		"\u01a3\3\2\2\2\u01a5\u01a6\3\2\2\2\u01a6\u01a4\3\2\2\2\u01a6\u01a7\3\2"+
		"\2\2\u01a7\u01a9\3\2\2\2\u01a8\u0188\3\2\2\2\u01a8\u0191\3\2\2\2\u01a8"+
		"\u0198\3\2\2\2\u01a9t\3\2\2\2\u01aa\u01b0\7$\2\2\u01ab\u01af\n\n\2\2\u01ac"+
		"\u01ad\7^\2\2\u01ad\u01af\t\13\2\2\u01ae\u01ab\3\2\2\2\u01ae\u01ac\3\2"+
		"\2\2\u01af\u01b2\3\2\2\2\u01b0\u01ae\3\2\2\2\u01b0\u01b1\3\2\2\2\u01b1"+
		"\u01b3\3\2\2\2\u01b2\u01b0\3\2\2\2\u01b3\u01b4\7$\2\2\u01b4v\3\2\2\2\u01b5"+
		"\u01b7\t\f\2\2\u01b6\u01b5\3\2\2\2\u01b7\u01b8\3\2\2\2\u01b8\u01b6\3\2"+
		"\2\2\u01b8\u01b9\3\2\2\2\u01b9\u01ba\3\2\2\2\u01ba\u01bb\b<\2\2\u01bb"+
		"x\3\2\2\2\u01bc\u01c2\7$\2\2\u01bd\u01c1\n\n\2\2\u01be\u01bf\7^\2\2\u01bf"+
		"\u01c1\t\13\2\2\u01c0\u01bd\3\2\2\2\u01c0\u01be\3\2\2\2\u01c1\u01c4\3"+
		"\2\2\2\u01c2\u01c0\3\2\2\2\u01c2\u01c3\3\2\2\2\u01c3\u01c6\3\2\2\2\u01c4"+
		"\u01c2\3\2\2\2\u01c5\u01c7\t\r\2\2\u01c6\u01c5\3\2\2\2\u01c6\u01c7\3\2"+
		"\2\2\u01c7z\3\2\2\2\u01c8\u01ce\7$\2\2\u01c9\u01ca\7^\2\2\u01ca\u01cd"+
		"\t\13\2\2\u01cb\u01cd\n\n\2\2\u01cc\u01c9\3\2\2\2\u01cc\u01cb\3\2\2\2"+
		"\u01cd\u01d0\3\2\2\2\u01ce\u01cc\3\2\2\2\u01ce\u01cf\3\2\2\2\u01cf\u01d1"+
		"\3\2\2\2\u01d0\u01ce\3\2\2\2\u01d1\u01d2\7^\2\2\u01d2\u01d3\n\13\2\2\u01d3"+
		"|\3\2\2\2\u01d4\u01d5\13\2\2\2\u01d5~\3\2\2\2\37\2\u0085\u008b\u0096\u009b"+
		"\u009d\u00a4\u0105\u0135\u0137\u013a\u013c\u0181\u0186\u018a\u018f\u0195"+
		"\u019c\u01a1\u01a6\u01a8\u01ae\u01b0\u01b8\u01c0\u01c2\u01c6\u01cc\u01ce"+
		"\3\b\2\2";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}