/**
 * Student name: NGUYEN TRUONG LONG
 */

package bkool.checker

import bkool.parser._
import bkool.utils._
import java.io.{PrintWriter, File}
import org.antlr.v4.runtime.ANTLRFileStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree._
import scala.collection.JavaConverters._


class StaticChecker(ast:AST) {
	def check() = {
				val redecl = new RedeclaredChecker(ast)
				redecl.visitProgram(ast.asInstanceOf[Program], null)
       		    val undecl = new UndeclaredChecker(ast)
       		    undecl.visitProgram(ast.asInstanceOf[Program], null)
    }
}

class RedeclaredChecker(ast:AST) extends Visitor
{
	// Kiem tra xem co Id nao nam trong List, neu co tra va true
	def checkListId(listId: List[(String, Kind, Any)], id: String): Boolean = {
		listId match{
			case List() => false
			case head::tail => if(head._1 == id) true; else checkListId(tail, id) }
	}
	// Lay ten va kieu cua cac khai bao trong pham vi Class
	def getMemClass(ast: MemDecl): List[(String, Kind, Any)] = {
		var tmp_string: String = null
		var tmp_kind: Kind = null
		var tmp_object: Any = null
	// Tao 1 list bao gom method, vardecl, consdecl, class decl
	    if(ast.isInstanceOf[MethodDecl])
	    {
			tmp_string = ast.asInstanceOf[MethodDecl].name.toString()
			if(ast.asInstanceOf[MethodDecl].returnType != null)
				tmp_kind = Method
			else tmp_kind = SpecialMethod
			tmp_object = ast.asInstanceOf[MethodDecl]
	    }
		else
		{
			if(ast.asInstanceOf[AttributeDecl].decl.isInstanceOf[ConstDecl])
			{
				tmp_string = ast.asInstanceOf[AttributeDecl].decl.asInstanceOf[ConstDecl].id.toString()
				tmp_kind = Attribute
				tmp_object = ast.asInstanceOf[AttributeDecl].decl.asInstanceOf[ConstDecl]
			}
			else if(ast.asInstanceOf[AttributeDecl].decl.isInstanceOf[VarDecl])
			{
				tmp_string = ast.asInstanceOf[AttributeDecl].decl.asInstanceOf[VarDecl].variable.toString()
				tmp_kind = Attribute
				tmp_object = ast.asInstanceOf[AttributeDecl].decl.asInstanceOf[VarDecl]
			}
			else
			{
				tmp_string = ast.asInstanceOf[AttributeDecl].decl.asInstanceOf[ClassDecl].name.toString()
				tmp_kind = Class
				tmp_object = ast.asInstanceOf[AttributeDecl].decl.asInstanceOf[ClassDecl]
			}
		}
		List((tmp_string, tmp_kind, tmp_object))
	}

	def getListParam(ast: ParamDecl): List[(String, Kind, Any)] = {
		var tmp_string: String = ast.id.toString()
		var tmp_kind: Kind = Parameter
		var tmp_object: Any = ast.asInstanceOf[ParamDecl]
		List((tmp_string, tmp_kind, tmp_object))
	}

	// Lay ten va kieu cua cac khai bao trong pham vi Class
	def getMethodDecl(ast: Decl): List[(String, Kind, Any)] = {
		var tmp_string: String = null
		var tmp_kind: Kind = null
		var tmp_object: Any = null
	// Tao 1 list bao gom method, vardecl, consdecl, class decl
			if(ast.isInstanceOf[ConstDecl])
			{
				tmp_string = ast.asInstanceOf[ConstDecl].id.toString()
				tmp_kind = Constant
				tmp_object = ast.asInstanceOf[ConstDecl]
			}
			else if(ast.isInstanceOf[VarDecl])
			{
				tmp_string = ast.asInstanceOf[VarDecl].variable.toString()
				tmp_kind = Variable
				tmp_object = ast.asInstanceOf[VarDecl]
			}
			else
			{
				tmp_string = ast.asInstanceOf[ClassDecl].name.toString()
				tmp_kind = Class
				tmp_object = ast.asInstanceOf[ClassDecl]
			}

		List((tmp_string, tmp_kind, tmp_object))
	}

    override def visitProgram(ast: Program, c: Context) = {
    	var listClass = ast.decl.asInstanceOf[List[ClassDecl]]	// Tao mot list chua cac Class
    	var listIdClass: List[(String, Kind, Any)] = listClass.map((x: ClassDecl) => (x.name.toString(), Class, null))	// Tao mot list cac Id ten Class tu cac Class
    	var scan = listClass.map((x:ClassDecl) => x.accept(this, null))	// Neu tat ca cac Class khong co loi thi tiep tuc xet tung Class
    	var env = listIdClass.foldLeft(List[(String, Kind, Any)]())((a, b) => if(checkListId(a, b._1) || b._1 == "io") throw Redeclared(b._2,b._1)
    	else b::a)
    	null
    }

	override def visitClassDecl(ast: ClassDecl, c: Context) = {
		var listMemDecl = ast.decl // Tao mot list chua cac khai bao trong Class
     	var tmp: List[List[(String, Kind, Any)]] = List(List((ast.asInstanceOf[ClassDecl].name.toString(), Class, null))):::listMemDecl.map(x => getMemClass(x))	// Tao mot list cac ten khai bao va kieu khai bao cua no co trong mot Class
    	var listIdMemDecl: List[(String, Kind, Any)] = tmp.flatten
    	var env = listIdMemDecl.foldLeft(List[(String, Kind, Any)]())((a, b) => if(checkListId(a,b._1) && !(b._2 == SpecialMethod)) throw Redeclared(b._2,b._1) 
    	else {
    	if(b._3.isInstanceOf[MethodDecl]) visitMethodDecl(b._3.asInstanceOf[MethodDecl], null)
    	if(b._3.isInstanceOf[ClassDecl]) visitClassDecl(b._3.asInstanceOf[ClassDecl], null)
    	b::a })
    	null
	}

	override def visitMethodDecl(ast: MethodDecl, c: Context) = {
		var listParam: List[ParamDecl] = ast.param // List cac khai bao tham so
		var listDecl: List[Decl] = ast.body.asInstanceOf[Block].decl // list cac khai bao trong ham
		var tmp: List[List[(String, Kind, Any)]] = listParam.map(x => getListParam(x))
		var listIdParam: List[(String, Kind, Any)] = tmp.flatten	// List ma moi phan tu chua ten, Kind, Object cua moi khai bao tham so
    	tmp = listDecl.map(x => getMethodDecl(x))
		var listIdDecl: List[(String, Kind, Any)] = tmp.flatten	// List ma moi phan tu chua ten, Kind, Object cua moi khai bao
		var listParam_Decl: List[(String, Kind, Any)] = listIdParam:::listIdDecl // Tra ve list bao gom khai bao tham so va khai bao
		var env_decl = listParam_Decl.foldLeft(List[(String, Kind, Any)]())((a, b) => if(checkListId(a,b._1)) throw Redeclared(b._2,b._1) 
    	else b::a)	// Kiem tra xem khai bao tham so co trung voi khai bao trong than ham hay khong
    	var listStmt:List[Stmt] = ast.asInstanceOf[MethodDecl].body.asInstanceOf[Block].stmt
    	var scan = listStmt.map((x:Stmt) => x.accept(this, null))	// Quet goi ham visit tuong ung voi tung lenh trong than ham
		null
	}

	override def visitBlock(ast: Block, c: Context) = {
		var listDecl: List[Decl] = ast.decl // List cac khai bao Block
		var listStmt: List[Stmt] = ast.stmt // List cac lenh trong Block
		var tmp = listDecl.map(x => getMethodDecl(x))
		var listIdDecl: List[(String, Kind, Any)] = tmp.flatten	//Tra ve list cac khai bao
		var env_decl = listIdDecl.foldLeft(List[(String, Kind, Any)]())((a, b) => if(checkListId(a,b._1)) throw Redeclared(b._2,b._1) 
    	else b::a)	// Kiem tra xem cac khai bao trong Block co bi trung nhau khong
		var env_stmt = listStmt.map((x:Stmt) => x.accept(this, null))	//Goi den tung ham visit tuong ung voi moi lenh trong Block
		null
	}

	override def visitIf(ast: If, c: Context) = {
		var stmt1 = ast.thenStmt.accept(this, null)
		var stmt2 = ast.elseStmt
		if(stmt2 != None)
			stmt2.get.asInstanceOf[Stmt].accept(this, null)
		null
	}
	override def visitFor(ast: For, c: Context) = {
		var stmt = ast.loop.accept(this, null)
		null
	}

    override def visitVarDecl(ast: VarDecl, c: Context): Object = null
	override def visitConstDecl(ast: ConstDecl, c: Context): Object = null
	override def visitParamDecl(ast: ParamDecl, c: Context): Object = null
	override def visitAttributeDecl(ast: AttributeDecl, c: Context): Object = null
	override def visitInstance(ast: Instance.type, c: Context): Object = null
	override def visitStatic(ast: Static.type, c: Context): Object = null
	override def visitIntType(ast: IntType.type, c: Context): Object = null
	override def visitFloatType(ast: FloatType.type, c: Context): Object = null
	override def visitBoolType(ast: BoolType.type, c: Context): Object = null
	override def visitStringType(ast: StringType.type, c: Context): Object = null
	override def visitVoidType(ast: VoidType.type, c: Context): Object = null
	override def visitArrayType(ast: ArrayType, c: Context): Object = null
	override def visitClassType(ast: ClassType, c: Context): Object = null
	override def visitBinaryOp(ast: BinaryOp, c: Context): Object = null
	override def visitUnaryOp(ast: UnaryOp, c: Context): Object = null
	override def visitNewExpr(ast: NewExpr, c: Context): Object = null
	override def visitCallExpr(ast: CallExpr, c: Context): Object = null
	override def visitId(ast: Id, c: Context): Object = null
	override def visitArrayCell(ast: ArrayCell, c: Context): Object = null
	override def visitFieldAccess(ast: FieldAccess, c: Context): Object = null
	override def visitAssign(ast: Assign, c: Context): Object = null
	override def visitCall(ast: Call, c: Context): Object = null
	override def visitBreak(ast: Break.type, c: Context): Object = null
	override def visitContinue(ast: Continue.type, c: Context): Object = null
	override def visitReturn(ast: Return, c: Context): Object = null
	override def visitIntLiteral(ast: IntLiteral, c: Context): Object = null
	override def visitFloatLiteral(ast: FloatLiteral, c: Context): Object = null
	override def visitStringLiteral(ast: StringLiteral, c: Context): Object = null
	override def visitBooleanLiteral(ast: BooleanLiteral, c: Context): Object = null
	override def visitNullLiteral(ast: NullLiteral.type, c: Context): Object = null
	override def visitSelfLiteral(ast: SelfLiteral.type, c: Context): Object = null
}

class UndeclaredChecker(ast:AST) extends Visitor
{	//ParamDecl(val id: Id, val paramType: Type)
	val mem_io1: MemDecl = MethodDecl(Static, Id("readInt"), List(), IntType, Block(List(), List()))
	val mem_io2: MemDecl = MethodDecl(Static, Id("writeInt"), List(ParamDecl(Id("anArg"), IntType)), VoidType, Block(List(), List()))
	val mem_io3: MemDecl = MethodDecl(Static, Id("writeIntLn"), List(ParamDecl(Id("anArg"), IntType)), VoidType, Block(List(), List()))
	val mem_io4: MemDecl = MethodDecl(Static, Id("readFloat"), List(), FloatType, Block(List(), List()))
	val mem_io5: MemDecl = MethodDecl(Static, Id("writeFloat"), List(ParamDecl(Id("anArg"), FloatType)), VoidType, Block(List(), List()))
	val mem_io6: MemDecl = MethodDecl(Static, Id("writeFloatLn"), List(ParamDecl(Id("anArg"), FloatType)), VoidType, Block(List(), List()))
	val mem_io7: MemDecl = MethodDecl(Static, Id("readBool"), List(), BoolType, Block(List(), List()))
	val mem_io8: MemDecl = MethodDecl(Static, Id("writeBool"), List(ParamDecl(Id("anArg"), BoolType)), VoidType, Block(List(), List()))
	val mem_io9: MemDecl = MethodDecl(Static, Id("writeBoolLn"), List(ParamDecl(Id("anArg"), BoolType)), VoidType, Block(List(), List()))
	val mem_io10: MemDecl = MethodDecl(Static, Id("readStr"), List(), StringType, Block(List(), List()))
	val mem_io11: MemDecl = MethodDecl(Static, Id("writeStr"), List(ParamDecl(Id("anArg"), StringType)), VoidType, Block(List(), List()))
	val mem_io12: MemDecl = MethodDecl(Static, Id("writeStrLn"), List(ParamDecl(Id("anArg"), StringType)), VoidType, Block(List(), List()))
	val list_mem_io:List[MemDecl] = List(mem_io1):::List(mem_io2):::List(mem_io3):::List(mem_io4):::List(mem_io5):::List(mem_io6):::List(mem_io7):::List(mem_io8):::List(mem_io9):::List(mem_io10):::List(mem_io11):::List(mem_io12)
	val class_io: ClassDecl = ClassDecl(Id("io"), null, list_mem_io)

	case class env_program(var memClass : List[(String, MemDecl, SIKind, Type)], var memMethod : List[(String, Decl, Type)], var class_exe : Any, var scope_exe : Any) extends Context

	var listIdAllClass:List[String] = ast.asInstanceOf[Program].decl.map(x => x.name.toString):::List("io")	//List ten tat cac cac class
	var listAllClass:List[ClassDecl] = ast.asInstanceOf[Program].decl:::List(class_io)
	def checkIdInList(listId: List[String], id: String): Boolean = {
		listId match{
			case List() => false
			case head::tail => if(head == id) true; else if(id == null) false ; else checkIdInList(tail, id) }
	}

	def getallmemclass(ast: ClassDecl): List[(String, MemDecl, SIKind, Type)] = {
		ast.parent match {
			case null => ast.decl.map(x => getMemClass(x))	//Neu khong ton tai class cha
			case _  => ast.decl.map(x => getMemClass(x)):::getallmemclass(listAllClass.filter(_.name.toString == ast.parent.toString).head)
		}
	}

	def getType(list_id: List[(String, Type)], id: String): Type = {
		list_id match{
			case List() => null
			case head::tail => if(head._1 == id)  head._2; else if(id == null) null ; else getType(tail, id) }
	}

	def getMemClass(ast: MemDecl): (String, MemDecl, SIKind, Type) = {
		var tmp_string: String = null
		var tmp_object: MemDecl = null
		var tmp_kind: SIKind = null
		var tmp_type: Type = null
	    if(ast.isInstanceOf[MethodDecl])
	    {
			tmp_string = ast.asInstanceOf[MethodDecl].name.toString()
			tmp_kind = ast.asInstanceOf[MethodDecl].kind // sua thanh static hoac atrribute
			tmp_object = ast.asInstanceOf[MethodDecl]
			tmp_type = ast.asInstanceOf[MethodDecl].returnType
		}
		else
		{
			if(ast.asInstanceOf[AttributeDecl].decl.isInstanceOf[ConstDecl])
			{
				tmp_string = ast.asInstanceOf[AttributeDecl].decl.asInstanceOf[ConstDecl].id.toString()
				tmp_kind = ast.asInstanceOf[AttributeDecl].kind
				tmp_object = ast.asInstanceOf[AttributeDecl]
				tmp_type = ast.asInstanceOf[AttributeDecl].decl.asInstanceOf[ConstDecl].constType
			}
			if(ast.asInstanceOf[AttributeDecl].decl.isInstanceOf[VarDecl])
			{
				tmp_string = ast.asInstanceOf[AttributeDecl].decl.asInstanceOf[VarDecl].variable.toString()
				tmp_kind = ast.asInstanceOf[AttributeDecl].kind
				tmp_object = ast.asInstanceOf[AttributeDecl]
				tmp_type = ast.asInstanceOf[AttributeDecl].decl.asInstanceOf[VarDecl].varType
			}
		}
		(tmp_string, tmp_object, tmp_kind, tmp_type)
	}

	override def visitProgram(ast: Program, c: Context) = {

		ast.decl.map(x => x.accept(this, c))	//Quet tung class,neu co class cha va xet ten class cha da khai bao chua
		null
	}

	override def visitClassDecl(ast: ClassDecl, c: Context) = {
		var env = env_program(ast.decl.map(x => getMemClass(x)), List(), ast, ast)	//Truong thu 3 la class hien tai
		if(ast.parent != null)
		{
			if(!checkIdInList(listIdAllClass, ast.parent.toString)) throw Undeclared(Class, ast.parent.toString)	//Neu lop cha chua khai bao thi thong bao loi
		}

		var scan = ast.decl.map(x => x.accept(this, env))
		null
	}

	override  def visitMethodDecl(ast: MethodDecl, c: Context) = {
		var tmp = c.asInstanceOf[env_program].scope_exe
		var name = ast.asInstanceOf[MethodDecl].name.toString
		if(ast.asInstanceOf[MethodDecl].returnType.isInstanceOf[ClassType])	//Kiem tra neu kieu tra ve cua method la class thi class da khai bao chua
		{
			var type_ = ast.asInstanceOf[MethodDecl].returnType.asInstanceOf[ClassType].classType
			if(!checkIdInList(listIdAllClass, type_))
				throw Undeclared(Class, type_)
		}
		c.asInstanceOf[env_program].scope_exe = ast
		var env_ParamDecl = ast.param.foldLeft(c)((a, b) => b.accept(this,a).asInstanceOf[env_program])	//Quet tham so
		ast.body.accept(this, env_ParamDecl)	// Quet Block voi bien moi truong la ket qua tra ve chua cac tham so cua method tu viec Quet tham so
		if(ast.body.asInstanceOf[Block].stmt.filter(_.isInstanceOf[Return]) == List() && ast.returnType != VoidType && ast.returnType != null) 	// Neu kieu tra ve khong phai la void hoac khong phai phuong thuc khoi tao
			throw MethodNotReturn(ast.name.toString)
		c.asInstanceOf[env_program].scope_exe = tmp
		c.asInstanceOf[env_program].memMethod = List()	// Huy di khi chuyen sang quet MemDecl moi
		c
	}

	override def visitParamDecl(ast: ParamDecl, c: Context) = {
		var name: String = ast.id.toString()
		if(ast.asInstanceOf[ParamDecl].paramType.isInstanceOf[ClassType])
		{
			var type_ = ast.asInstanceOf[ParamDecl].paramType.asInstanceOf[ClassType].classType
			if(!checkIdInList(listIdAllClass, type_))
					throw Undeclared(Class, type_)
		}
		c.asInstanceOf[env_program].memMethod = c.asInstanceOf[env_program].memMethod.asInstanceOf[List[(String, Decl, Type)]] ::: List((name, ast, ast.paramType))
		c
	}

	override  def visitBlock(ast: Block, c: Context) = {
		var tmp = c.asInstanceOf[env_program].memMethod 	//Luu tam thoi lai list trang thai ban dau
		var env_Decl = ast.decl.foldLeft(c)((a, b) => b.accept(this,a).asInstanceOf[env_program])
		ast.stmt.map(x => x.accept(this,env_Decl))
		c.asInstanceOf[env_program].memMethod = tmp 	//tra ve nhu list cu ban dau
		null
	}

	override def visitVarDecl(ast: VarDecl, c: Context) = {
		var name:String = ast.variable.toString()
			if(ast.varType.isInstanceOf[ClassType])
			{
				var type_ = ast.varType.asInstanceOf[ClassType].classType
				if(!checkIdInList(listIdAllClass, type_))
					throw Undeclared(Class, type_)
			}
		c.asInstanceOf[env_program].memMethod = c.asInstanceOf[env_program].memMethod.asInstanceOf[List[(String, Decl, Type)]] ::: List((name, ast, ast.varType))
		c
	}

	override def visitConstDecl(ast: ConstDecl, c: Context) = {
		// class ConstDecl(val id: Id, val constType: Type, val const: Expr) extends Decl
		var name:String = ast.id.toString()
		if(ast.constType.isInstanceOf[ClassType])
		{
			var type_ = ast.constType.asInstanceOf[ClassType].classType
			if(!checkIdInList(listIdAllClass, type_))
				throw Undeclared(Class, type_)
		}
		c.asInstanceOf[env_program].memMethod = c.asInstanceOf[env_program].memMethod.asInstanceOf[List[(String, Decl, Type)]] ::: List((name, ast, ast.constType))
		var expr_type:Type = ast.const.accept(this, c).asInstanceOf[Type]	//Goi thuc thi kiem tra phan bieu thuc, if c.memMethod != null la thuoc ve method, == null la khai bao hang thuoc ve class 
		if(expr_type == IntType && ast.constType == FloatType)
		{

		}
		else if(expr_type != ast.constType)
			throw TypeMismatchInConstant(ast)
		c
	}

	override  def visitAttributeDecl(ast: AttributeDecl, c: Context) = {
		//AttributeDecl(val kind: SIKind, val decl: Decl) extends MemDecl
		var name: String = null
		if(ast.asInstanceOf[AttributeDecl].decl.isInstanceOf[ConstDecl])
			{
				name = ast.asInstanceOf[AttributeDecl].decl.asInstanceOf[ConstDecl].id.toString()
				if(ast.asInstanceOf[AttributeDecl].decl.asInstanceOf[ConstDecl].constType.isInstanceOf[ClassType])
				{
					var type_ = ast.asInstanceOf[AttributeDecl].decl.asInstanceOf[ConstDecl].constType.asInstanceOf[ClassType].classType
					if(!checkIdInList(listIdAllClass, type_))
						throw Undeclared(Class, type_)
				}
				ast.asInstanceOf[AttributeDecl].decl.asInstanceOf[ConstDecl].accept(this, c)
				//ConstDecl(val id: Id, val constType: Type, val const: Expr)
			}

		if(ast.asInstanceOf[AttributeDecl].decl.isInstanceOf[VarDecl])
			{
				name = ast.asInstanceOf[AttributeDecl].decl.asInstanceOf[VarDecl].variable.toString()
				if(ast.asInstanceOf[AttributeDecl].decl.asInstanceOf[VarDecl].varType.isInstanceOf[ClassType])
				{
					var type_ = ast.asInstanceOf[AttributeDecl].decl.asInstanceOf[VarDecl].varType.asInstanceOf[ClassType].classType
					if(!checkIdInList(listIdAllClass, type_))
						throw Undeclared(Class, type_)
				}
			}
		c
	}

	override  def visitBinaryOp(ast: BinaryOp, c: Context) = {
		// BinaryOp(op: String, left: Expr, right: Expr) extends Expr

		var left_type: Type = ast.left.accept(this, c).asInstanceOf[Type]
		var right_type: Type = ast.right.accept(this, c).asInstanceOf[Type]
		var tmp_type: Type = null	// ket qua tra ve cua bieu thuc nhi nguyen
		if(left_type == VoidType || right_type == VoidType)
		{
			throw TypeMismatchInExpression(ast)
		}
		if((ast.op == "+") || (ast.op == "-") || (ast.op == "*") || (ast.op == "<") || (ast.op == "<=") || (ast.op == ">") || (ast.op == ">="))
		{
			if((left_type == FloatType && right_type == FloatType) || (left_type == FloatType && right_type == IntType) || (left_type == IntType && right_type == FloatType))
				tmp_type = FloatType
			else if (left_type == IntType && right_type == IntType)
				tmp_type = IntType
			else throw TypeMismatchInExpression(ast)
		}
		if((ast.op == "<") || (ast.op == "<=") || (ast.op == ">") || (ast.op == ">="))
		{
			if((left_type == FloatType && right_type == FloatType) || (left_type == FloatType && right_type == IntType) || (left_type == IntType && right_type == FloatType) || (left_type == IntType && right_type == IntType))
				tmp_type = BoolType
			else throw TypeMismatchInExpression(ast)
		}
		if((ast.op == "/"))
		{
			if((left_type == FloatType && right_type == FloatType) || (left_type == FloatType && right_type == IntType) || (left_type == IntType && right_type == FloatType) || (left_type == IntType && right_type == IntType))
				tmp_type = FloatType
			else throw TypeMismatchInExpression(ast)
		}
		if((ast.op == "^"))
		{
			if((left_type == StringType) && (right_type == StringType))
				tmp_type = StringType
			else throw TypeMismatchInExpression(ast)
		}
		if((ast.op == "!=") || (ast.op == "=="))
		{
			if((left_type == BoolType) && (right_type == BoolType))
				tmp_type = BoolType
			else if((left_type == IntType) && (right_type == IntType))
				tmp_type = IntType
			else throw TypeMismatchInExpression(ast)
		}
		if((ast.op == "\\") || (ast.op == "%"))
		{
			if((left_type == IntType) && (right_type == IntType))
				tmp_type = IntType
			else throw TypeMismatchInExpression(ast)
		}
		if((ast.op == "&&") || (ast.op == "||") || (ast.op == "!"))
		{
			if((left_type == BoolType) && (right_type == BoolType))
				tmp_type = BoolType
			else throw TypeMismatchInExpression(ast)
		}
		tmp_type
	}

	override  def visitUnaryOp(ast: UnaryOp, c: Context) = {
		//  UnaryOp(op: String, body: Expr) extends Expr
		var tmp_type: Type = null
		var operand_type: Type = ast.body.accept(this, c).asInstanceOf[Type]
		if(operand_type == VoidType)
		{
			throw TypeMismatchInExpression(ast)
		}
		if((ast.op == "!"))
		{
			if(operand_type == BoolType)
				tmp_type = BoolType
			else throw TypeMismatchInExpression(ast)
		}
		if((ast.op == "+") || (ast.op == "-"))
		{
			if(operand_type == IntType)
				tmp_type = IntType
			else if (operand_type == FloatType)
				tmp_type = FloatType
			else throw TypeMismatchInExpression(ast)
		}
		tmp_type
	}

	override  def visitNewExpr(ast: NewExpr, c: Context) = {
		// NewExpr(val name: Id, val exprs: List[Expr]) extends Expr
		if(!checkIdInList(listIdAllClass, ast.name.toString))
				throw Undeclared(Class, ast.name.toString)
		ClassType(ast.name.toString)
	}

	override  def visitCallExpr(ast: CallExpr, c: Context) = {
		// CallExpr(val cName: Expr, val method: Id, val params: List[Expr])
		var flag_scope: Call = null;
		if(c.asInstanceOf[env_program].scope_exe.isInstanceOf[Call])
			flag_scope = c.asInstanceOf[env_program].scope_exe.asInstanceOf[Call]
		var return_type: Type = null 	// kieu tra ve cua bieu thuc
		if(ast.cName == SelfLiteral)
		{
			var tmp_memclass = getallmemclass(c.asInstanceOf[env_program].class_exe.asInstanceOf[ClassDecl]).filter(_._2.isInstanceOf[MethodDecl]).map(x => x._1) // Lay ten tat ca cac method cua class cha va class con
			if(!checkIdInList(tmp_memclass, ast.method.toString))
				throw Undeclared(Method, ast.method.toString)
			else 	//
			{
				//Neu la thanh vien thi kiem tra list tham so
				var tmp_method: MethodDecl = getallmemclass(c.asInstanceOf[env_program].class_exe.asInstanceOf[ClassDecl]).filter(_._2.isInstanceOf[MethodDecl]).asInstanceOf[List[MethodDecl]].filter(_.name.toString == ast.method.toString).head // method tim duoc
				if(tmp_method.returnType != VoidType)
					if(flag_scope.isInstanceOf[Call])
						throw TypeMismatchInStatement(flag_scope)

				var list_paramtype:List[Type] = tmp_method.param.map(x => x.paramType)
				var list_exprtype:List[Type] = ast.params.map(x => x.accept(this, c).asInstanceOf[Type])
				if(!(list_paramtype == list_exprtype))
					if(flag_scope.isInstanceOf[Call])
						throw TypeMismatchInStatement(flag_scope)
					else
						throw TypeMismatchInExpression(ast)
				else	// Neu khong phai la void thi kieu tra ve chinh la cua method duoc goi
					return_type = tmp_method.returnType
			}
		}
		else
		{
			if(ast.cName.isInstanceOf[Id])
			{
				if(c.asInstanceOf[env_program].memMethod != null)	//neu nam trong pham vi method, dua ra loi undecl id
				{
					var join = c.asInstanceOf[env_program].memMethod.map(x => x._1)
					join = join.foldLeft(List[String]())((a,b) => b::a)	// dao nguoc danh sach ten lai de quet tu trong ra ngoai

					if(checkIdInList(join, ast.cName.asInstanceOf[Id].toString))	// Neu co ton tai khai bao
					{
						// Kiem tra kieu cua khai bao co phai la kieu class
						var tmp_id:String = join.filter( _ == ast.cName.toString ).head	// Lay phan tu cua khai bao dau tien, tinh tu trong ra ngoai
						var tmp_decl = c.asInstanceOf[env_program].memMethod.filter(_._1 == ast.cName.toString)	// Cac phan tu cua list moi truong co ten trung voi ten da su dung khai bao
						var tmp_type:Type = null 	// kieu cua bien duoc khai bao
						if(tmp_decl != List())	// Neu co phan tu thuoc pham vi method
						{
							if(tmp_decl.head._2.isInstanceOf[VarDecl])
								tmp_type = tmp_decl.head._2.asInstanceOf[VarDecl].varType
							if(tmp_decl.head._2.isInstanceOf[ConstDecl])
								tmp_type = tmp_decl.head._2.asInstanceOf[ConstDecl].constType
							if(tmp_decl.head._2.isInstanceOf[ParamDecl])
								tmp_type = tmp_decl.head._2.asInstanceOf[ParamDecl].paramType
						}

						if(tmp_type.isInstanceOf[ClassType])	// Neu kieu cua khai bao da cho la kieu class
						{
							var tmp_class = listAllClass.filter(_.name.toString == tmp_type.asInstanceOf[ClassType].classType)	//Loc ra class co ten trung voi kieu cua khai bao class
							if(tmp_class != List())	//Neu class duoc khai bao ton tai
							{
								//Kiem tra xem a co la thanh vien cua class hay khong
								var list_memclass:List[String] = tmp_class.head.decl.filter(_.isInstanceOf[MethodDecl]).map(x => getMemClass(x)).filter(_._3 == Instance).map(x => x._1)	// list chua cac method trong class								if(!checkIdInList(list_memclass, ast.field.toString))	//Neu khong co ton tai thanh vien 
								if(!checkIdInList(list_memclass, ast.method.toString))	//Neu a khong la thanh vien
									throw Undeclared(Method, ast.method.toString)
								else //Neu a la thanh vien thi kiem tra list tham so
								{	var tmp_method: MethodDecl = tmp_class.head.decl.filter(_.isInstanceOf[MethodDecl]).asInstanceOf[List[MethodDecl]].filter(_.name.toString == ast.method.toString).head // method tim duoc

									if(tmp_method.returnType != VoidType)
										if(flag_scope.isInstanceOf[Call])
											throw TypeMismatchInStatement(flag_scope)

									var list_paramtype:List[Type] = tmp_method.param.map(x => x.paramType)
									var list_exprtype:List[Type] = ast.params.map(x => x.accept(this, c).asInstanceOf[Type])
									if(!(list_paramtype == list_exprtype))
										if(flag_scope.isInstanceOf[Call])
											throw TypeMismatchInStatement(flag_scope)
										else
											throw TypeMismatchInExpression(ast)
									else 	// kieu tra ve chinh la cua method duoc goi
										return_type = tmp_method.returnType
								}
								//Tao ra 1 list chua kieu cac tham so va so sanh
							}
						}
						else 	//Neu kieu cua khai bao khong phai la kieu class
						{
							if(flag_scope.isInstanceOf[Call])
								throw TypeMismatchInStatement(flag_scope)
							else
								throw TypeMismatchInExpression(ast)
						}
					}
					else //Neu khong ton tai khai bao x
					{
						var tmp_class = listAllClass.filter(_.name.toString == ast.cName.asInstanceOf[Id].toString)	//Loc ra class co ten trung voi x
						if(tmp_class != List())	//Neu class  ton tai
						{
							//Kiem tra xem co la thanh vien cua class hay khong
							var list_memclass:List[String] = tmp_class.head.decl.filter(_.isInstanceOf[MethodDecl]).map(x => getMemClass(x)).filter(_._3 == Static).map(x => x._1)
							if(!checkIdInList(list_memclass, ast.method.toString))	//Neu khong co ton tai thanh vien
								throw Undeclared(Method, ast.method.toString)
							else
							{
									var tmp_method: MethodDecl = tmp_class.head.decl.filter(_.isInstanceOf[MethodDecl]).asInstanceOf[List[MethodDecl]].filter(_.name.toString == ast.method.toString).head // method tim duoc
									if(tmp_method.returnType != VoidType)
										if(flag_scope.isInstanceOf[Call])
											throw TypeMismatchInStatement(flag_scope)
									var list_paramtype:List[Type] = tmp_method.param.map(x => x.paramType)
									var list_exprtype:List[Type] = ast.params.map(x => x.accept(this, c).asInstanceOf[Type])
									if(!(list_paramtype == list_exprtype))
										if(flag_scope.isInstanceOf[Call])
											throw TypeMismatchInStatement(flag_scope)
										else
											throw TypeMismatchInExpression(ast)
									else 	// kieu tra ve cua method duoc goi
										return_type = tmp_method.returnType
							}
						}
						else //Neu khong ton tai
						{
							throw Undeclared(Identifier, ast.cName.asInstanceOf[Id].toString)
						}
					}
				}
			}
		}
		return_type
	}

	override  def visitArrayCell(ast: ArrayCell, c: Context) = {
		//ArrayCell(arr: Expr, idx: Expr) extends LHS
		var tmp_type: Type = null
		if(ast.arr.isInstanceOf[Id])
		{
			if(!checkIdInList(c.asInstanceOf[env_program].memMethod.map(x => x._1), ast.arr.asInstanceOf[Id].toString))
				throw Undeclared(Identifier, ast.arr.asInstanceOf[Id].toString)
			else // Neu co ton tai khai bao cua ve trai
			{
				tmp_type = getType(c.asInstanceOf[env_program].memMethod.map(x => (x._1, x._3)), ast.arr.asInstanceOf[Id].toString)	// Tim xem trong list cac khai bao va lay kieu cua khai bao do

				if(!tmp_type.isInstanceOf[ArrayType])	// Neu khai bao cua ve trai khac kieu array thi in ra ve trai
				{
					throw TypeMismatchInExpression(ast)
				}
				else // Neu khai bao cua ve trai la kieu array thi so sanh kieu cua phan tu va chi so co phai la kieu nguyen hay khong
				{
					var tmp_index: Int = tmp_type.asInstanceOf[ArrayType].dimen.value
					if(ast.idx.accept(this, c).asInstanceOf[Type] != IntType)	//Kieu chi so khong phai la kieu nguyen thi in ra loi
						throw TypeMismatchInExpression(ast)
					else
					{
						if(ast.idx.isInstanceOf[IntLiteral])
							if(ast.idx.asInstanceOf[IntLiteral].value < 0 || ast.idx.asInstanceOf[IntLiteral].value >= tmp_index)
								throw TypeMismatchInExpression(ast)
					}
				}
			}
		}
		tmp_type.asInstanceOf[ArrayType].eleType	//Tra ve kieu co phan element cua phan tu mang neu da duoc khai bao
	}

	def getallparentclass(ast: List[ClassDecl]): List[ClassDecl] = {
		if(ast == List())
		{
			List()
		}
		else
		{
			ast.head.parent match {
				case null => ast
				case _ => ast:::getallparentclass(listAllClass.filter(_.name.toString == ast.head.parent))
			}
		}
	}

	override  def visitAssign(ast: Assign, c: Context)= {
		var left_type: Type = ast.leftHandSide.accept(this, c).asInstanceOf[Type]
		var right_type: Type = ast.expr.accept(this, c).asInstanceOf[Type]

		if(ast.leftHandSide.isInstanceOf[Id])
		{
			if(checkIdInList(c.asInstanceOf[env_program].memMethod.filter(_._2.isInstanceOf[ConstDecl]).map(x => x._1), ast.leftHandSide.asInstanceOf[Id].toString))
				throw CannotAssignToConstant(ast)
		}
		if(left_type.isInstanceOf[ClassType] && right_type.isInstanceOf[ClassType])
		{
			//getallmemclass(ast: ClassDecl): List[(String, MemDecl, SIKind, Type)]
			var listclass = getallparentclass(listAllClass.filter(_.name.toString == right_type.asInstanceOf[ClassType].classType)) // lay tat cac class cha cua ve phai
			if(!checkIdInList(listclass.map(x => x.name.toString), left_type.asInstanceOf[ClassType].classType))	//Neu ve trai khong phai la class cha cua ve phai
			{
				throw TypeMismatchInStatement(ast)
			}
		}
		else if(left_type == FloatType && right_type == IntType)	// Kieu cua ve trai la float va kieu cua ve phai int thi hop le, co the ep kieu
		{
			null
		}
		else if(left_type != right_type)
			throw TypeMismatchInStatement(ast)
		null
	}
	//FieldAccess cung la mot bieu thuc, khong truy cap method, chi truy cap atrribute
	override  def visitFieldAccess(ast: FieldAccess, c: Context) = {
		//FieldAccess(name: Expr, field: Id) extends LHS
		var return_type: Type = null
		if(ast.name == SelfLiteral)
		{
			var tmp_memclass = getallmemclass(c.asInstanceOf[env_program].class_exe.asInstanceOf[ClassDecl]).filter(_._2.isInstanceOf[AttributeDecl]).map(x => x._1) // Lay ten tat ca cac method cua class cha va class con
			if(!checkIdInList(tmp_memclass, ast.field.toString))
				throw Undeclared(Attribute, ast.field.toString)
			else 	//
			{
				//Neu a la thanh vien thi kiem tra list tham so
				var tmp_attribute = getallmemclass(c.asInstanceOf[env_program].class_exe.asInstanceOf[ClassDecl]).filter(_._2.isInstanceOf[AttributeDecl]).filter(_._1 == ast.field.toString).head // method tim duoc
					return_type = tmp_attribute._4
			}
		}
		else
		{
			if(ast.name.isInstanceOf[Id])
			{
				if(c.asInstanceOf[env_program].memMethod != null)	//nam trong method, quang loi undecl id
				{
					var join = c.asInstanceOf[env_program].memMethod.map(x => x._1)
					join = join.foldLeft(List[String]())((a,b) => b::a)	// dao nguoc danh sach ten lai de quet tu trong ra ngoai

					if(checkIdInList(join, ast.name.asInstanceOf[Id].toString))	// Neu co ton tai khai bao
					{
						// Kiem tra kieu cua khai bao co phai la kieu class
						var tmp_id:String = join.filter( _ == ast.name.toString ).head	// Lay phan tu cua khai bao dau tien, tinh tu trong ra ngoai
						var tmp_decl = c.asInstanceOf[env_program].memMethod.filter(_._1 == ast.name.toString)	//Phan tu cua list env co ten trung voi ten da su dung khai bao
						var tmp_type:Type = null 	// kieu cua bien duoc khai bao
						if(tmp_decl != List())	// Neu co phan tu thuoc pham vi method
						{
							if(tmp_decl.head._2.isInstanceOf[VarDecl])
								tmp_type = tmp_decl.head._2.asInstanceOf[VarDecl].varType
							if(tmp_decl.head._2.isInstanceOf[ConstDecl])
								tmp_type = tmp_decl.head._2.asInstanceOf[ConstDecl].constType
							if(tmp_decl.head._2.isInstanceOf[ParamDecl])
								tmp_type = tmp_decl.head._2.asInstanceOf[ParamDecl].paramType
						}

						if(tmp_type.isInstanceOf[ClassType])	// Neu kieu cua khai bao da cho la kieu class
						{
							var tmp_class = listAllClass.filter(_.name.toString == tmp_type.asInstanceOf[ClassType].classType)	//Loc ra class co ten trung voi kieu cua khai bao class
							if(tmp_class != List())	//Neu class duoc khai bao ton tai
							{
								//Kiem tra xem co la thanh vien cua class hay khong
								var list_memclass:List[String] = tmp_class.head.decl.filter(!_.isInstanceOf[MethodDecl]).map(x => getMemClass(x)).filter(_._3 == Instance).map(x => x._1)
								if(!checkIdInList(list_memclass, ast.field.toString))	//Neu khong co ton tai thanh vien
									throw Undeclared(Attribute, ast.field.toString)
								else //Neu la thanh vien thi tra ve kieu cua no
								{
									var tmp_attribute = tmp_class.head.decl.filter(!_.isInstanceOf[MethodDecl]).map(x => getMemClass(x)).filter(_._3 == Instance).head
									return_type = tmp_attribute._4
								}
							}
						}
						else throw TypeMismatchInExpression(ast) //Neu kieu cua khai bao khong phai la kieu class

					}
					else //Neu khong ton tai khai bao
					{
						var tmp_class = listAllClass.filter(_.name.toString == ast.name.asInstanceOf[Id].toString)
						if(tmp_class != List())	//Neu class  ton tai
						{
							//Kiem tra xem a co la thanh vien cua class hay khong
							var list_memclass:List[String] = tmp_class.head.decl.filter(!_.isInstanceOf[MethodDecl]).map(x => getMemClass(x)).filter(_._3 == Static).map(x => x._1)
							if(!checkIdInList(list_memclass, ast.field.toString))	//Neu khong co ton tai thanh vien
								throw Undeclared(Attribute, ast.field.toString)
							else //Neu la thanh vien thi kiem tra list tham so
							{
									var tmp_attribute = tmp_class.head.decl.filter(!_.isInstanceOf[MethodDecl]).map(x => getMemClass(x)).filter(_._3 == Static)
									if(tmp_attribute != List())
										return_type = tmp_attribute.head._4
							}
						}
						else //Neu khong ton tai class
						{
							throw Undeclared(Identifier, ast.name.asInstanceOf[Id].toString)
						}
					}
				}
			}
		}
		return_type
	}

	override  def visitId(ast: Id, c: Context) = {
		var id_type: Type = null
		if(c.asInstanceOf[env_program].scope_exe.isInstanceOf[ClassDecl])
		{
			if(!checkIdInList(c.asInstanceOf[env_program].memMethod.map(x => x._1), ast.toString))
				throw Undeclared(Identifier, ast.toString)
		}
		if(c.asInstanceOf[env_program].scope_exe.isInstanceOf[MethodDecl])
		{
			if(!checkIdInList(c.asInstanceOf[env_program].memMethod.map(x => x._1), ast.toString))
				throw Undeclared(Identifier, ast.toString)
		else
				id_type = getType(c.asInstanceOf[env_program].memMethod.map(x => (x._1, x._3)), ast.toString).asInstanceOf[Type]
		}
		id_type
	}

	override  def visitIf(ast: If, c: Context) = {
		var expr_type: Type = ast.expr.accept(this, c).asInstanceOf[Type]
		if(expr_type != BoolType)
			throw TypeMismatchInStatement(ast)
		ast.thenStmt.accept(this, c)
		if(ast.elseStmt != None)
			ast.elseStmt.get.accept(this, c)
		null
	}
  	//Lenh goi method
	override def visitCall(ast: Call, c: Context) = {
		var tmp_scope = c.asInstanceOf[env_program].scope_exe
		c.asInstanceOf[env_program].scope_exe = ast
		val tmp = CallExpr(ast.parent.asInstanceOf[Expr], ast.method.asInstanceOf[Id], ast.params.asInstanceOf[List[Expr]])
		visitCallExpr(tmp, c)
		c.asInstanceOf[env_program].scope_exe = tmp_scope
		null
	}

	override def visitFor(ast: For, c: Context) = {
		//For(val idx:String,val expr1:Expr, val up:Boolean, val expr2: Expr, val loop: Stmt) extends Stmt
		if(c.asInstanceOf[env_program].memMethod != null)	//neu nam trong pham vi method, dua ra loi undecl id
		{
					var join = c.asInstanceOf[env_program].memMethod.map(x => x._1)
					join = join.foldLeft(List[String]())((a,b) => b::a)	// dao nguoc danh sach ten lai de quet tu trong ra ngoai

					if(checkIdInList(join, ast.idx))	// Neu co ton tai khai bao
					{
						var tmp_decl = c.asInstanceOf[env_program].memMethod.filter(_._1 == ast.idx)	// Cac phan tu cua list moi truong co ten trung voi ten da su dung khai bao
						var tmp_type:Type = null 	// kieu cua bien duoc khai bao
						if(tmp_decl != List())	// Neu co phan tu thuoc pham vi method
						{
							if(tmp_decl.head._2.isInstanceOf[VarDecl])
								tmp_type = tmp_decl.head._2.asInstanceOf[VarDecl].varType
							if(tmp_decl.head._2.isInstanceOf[ConstDecl])
							{
								//if(checkIdInList(c.asInstanceOf[env_program].memMethod.filter(_._2.isInstanceOf[ConstDecl]).map(x => x._1), ast.toString))
								throw CannotAssignToConstant(Assign(Id(ast.idx), ast.expr1))
								tmp_type = tmp_decl.head._2.asInstanceOf[ConstDecl].constType
							}
							if(tmp_decl.head._2.isInstanceOf[ParamDecl])
								tmp_type = tmp_decl.head._2.asInstanceOf[ParamDecl].paramType
						}
						if(tmp_type != IntType)
							throw TypeMismatchInStatement(ast)
					}
					else  // Neu khong ton tai khong bao
						throw Undeclared(Class, ast.idx)
		}

		var expr1_type: Type = ast.expr1.accept(this, c).asInstanceOf[Type]
		var expr2_type: Type = ast.expr2.accept(this, c).asInstanceOf[Type]
		if(expr1_type != IntType || expr2_type != IntType)
			throw TypeMismatchInStatement(ast)
		ast.loop.accept(this, c)
		null
	}

	override  def visitReturn(ast: Return, c: Context) = {
		//Return(val expr: Expr) extends Stmt
		var tmp_type = ast.expr.accept(this, c).asInstanceOf[Type]
		var return_type:Type = c.asInstanceOf[env_program].scope_exe.asInstanceOf[MethodDecl].returnType.asInstanceOf[Type]
		if(tmp_type == IntType && return_type == FloatType)
		{
			null
		}
		else if(tmp_type != return_type)
			throw TypeMismatchInStatement(ast)
		null
	}

	override  def visitInstance(ast: Instance.type, c: Context): Object = null
	override  def visitStatic(ast: Static.type, c: Context): Object = null
	override  def visitIntType(ast: IntType.type, c: Context): Object  = null
	override  def visitFloatType(ast: FloatType.type, c: Context): Object = null
	override  def visitBoolType(ast: BoolType.type, c: Context): Object = null
	override  def visitStringType(ast: StringType.type, c: Context): Object = null
	override  def visitVoidType(ast: VoidType.type, c: Context): Object = null
	override  def visitArrayType(ast: ArrayType, c: Context): Object = null
	override  def visitClassType(ast: ClassType, c: Context): Object = null
	override  def visitBreak(ast: Break.type, c: Context): Object = null
	override  def visitContinue(ast: Continue.type, c: Context): Object = null

	override  def visitIntLiteral(ast: IntLiteral, c: Context) = {
		IntType //////
	}
	override  def visitFloatLiteral(ast: FloatLiteral, c: Context) = {
		FloatType
	}
	override  def visitStringLiteral(ast: StringLiteral, c: Context) = {
		//StringLiteral(val value: String) extends Literal
		StringType
	}
	override  def visitBooleanLiteral(ast: BooleanLiteral, c: Context) = {
		BoolType
	}
	override  def visitNullLiteral(ast: NullLiteral.type, c: Context): Object = null
	override  def visitSelfLiteral(ast: SelfLiteral.type, c: Context): Object = null
}