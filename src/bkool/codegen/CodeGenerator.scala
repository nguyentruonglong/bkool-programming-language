/**
 *  This file provides a simple version of code generator
 *  Student name: NGUYEN TRUONG LONG
 */

package bkool.codegen


import bkool.checker._
import bkool.utils._
import java.io.{PrintWriter, File}

case class MethodType(in:List[Type],out:Type) extends Type

class GlobalEnvironment(env:List[ClassData]) extends BaseVisitor {
    override def visitProgram(ast:Program,o:Context) = ast.decl.foldLeft(ListClass(env))((x,y) => visit(y,x).asInstanceOf[ListClass])

    override def visitClassDecl(ast:ClassDecl,o:Context) = {
      val env = o.asInstanceOf[ListClass].value
      ListClass(ClassData(ast.name.name,if (ast.parent == null) "" else ast.parent.name,
              ast.decl.foldLeft(List[Member]())((x,y) => visit(y,ListMember(x)).asInstanceOf[List[Member]]))::env)
    }
    //
    override def visitMethodDecl(ast:MethodDecl,o:Context) = {
      val env = o.asInstanceOf[ListMember].value  //value la 1 list member
      Member(ast.name.name,ast.kind,if(ast.returnType == null) SpecialMethod else Method,
            MType(ast.param.map(x=>visit(x.paramType,o)).asInstanceOf[List[DType]],if (ast.returnType != null) visit(ast.returnType,o).asInstanceOf[DType] else VType),None) ::env
    }

    //Member(val name:String,val skind:SIKind,val kind:Kind,val mtype:BKType,val value:Option[Expr])
    override def visitAttributeDecl(ast:AttributeDecl,o:Context) = {
      var tmp: Member = null
      val env = o.asInstanceOf[ListMember].value
      if(ast.decl.isInstanceOf[ConstDecl])
        tmp = Member(ast.decl.asInstanceOf[ConstDecl].id.name,ast.kind,Attribute,visit(ast.decl.asInstanceOf[ConstDecl].constType,o).asInstanceOf[BKType], Some(ast.decl.asInstanceOf[ConstDecl].const))
      else
          tmp = Member(ast.decl.asInstanceOf[VarDecl].variable.name,ast.kind,Attribute,visit(ast.decl.asInstanceOf[VarDecl].varType,o).asInstanceOf[BKType], None)
      tmp :: env
    }

    override def visitIntType(ast:IntType.type,o:Context) = IType
    override def visitFloatType(ast:FloatType.type,o:Context) = FType
    override def visitStringType(ast:StringType.type,o:Context) = SType
    override def visitBoolType(ast:BoolType.type,o:Context) = BType
    override def visitVoidType(ast:VoidType.type,o:Context) = VType
    override def visitArrayType(ast:ArrayType,o:Context) = AType(visit(ast.eleType,o).asInstanceOf[PType],ast.dimen.value)
    override def visitClassType(ast:ClassType,o:Context) = CType(ast.classType)
}

object CodeGenerator extends Utils {
  def init() = {
    val mem = List( Member("readInt",Static,Method,MType(List(),IType),None),
                      Member("writeInt",Static,Method,MType(List(IType),VType),None),
                      Member("writeIntLn",Static,Method,MType(List(IType),VType),None),
                      Member("readFloat",Static,Method,MType(List(),FType),None),
                      Member("writeFloat",Static,Method,MType(List(FType),VType),None),
                      Member("writeFloatLn",Static,Method,MType(List(FType),VType),None),
                      Member("readBool",Static,Method,MType(List(),BType),None),
                      Member("writeBool",Static,Method,MType(List(BType),VType),None),
                      Member("writeBoolLn",Static,Method,MType(List(BType),VType),None),
                      Member("readStr",Static,Method,MType(List(),SType),None),
                      Member("writeStr",Static,Method,MType(List(SType),VType),None),
                      Member("writeStrLn",Static,Method,MType(List(SType),VType),None)
                    )
    ClassData("io","",mem)
  }
  def check(ast:AST,dir:File) = {

    val io = init()
    val ge = new GlobalEnvironment(List(io))
    val sl = ge.visit(ast,null).asInstanceOf[ListClass]

    val gc = new CodeGenVisitor(ast,sl.value,dir)

    gc.visit(ast, null);

  }
}



trait BKType
trait DType extends BKType
trait PType extends DType
case object IType extends PType
case object FType extends PType
case object VType extends PType
case object SType extends PType
case object BType extends PType
case object NType extends PType
case class CType(cname:String) extends PType
case class AType(memtype:PType,dim:Int) extends DType
case class MType(partype:List[DType],rettype:DType) extends BKType

case class Member(val name:String,val skind:SIKind,val kind:Kind,val mtype:BKType,val value:Option[Expr])
case class ClassData(val cname:String,val pname:String,val mem:List[Member])

case class ListMember(value:List[Member]) extends Context
case class ListClass(value:List[ClassData]) extends Context

case class SuperClass(parent:String) extends Context
//Dung trong cac field
case class SubContext(emit:Emitter,classname:String,parent:String,decl:List[Decl]) extends Context
//Dung cho cac khai bao trong method
case class SubBody(emit:Emitter,classname:String,frame:Frame,sym:List[(String,Type,Val)]) extends Context
//Dung trong bieu thuc
class Access(val emit:Emitter,val classname:String,val frame:Frame,val sym:List[(String,Type,Val)],val isLeft:Boolean,val isFirst:Boolean) extends Context

trait Val
  case class Index(value:Int) extends Val
  case class Const(value:Expr) extends Val
  case object StringBuff extends Type
  case object NullType extends Type



  class CodeGenVisitor(astTree: AST, env: List[ClassData], dir: File) extends BaseVisitor with Utils {

    override def visitProgram(ast: Program, c: Context) = ast.decl.map(visit(_, c))

    override def visitClassDecl(ast: ClassDecl, o: Context) = {
    val path = dir.getPath()
    val emit = new Emitter(path + "/" + ast.name.name + ".j")

    val lst = env.filter(x => x.cname == ast.name.name).head.mem

    if (ast.parent == null) {
        emit.printout(emit.emitPROLOG(ast.name.name, "java.lang.Object"))
        ast.decl.map(x => visit(x, SubContext(emit, ast.name.name, "java.lang.Object", List())))
      } else {
        emit.printout(emit.emitPROLOG(ast.name.name, ast.parent.name))
        ast.decl.map(x => visit(x, SubContext(emit, ast.name.name, ast.parent.name, List())))
    }

    val defaultConstructor = ast.decl.filter(x => x.isInstanceOf[MethodDecl] && x.asInstanceOf[MethodDecl].name.name == ast.name.name)
    if(defaultConstructor.isEmpty) {
        // generate default constructor
        genMETHOD(ast.name.name,
          MethodDecl(Instance, ast.name, List(), null, Block(List(), List())), o, List(), new Frame("<init>", VoidType), emit)
    }

    emit.emitEPILOG()
    o
  }

     /** generate code for default constructor
   *  @param classname the name of the enclosing class
   *  @param lst the list of instance attributes (array type or immutable) that need to initialize
   *  @param frame the frame where the initialization happen
   *  @param v the visitor that visits the sub-node to generate code
   */
  def genMETHOD(classname:String,consdecl:MethodDecl,o:Context,lst:List[Decl],frame:Frame,emit:Emitter) = {

    val isInit = consdecl.returnType == null
    val isMain = consdecl.kind == Static && consdecl.name.name == "main" && consdecl.param.length == 0 && consdecl.returnType == VoidType
    val returnType = if (isInit) VoidType else consdecl.returnType
    val methodName = if (isInit) "<init>" else consdecl.name.name
    val param = if (isMain) List(ParamDecl(Id("args"), ArrayType(IntLiteral(0),StringType))) else consdecl.param
    val mtype =  MethodType(param.map(_.paramType),returnType)

    emit.printout(emit.emitMETHOD(methodName, mtype, consdecl.kind==Static,frame));
    frame.enterScope(true);

    // Generate code for parameter declarations
    if (consdecl.kind == Instance) emit.printout(emit.emitVAR(frame.getNewIndex,"this",ClassType(classname),frame.getStartLabel,frame.getEndLabel,frame))
    val env = param.foldLeft(List[(String,Type,Val)]())((y,x) => visit(x,SubBody(emit,classname,frame,y)).asInstanceOf[List[(String,Type,Val)]])
    val body = consdecl.body.asInstanceOf[Block]

    //Generate code for local declarations
    val newenv = body.decl.foldLeft(List[(String,Type,Val)]())((y,x) => visit(x,SubBody(emit,classname,frame,y)).asInstanceOf[List[(String,Type,Val)]])
    emit.printout(emit.emitLABEL(frame.getStartLabel(),frame))
    //Generate code for statements
    if (isInit) {
      emit.printout(emit.emitREADVAR("this",ClassType(classname),0,frame))
      emit.printout(emit.emitINVOKESPECIAL(frame))
    }
    body.stmt.map(x=>visit(x,SubBody(emit,classname,frame,env++newenv)))

    emit.printout(emit.emitLABEL(frame.getEndLabel(),frame))
    if (returnType == VoidType) emit.printout(emit.emitRETURN(VoidType,frame));
    emit.printout(emit.emitENDMETHOD(frame));
    frame.exitScope();

  }

  /* /**   generate the field (static) directive for a class mutable or immutable attribute.
  * @param lexeme the name of the attribute.
  * @param in the type of the attribute.
  * @param isFinal true in case of constant; false otherwise
  */
  def emitATTRIBUTE(lexeme:String, kind:SIKind, in:Type, isFinal:Boolean, value:String) */

  override def visitAttributeDecl(ast: AttributeDecl, o: Context) = {
    val ctxt = o.asInstanceOf[SubContext]
    val emit = ctxt.emit
    if(ast.decl.isInstanceOf[ConstDecl])
    {
      val tmp:ConstDecl = ast.decl.asInstanceOf[ConstDecl]
      emit.printout(emit.emitATTRIBUTE(tmp.id.name, ast.kind, tmp.constType, true, emit.getConst(tmp.const.asInstanceOf[Literal])._1));
    }
    else
    {
      val tmp:VarDecl = ast.decl.asInstanceOf[VarDecl]
      emit.printout(emit.emitATTRIBUTE(tmp.variable.name, ast.kind, tmp.varType, false, null));
    }
    o
  }

  override def visitMethodDecl(ast:MethodDecl,o:Context) = {
    val subctxt = o.asInstanceOf[SubContext]
    val emit = subctxt.emit
    val frame = new Frame(ast.name.name,ast.returnType)
    genMETHOD(subctxt.classname,ast,o,subctxt.decl,frame,emit)
    o
  }

  override def visitParamDecl(ast:ParamDecl,o:Context) = {
    val ctxt = o.asInstanceOf[SubBody]
    val emit = ctxt.emit
    val frame = ctxt.frame
    val env = ctxt.sym
    val idx = frame.getNewIndex
    emit.printout(emit.emitVAR(idx,ast.id.name,ast.paramType,frame.getStartLabel(),frame.getEndLabel(),frame))
    env :+ (ast.id.name,ast.paramType,Index(idx))
  }

  override def visitVarDecl(ast: VarDecl, o: Context) = {
    val ctxt = o.asInstanceOf[SubBody]
    val emit = ctxt.emit
    val frame = ctxt.frame
    val env = ctxt.sym
    val idx = frame.getNewIndex
    emit.printout(emit.emitVAR(idx,ast.variable.name,ast.varType,frame.getStartLabel(),frame.getEndLabel(),frame))
    env :+ (ast.variable.name,ast.varType,Index(idx))
  }

  override def visitConstDecl(ast: ConstDecl, o: Context) = {
    val ctxt = o.asInstanceOf[SubBody]
    val emit = ctxt.emit
    val frame = ctxt.frame
    val env = ctxt.sym
    val idx = frame.getNewIndex
    emit.printout(emit.emitVAR(idx,ast.id.name,ast.constType,frame.getStartLabel(),frame.getEndLabel(),frame))
    val expr = visit(ast.const, new Access(emit, ctxt.classname, frame, env, false, true)).asInstanceOf[(String, Type)]
    if(ast.constType == FloatType && expr._2 == IntType)
    {
      emit.printout(expr._1)
      emit.printout(emit.emitI2F(frame))

    }
    if(ast.constType == expr._2)
    {
      emit.printout(expr._1)
    }
    emit.printout(emit.emitWRITEVAR(ast.id.name,ast.constType,idx,frame))
    env :+ (ast.id.name,ast.constType,Index(idx))
  }

  //NewExpr(val name: Id, val exprs: List[Expr])
    override def visitNewExpr(ast: NewExpr, c: Context) = {
    val subBody = c.asInstanceOf[Access]
    val sym = subBody.sym
    val emit = subBody.emit
    val frame = subBody.frame

    val str1 = emit.emitNEW(ClassType(ast.name.name), frame)

    val cl = env.filter(x => x.cname == ast.name.name).head
    val methods = cl.mem.filter(x => x.name == ast.name.name)
    val in = ast.exprs.foldLeft(("", List[Type]()))((y, x) => {
      var (str1, typ1) = visit(x, new Access(emit, subBody.classname, frame, subBody.sym, false, true)).asInstanceOf[(String, Type)]

      val index = y._2.length
      val method = methods.head
      if (method.mtype.asInstanceOf[MType].partype(index) == FType && typ1 == IntType) {
        str1 += emit.emitI2F(frame)
        typ1 = FloatType
      }

      (y._1 + str1, y._2 :+ typ1)
    })
    val mType = MethodType(in._2, VoidType)
    val str2 = emit.emitINVOKESPECIAL(ast.name.name + "/" + "<init>", mType, frame)

    (str1 + in._1 + str2, ClassType(ast.name.name))
  }

  override def visitBlock(ast: Block, c: Context) = {
    val subBody = c.asInstanceOf[SubBody]
    val sym = subBody.sym
    val emit = subBody.emit
    val frame = subBody.frame
    val classname = subBody.classname

    frame.enterScope(false)

    val newenv = ast.decl.foldLeft(List[(String, Type, Val)]())((y, x) => visit(x, SubBody(emit, classname, frame, y)).asInstanceOf[List[(String, Type, Val)]])
    emit.printout(emit.emitLABEL(frame.getStartLabel(), frame))

    /*newenv.foreach(x => {
      if (x._3.isInstanceOf[Const]) {
        emit.printout(emit.emitPUSHCONST(emit.getConst(x._3.asInstanceOf[Const].value.asInstanceOf[Literal])._1, x._2, frame))
        emit.printout(emit.emitWRITEVAR(x._1, x._2, x._3.asInstanceOf[Const].value, frame))
      }
    })*/

    ast.stmt.map(x => visit(x, SubBody(emit, classname, frame, sym ++ newenv)))

    emit.printout(emit.emitLABEL(frame.getEndLabel(), frame))
    frame.exitScope();
    c
  }


  override def visitUnaryOp(ast:UnaryOp,o:Context) = {
    val ctxt = o.asInstanceOf[Access]
    val emit = ctxt.emit
    val frame = ctxt.frame
    val nenv = ctxt.sym
    val (str,typ) = visit(ast.body,new Access(emit,ctxt.classname,frame,nenv,false,true)).asInstanceOf[(String,Type)]

    (ast.op) match {
      case ("!") => {
        (str+emit.emitNOT(BoolType,frame),BoolType)
      }
      case("-") => {
          if(typ == IntType)
            (str+emit.emitNEGOP(IntType,frame), IntType)
          else
            (str+emit.emitNEGOP(FloatType,frame), FloatType)
      }
    }
  }

  override def visitBinaryOp(ast:BinaryOp,o:Context) = {
    val ctxt = o.asInstanceOf[Access]
    val emit = ctxt.emit
    val frame = ctxt.frame
    val nenv = ctxt.sym
    val (str,typ) = visit(ast.left,new Access(emit,ctxt.classname,frame,nenv,false,true)).asInstanceOf[(String,Type)]
    val (str1,typ1) = visit(ast.right,new Access(emit,ctxt.classname,frame,nenv,false,true)).asInstanceOf[(String,Type)]

    (ast.op) match {
      case ("+" | "-" ) => {
        if(typ == IntType && typ1 == IntType)
            (str+str1+emit.emitADDOP(ast.op,IntType,frame),IntType)
        else if(typ == IntType && typ1 == FloatType)
            (str+emit.emitI2F(frame)+str1+emit.emitADDOP(ast.op,FloatType,frame),FloatType)
        else if(typ == FloatType && typ1 == IntType)
            (str+str1+emit.emitI2F(frame)+emit.emitADDOP(ast.op,FloatType,frame),FloatType)
        else
            (str+str1+emit.emitADDOP(ast.op,FloatType,frame),FloatType)
      }
      case ("*") => {
        if(typ == IntType && typ1 == IntType)
            (str+str1+emit.emitMULOP(ast.op,IntType,frame),IntType)
        else if(typ == IntType && typ1 == FloatType)
            (str+emit.emitI2F(frame)+str1+emit.emitMULOP(ast.op,FloatType,frame),FloatType)
        else if(typ == FloatType && typ1 == IntType)
            (str+str1+emit.emitI2F(frame)+emit.emitMULOP(ast.op,FloatType,frame),FloatType)
        else
            (str+str1+emit.emitMULOP(ast.op,FloatType,frame),FloatType)
      }
      case ("/") => {
            if(typ == IntType && typ1 == IntType)
                    (str+emit.emitI2F(frame)+str1+emit.emitI2F(frame)+emit.emitMULOP(ast.op,FloatType,frame),FloatType) ///////
            else if(typ == IntType && typ1 == FloatType)
                    (str+emit.emitI2F(frame)+str1+emit.emitMULOP(ast.op,FloatType,frame),FloatType)
            else if(typ == FloatType && typ1 == IntType)
                    (str+str1+emit.emitI2F(frame)+emit.emitMULOP(ast.op,FloatType,frame),FloatType)
            else
                    (str+str1+emit.emitMULOP(ast.op,FloatType,frame),FloatType)
      }
      case ("\\") => (str+str1+emit.emitMULOP(ast.op,IntType,frame),IntType)
      case ("%") => (str+str1+emit.emitMOD(frame),IntType)
      case (">" | ">=" | "<" | "<=") => if(typ == IntType && typ1 == IntType)
                                                  (str+str1+emit.emitREOP(ast.op, IntType,frame), BoolType)
                                        else
                                                  (str+str1+emit.emitREOP(ast.op, FloatType,frame), BoolType)
      case ("!=" | "==") => if(typ == IntType && typ1 == IntType)
                                        (str+str1+emit.emitREOP(ast.op, IntType,frame), BoolType)
                            else if(typ == BoolType && typ1 == BoolType)
                                        (str+str1+emit.emitREOP(ast.op, BoolType,frame), BoolType)
                            else
                                        (str+str1+emit.emitREOP(ast.op,FloatType,frame), BoolType)
      case ("&&") => (str+str1+emit.emitANDOP(frame), BoolType)
      case ("||") => (str+str1+emit.emitOROP(frame), BoolType)
    }
  }

override def visitFor(ast: For, c: Context) = {
    val subBody = c.asInstanceOf[SubBody]
    val sym = subBody.sym
    val emit = subBody.emit
    val frame = subBody.frame
    val classname = subBody.classname

    frame.enterLoop()
    val eIndex = frame.getNewIndex()
    emit.printout(emit.emitVAR(eIndex, "", IntType, frame.getContinueLabel(), frame.getBreakLabel(), frame))
    val index = frame.getNewIndex()
    emit.printout(emit.emitVAR(index, ast.idx, IntType, frame.getContinueLabel(), frame.getBreakLabel(), frame))


    val newenv = sym :+ (ast.idx, IntType, Index(index))

    emit.printout(visit(ast.expr1, new Access(emit, classname, frame, newenv, false, true)).asInstanceOf[(String, Type)]._1)
    emit.printout(emit.emitWRITEVAR("", IntType, index, frame))
    emit.printout(visit(ast.expr2, new Access(emit, classname, frame, newenv, false, true)).asInstanceOf[(String, Type)]._1)
    emit.printout(emit.emitWRITEVAR("", IntType, eIndex, frame))

    emit.printout(emit.emitLABEL(frame.getContinueLabel(), frame))
    emit.emitForCondition(index, eIndex, frame, ast.up)

    visit(ast.loop, SubBody(emit, classname, frame, newenv))


    emit.emitIInc(index, if(ast.up) 1 else -1)
    emit.printout(emit.emitGOTO(frame.getContinueLabel(), frame))
    emit.printout(emit.emitLABEL(frame.getBreakLabel(), frame))
    frame.exitLoop()
    c
  }

  override def visitBreak(ast: Break.type, c: Context) = {
    val subBody = c.asInstanceOf[SubBody]
    val emit = subBody.emit
    val frame = subBody.frame

    emit.printout(emit.emitGOTO(frame.getBreakLabel(), frame))
  }

  override def visitContinue(ast: Continue.type, c: Context) = {
    val subBody = c.asInstanceOf[SubBody]
    val emit = subBody.emit
    val frame = subBody.frame

    emit.printout(emit.emitGOTO(frame.getContinueLabel(), frame))
  }

  override def visitReturn(ast: Return, c: Context) = {
    val subBody = c.asInstanceOf[SubBody]
    val frame = subBody.frame
    val emit = subBody.emit

    val (str, typ) = visit(ast.expr, new Access(emit, subBody.classname, frame, subBody.sym, false, true)).asInstanceOf[(String, Type)]
    emit.printout(str)
    if (typ == IntType && frame.returnType == FloatType) {
      emit.printout(emit.emitI2F(frame))
    }

    emit.printout(emit.emitGOTO(1, frame))
  }

  //If(val expr: Expr, val thenStmt: Stmt, val elseStmt: Option[Stmt]) extends Stmt
  override def visitIf(ast: If, o: Context) = {
    val ctxt = o.asInstanceOf[SubBody]
    val emit = ctxt.emit
    val frame = ctxt.frame
    val env = ctxt.sym
    val expr = visit(ast.expr,new Access(emit,ctxt.classname,frame,env,false,true)).asInstanceOf[(String,Type)]
    emit.printout(expr._1)

    val label1 = frame.getNewLabel()
    val label2 = frame.getNewLabel()
    emit.printout(emit.jvm.emitIFEQ(label1))
    visit(ast.thenStmt,o)
    emit.printout(emit.emitGOTO(label2,frame)+emit.emitLABEL(label1,frame))
    ast.elseStmt match {
      case Some(c) => visit(c,o)
      case None => o
    }
    emit.printout(emit.emitLABEL(label2,frame))
    o
  }

  override def visitAssign(ast: Assign, c: Context) = {
    val ctxt = c.asInstanceOf[SubBody]
    val emit = ctxt.emit
    val frame = ctxt.frame
    val sym = ctxt.sym

    if (ast.leftHandSide.isInstanceOf[Id]) {
      val expr = visit(ast.expr, new Access(emit, ctxt.classname, frame, sym, false, true)).asInstanceOf[(String, Type)]
      val left_2 = visit(ast.leftHandSide, new Access(emit, ctxt.classname, frame, sym, true, true)).asInstanceOf[(String, Type)]
      if (expr._2 == left_2._2) {
        emit.printout(expr._1)
        emit.printout(left_2._1)
      }
      else
      {
        emit.printout(expr._1)
        emit.printout(emit.emitI2F(frame))
        emit.printout(left_2._1)
      }
    }
    else
    {
      val left_1 = visit(ast.leftHandSide, new Access(emit, ctxt.classname, frame, sym, true, true)).asInstanceOf[(String, Type)] //isFirst
      val expr = visit(ast.expr, new Access(emit, ctxt.classname, frame, sym, false, true)).asInstanceOf[(String, Type)]
      val left_2 = visit(ast.leftHandSide, new Access(emit, ctxt.classname, frame, sym, true, false)).asInstanceOf[(String, Type)]

      if (expr._2 == IntType && left_1._2 == FloatType)
      {
        emit.printout(left_1._1)
        emit.printout(expr._1)
        emit.printout(emit.emitI2F(frame))
        emit.printout(left_2._1)
      }
      else
      {
        emit.printout(left_1._1)
        emit.printout(expr._1)
        emit.printout(left_2._1)
      }
    }
  }

  def bktypetotype(in: BKType): Type =
  {
    var tmp: Type = null
    if(in == IType)
      tmp = IntType
    else if(in == BType)
      tmp = BoolType
    else if(in == FType)
      tmp = FloatType
    else if(in == SType)
      tmp = StringType
    else
      tmp = VoidType
    tmp
  }



  //FieldAccess(name: Expr, field: Id) extends LHS
  override def visitFieldAccess(ast: FieldAccess, c: Context) = {
    val access = c.asInstanceOf[Access]
    val isLeft = access.isLeft
    val isFirst = access.isFirst
    val frame = access.frame
    val emit = access.emit
    val syms = access.sym

    if(isLeft && isFirst) {            //lan 1 ben trai
      visit(ast.name, new Access(emit, access.classname, frame, syms, false, true)).asInstanceOf[(String, ClassType)]
    } else if (isLeft && !isFirst) {    //lan 2 ben trai
      val typ = visitFieldAccess(ast, new Access(emit, access.classname, frame, syms, true, true)).asInstanceOf[(String, ClassType)]._2
      val mems = astTree.asInstanceOf[Program].decl.filter(x => x.name.name == typ.classType).head.decl
      val vars = mems.filter(x => x.isInstanceOf[AttributeDecl]).filter(x => x.asInstanceOf[AttributeDecl].decl.isInstanceOf[VarDecl])
      val field = vars.filter(x => x.asInstanceOf[AttributeDecl].decl.asInstanceOf[VarDecl].variable.name == ast.field.name).head.asInstanceOf[AttributeDecl]

      if(field.kind == Static) {
        (emit.emitPUTSTATIC(typ.classType + "." + ast.field.name, field.decl.asInstanceOf[VarDecl].varType, frame), field.decl.asInstanceOf[VarDecl].varType, 1)
      } else {
        (emit.emitPUTFIELD(typ.classType + "." + ast.field.name, field.decl.asInstanceOf[VarDecl].varType, frame), field.decl.asInstanceOf[VarDecl].varType, 0)
      }
    } else {                            //ben phai
      val (str1, typ1) = visit(ast.name, access).asInstanceOf[(String, ClassType)]
      val attrs = astTree.asInstanceOf[Program].decl.filter(x => x.name.name == typ1.classType).head.decl.filter(x => x.isInstanceOf[AttributeDecl])
      val field = attrs.filter(x => {
        x.asInstanceOf[AttributeDecl].decl match {
          case y: ConstDecl => {
            y.id.name == ast.field.name
          }
          case y: VarDecl => {
            y.variable.name == ast.field.name
          }
        }
      }).head.asInstanceOf[AttributeDecl]

      field.decl match  {
        case x: ConstDecl => {
          val str2 = emit.emitPUSHCONST(emit.getConst(x.const.asInstanceOf[Literal])._1, x.constType, frame)
          (str2, x.constType)
        }
        case x: VarDecl => {
          if(field.kind == Static) {
            val str2 = emit.emitGETSTATIC(str1 + "." + x.variable.name, x.varType, frame)
            (str2, x.varType)
          } else {
            val str2 = emit.emitGETFIELD(access.classname + "." + x.variable.name, x.varType, frame)
            (str1 + str2, x.varType)
          }
        }
      }
    }
  }

  override def visitCallExpr(ast: CallExpr, c: Context) = {
    val access = c.asInstanceOf[Access]
    val isLeft = access.isLeft
    val isFirst = access.isFirst
    val frame = access.frame
    val emit = access.emit

    val (str, typ) = visit(ast.cName, new Access(emit, access.classname, frame, access.sym, false, true)).asInstanceOf[(String, Type)]

    val currentClass = astTree.asInstanceOf[Program].decl.filter(x => x.name.name == typ.asInstanceOf[ClassType].classType).head
    val methods = currentClass.decl.filter(x => x.isInstanceOf[MethodDecl]).asInstanceOf[List[MethodDecl]]
    val method = methods.filter(x => x.name.name == ast.method.name).head

    val in = ast.params.foldLeft(("", List[Type]()))((y, x) => {
      var (str1, typ1) = visit(x, new Access(emit, access.classname, frame, access.sym, false, true)).asInstanceOf[(String, Type)]
      val index = y._2.length
      if (method.param(index).paramType == FloatType && typ1 == IntType) {
        str1 += emit.emitI2F(frame)
        typ1 = FloatType
      }

      (y._1 + str1, y._2 :+ typ1)
    })

    if (method.kind == Static) {
      val s = emit.emitINVOKESTATIC(str + "/" + ast.method.name, MethodType(in._2, method.returnType), frame)
      (in._1 + s, method.returnType)
    } else {
      val s = emit.emitINVOKEVIRTUAL(typ.asInstanceOf[ClassType].classType + "/" + ast.method.name, MethodType(in._2, method.returnType), frame)
      (str + in._1 + s, method.returnType)
    }
  }

  override def visitCall(ast: Call, o: Context) = {
    val ctxt = o.asInstanceOf[SubBody]
    val emit = ctxt.emit
    val frame = ctxt.frame
    val nenv = ctxt.sym
    val (str, typ) = visit(ast.parent, new Access(emit, ctxt.classname, frame, nenv, false, true)).asInstanceOf[(String, Type)]

    val currentClass = env.filter(x => x.cname == typ.asInstanceOf[ClassType].classType).head
    val methods = currentClass.mem.filter(x => x.mtype.isInstanceOf[MType])
    val method = methods.filter(x => x.name == ast.method.name).head
    if(method.skind != Static) {
      emit.printout(str)
    }

    val in = ast.params.foldLeft(("", List[Type]()))((y, x) => {
      var (str1, typ1) = visit(x, new Access(emit, ctxt.classname, frame, nenv, false, true)).asInstanceOf[(String, Type)]
      val index = y._2.length
      if (method.mtype.asInstanceOf[MType].partype(index) == FType && typ1 == IntType) {
        str1 += emit.emitI2F(frame)
        typ1 = FloatType
      }
      (y._1 + str1, y._2 :+ typ1)
    })
    emit.printout(in._1)

    if (method.skind == Static) {
      emit.printout(emit.emitINVOKESTATIC(str + "/" + ast.method.name, MethodType(in._2, VoidType), frame))
    } else {
      emit.printout(emit.emitINVOKEVIRTUAL(typ.asInstanceOf[ClassType].classType + "/" + ast.method.name, MethodType(in._2, VoidType), frame))
    }
    o
  }

  override def visitId(ast:Id,o:Context) = {
    val ctxt = o.asInstanceOf[Access]
    val emit = ctxt.emit
    val frame = ctxt.frame
    val sym = ctxt.sym
    var tmp :(String, Type) = (null, null)
    lookup(ast.name,sym,(x:(String,Type,Val))=>x._1) match {
    //case None =>  throw Undeclared(Identifier,ast.name)
      case None => lookup(ast.name,env,(y:ClassData) => y.cname) match {
        case Some(c) => tmp = (ast.name,ClassType(ast.name))
        case None => throw Undeclared(Identifier,ast.name)
      }
    case Some(c) =>
      if(c._3.isInstanceOf[Const])
         tmp = visit(c._3.asInstanceOf[Const].value, new Access(emit,ctxt.classname,frame,sym,false,true)).asInstanceOf[(String,Type)]
      else
      {
        if(!ctxt.isLeft)
          tmp = (emit.emitREADVAR(c._1, c._2,c._3.asInstanceOf[Index].value, frame),c._2)
        else
          tmp = (emit.emitWRITEVAR(c._1, c._2,c._3.asInstanceOf[Index].value, frame),c._2)
      }
    }
    tmp
  }


  override def visitIntLiteral(ast:IntLiteral,o:Context) = {
    val ctxt = o.asInstanceOf[Access]
    val emit = ctxt.emit
    val frame = ctxt.frame
    (emit.emitPUSHICONST(ast.value, frame), IntType)
  }

  override def visitFloatLiteral(ast:FloatLiteral,o:Context) = {
    val ctxt = o.asInstanceOf[Access]
    val emit = ctxt.emit
    val frame = ctxt.frame
    (emit.emitPUSHFCONST(ast.value.toString, frame),FloatType)
  }

    override def visitStringLiteral(ast: StringLiteral, o: Context) = {
    val ctxt = o.asInstanceOf[Access]
    val emit = ctxt.emit
    val frame = ctxt.frame
    (emit.emitPUSHCONST(ast.value, StringType, frame), StringType)
  }

  override def visitBooleanLiteral(ast: BooleanLiteral, o: Context) = {
    val ctxt = o.asInstanceOf[Access]
    val emit = ctxt.emit
    val frame = ctxt.frame
    (emit.emitPUSHCONST(ast.value.toString, BoolType, frame), BoolType)
  }

  override def visitNullLiteral(ast: NullLiteral.type, o: Context) = {
    val ctxt = o.asInstanceOf[Access]
    val emit = ctxt.emit
    val frame = ctxt.frame
    (emit.emitPUSHCONST("", NullType, frame), NullType)
  }

}
