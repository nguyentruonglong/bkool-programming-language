/**
 * Student name: NGUYEN TRUONG LONG
 */

package bkool.astgen
import org.antlr.v4.runtime.Token
import org.antlr.v4.runtime.CommonTokenStream
import org.antlr.v4.runtime.ParserRuleContext
import java.io.{PrintWriter,File}
import org.antlr.v4.runtime.ANTLRFileStream
import bkool.utils._
import scala.collection.JavaConverters._
import org.antlr.v4.runtime.tree._
import bkool.parser._

class ASTGeneration extends BKOOLBaseVisitor[Object] {
	//program : (class_declaration)+ EOF;
	override def visitProgram(ctx: BKOOLParser.ProgramContext) ={
  		val tmp = ctx.class_declaration.asScala.toList.map(x => x.accept(this).asInstanceOf[ClassDecl])
  		Program(tmp)
	}

  	//class_declaration  : CLASS IDENTIFIER (EXTENDS IDENTIFIER)? LP class_body_declaration RP ;
	override def visitClass_declaration(ctx: BKOOLParser.Class_declarationContext) = {
  		var tmp: ClassDecl = null;
  		if(ctx.IDENTIFIER(1) != null)
  			tmp = ClassDecl(Id(ctx.IDENTIFIER(0).getText),Id(ctx.IDENTIFIER(1).getText), ctx.class_body_declaration.accept(this).asInstanceOf[List[MemDecl]])
  		else
  			tmp = ClassDecl(Id(ctx.IDENTIFIER(0).getText),null, ctx.class_body_declaration.accept(this).asInstanceOf[List[MemDecl]])
  		tmp
	}

	//class_body_declaration : (method_declaration | constant_declaration | variable_declaration | constructor_class | class_declaration)* ;
	override def visitClass_body_declaration(ctx: BKOOLParser.Class_body_declarationContext) = {
		val len = ctx.getChildCount()
    	var tmp: List[MemDecl] = List()
    	for (i <- 0 until len) tmp = tmp:::ctx.getChild(i).accept(this).asInstanceOf[List[MemDecl]]
    	tmp
    }

	//constructor_class : IDENTIFIER LB parameter_list? RB block_statement ;
	override def visitConstructor_class(ctx: BKOOLParser.Constructor_classContext) = {
		var tmp_list: List[(String,Type)] = List()
		var param_list: List[ParamDecl] = List()
		if(ctx.parameter_list != null){
			tmp_list = ctx.parameter_list().accept(this).asInstanceOf[List[(String,Type)]]
			param_list = tmp_list.map(x => ParamDecl(Id(x._1),x._2))
		}
		var block: Stmt = ctx.block_statement.accept(this).asInstanceOf[Stmt]
		List(MethodDecl(Instance,Id(ctx.IDENTIFIER.getText), param_list, null, block).asInstanceOf[MemDecl])
	}

	//constant_declaration : STATIC? FINAL type_declaration IDENTIFIER IS_EQUAL_TO expression SC ;
	override def visitConstant_declaration(ctx: BKOOLParser.Constant_declarationContext) = {
	    val tmp = ctx.type_declaration().accept(this).asInstanceOf[Type]
	    val decl = ConstDecl(Id(ctx.IDENTIFIER.getText),tmp, ctx.expression().accept(this).asInstanceOf[Expr])
  	    var mem_type :SIKind = {
	  	    if(ctx.STATIC != null)
	  	        Static
	  	    else
	  	    	Instance
  	    }
	    List(AttributeDecl(mem_type, decl))
	}

	//variable_declaration : STATIC? parameter SC ;
	override def visitVariable_declaration(ctx: BKOOLParser.Variable_declarationContext) = {
	    var mem_type: SIKind = {
	    if(ctx.STATIC != null)
	        Static
	    else
	    	Instance
	    }
	    val decl = ctx.parameter().accept(this).asInstanceOf[List[(String,Type)]]
	    val list_variable :List[AttributeDecl] = decl.map(x => AttributeDecl(mem_type, VarDecl(Id(x._1), x._2)))
	    list_variable
	}

	//method_declaration : type_declaration STATIC? IDENTIFIER LB parameter_list? RB  block_statement ;
	override def visitMethod_declaration(ctx: BKOOLParser.Method_declarationContext) = {
		var mem_type: SIKind = {
	    if(ctx.STATIC != null)
	        Static
	    else
	    	Instance
	    }
		val types =  ctx.type_declaration().accept(this).asInstanceOf[Type]
		var tmp_list: List[(String,Type)] = List()
    	var param_list: List[ParamDecl] = List()
    	if(ctx.parameter_list() != null){
    		tmp_list = ctx.parameter_list().accept(this).asInstanceOf[List[(String,Type)]]
	    	param_list = tmp_list.map(x => ParamDecl(Id(x._1), x._2))
    	}
		var block: Stmt = ctx.block_statement().accept(this).asInstanceOf[Stmt]
		List(MethodDecl(mem_type, Id(ctx.IDENTIFIER.getText), param_list,types, block).asInstanceOf[MemDecl]) // Method thieu tham so
 	}

 	/*parameter_list : parameter
	|	parameter_list SC parameter ;*/
	override def visitParameter_list(ctx: BKOOLParser.Parameter_listContext) = {
		var tmp: List[(String,Type)] = List()
		if(ctx.parameter_list() == null)
  	    	tmp = ctx.parameter().accept(this).asInstanceOf[List[(String,Type)]]
  		else
  	    	tmp = ctx.parameter_list().accept(this).asInstanceOf[List[(String,Type)]]:::ctx.parameter().accept(this).asInstanceOf[List[(String,Type)]]
		tmp
	}

	//parameter : identifier_list COLON type_declaration ;
	override def visitParameter(ctx: BKOOLParser.ParameterContext) = {
		var id_list: List[String] = ctx.identifier_list().accept(this).asInstanceOf[List[String]]
		var types = ctx.type_declaration().accept(this).asInstanceOf[Type]
		val parameter: List[(String,Type)] = id_list.map(x => (x, types))
		parameter
	}

	/*identifier_list :	IDENTIFIER
	|	identifier_list COMMA IDENTIFIER ;*/
	override def visitIdentifier_list(ctx: BKOOLParser.Identifier_listContext) = {
		var tmp: List[String] = List()
		if(ctx.identifier_list() == null)
	    	tmp = List(ctx.IDENTIFIER.getText)
		else
	    	tmp = ctx.identifier_list().accept(this).asInstanceOf[List[String]]:::List(ctx.IDENTIFIER.getText.asInstanceOf[String])
		tmp
	}

	/*type_declaration :	INT
		|	FLOAT
		|	BOOLEAN
		|	STRING
		|	VOID
		|	array
		|	class_type ;*/
	override def visitType_declaration(ctx: BKOOLParser.Type_declarationContext) = {
		var tmp: Type = null
		if(ctx.getChild(0).getText == "int")
	    	tmp = IntType;
		else if(ctx.getChild(0).getText == "float")
	    	tmp = FloatType;
		else if(ctx.getChild(0).getText == "string")
	    	tmp = StringType;
		else if(ctx.getChild(0).getText == "void")
	    	tmp = VoidType;
		else if(ctx.getChild(0).getText == "boolean")
	    	tmp = BoolType;
		else if(ctx.class_type() != null)
			tmp = ctx.class_type().accept(this).asInstanceOf[Type]
		else
			tmp = ctx.array().accept(this).asInstanceOf[Type]
	    tmp
	}

	override def visitElement_type(ctx :BKOOLParser.Element_typeContext) = {
		var tmp: Type = null
		if(ctx.getChild(0).getText == "int")
	    	tmp = IntType;
		else if(ctx.getChild(0).getText == "float")
	    	tmp = FloatType;
		else if(ctx.getChild(0).getText == "string")
			tmp = StringType;
		else if(ctx.getChild(0).getText == "boolean")
			tmp = BoolType;
		else if(ctx.getChild(0).getText == "void")
	    	tmp = VoidType;
	  	else
	    	tmp = ctx.class_type().accept(this).asInstanceOf[Type]
		tmp
	}

	//array: element_type LSB INTEGER_LITERAL RSB ;
	override def visitArray(ctx :BKOOLParser.ArrayContext) = {
	  	ArrayType(IntLiteral(ctx.INTEGER_LITERAL.getText.toInt), ctx.element_type().accept(this).asInstanceOf[Type])
	}

	//class_type: IDENTIFIER ;
	override def visitClass_type(ctx :BKOOLParser.Class_typeContext) = {
	    ClassType(ctx.IDENTIFIER.getText)
	}

	//block_statement : LP declaration_list statement_list RP ;
    override def visitBlock_statement(ctx :BKOOLParser.Block_statementContext) = {
		var tmp_decl: List[AttributeDecl] = ctx.declaration_list().accept(this).asInstanceOf[List[AttributeDecl]]   //Lay ket qua List AttributeDecl tu declaration_list
		var list_decl: List[Decl] = List()
		list_decl = tmp_decl.map(x => x.decl.asInstanceOf[Decl])
		var list_stmt: List[Stmt] = ctx.statement_list().accept(this).asInstanceOf[List[Stmt]]  //Lay ket qua List Stmt tu statement_list
		Block(list_decl, list_stmt)
	}

	//declaration_list : (constant_declaration | variable_declaration )* ;
	override def visitDeclaration_list(ctx :BKOOLParser.Declaration_listContext) = {
	    val len = ctx.getChildCount
	    var tmp_list: List[AttributeDecl] = List()
		if (len > 0){
	    for(i<-0 until len)
	    	tmp_list = tmp_list:::ctx.getChild(i).accept(this).asInstanceOf[List[AttributeDecl]]  //Tao ra List AttributeDecl tu cac VarDecl, ConstDecl
		}
		tmp_list
	}

	//statement_list : (statement)* ;
	override def visitStatement_list(ctx: BKOOLParser.Statement_listContext) = {
		val len = ctx.getChildCount()  // Dem so khai bao
		var temp_list:List[Stmt] = List()
		if(len > 0){
	    for(i<-0 until len){
	    	temp_list = temp_list:::ctx.getChild(i).accept(this).asInstanceOf[List[Stmt]]  //Tao ra List Stmt tu cac Node con Stmt/////////////
	    }
	  }
	  temp_list
	}

	//statement : assignment_statement | if_statement | for_statement | break_statement | continue_statement | return_statement | method_statement ;
	override def visitStatement(ctx: BKOOLParser.StatementContext) = {
		List(ctx.getChild(0).accept(this).asInstanceOf[Stmt])
	}

	//if_statement : IF expression THEN (statement | block_statement ) (ELSE (statement | block_statement ))? ;
	override def visitIf_statement(ctx: BKOOLParser.If_statementContext) = {
		var stmt1: List[Stmt] = List()
		var stmt2: Option[Stmt] = None
		var tmp: List[Stmt] = List()
		if(ctx.ELSE() != null){
	    	try{
	      		stmt1 = ctx.getChild(3).accept(this).asInstanceOf[List[Stmt]]
	    	}
			catch{
	    		case _: Throwable => {stmt1 = List(ctx.getChild(3).accept(this).asInstanceOf[Stmt])}
	    	}

	    	try{
	    		tmp = ctx.getChild(5).accept(this).asInstanceOf[List[Stmt]]  // Gan vao temp 1 List gom 1 phan tu Stmt
	    		stmt2 = Some(tmp(0))
	    	}
	    	catch{
	    		case _: Throwable => {
	        		tmp = List(ctx.getChild(5).accept(this).asInstanceOf[Stmt]);
    	    		stmt2 = Some(tmp(0))
    	    	}
	    	}
		}
		else
		{
	    	try{
	    		stmt1 = ctx.getChild(3).accept(this).asInstanceOf[List[Stmt]]
	    	}
	    	catch{
	    		case _: Throwable => {stmt1 = List(ctx.getChild(3).accept(this).asInstanceOf[Stmt])}
	    	}
		}

		If(ctx.expression().accept(this).asInstanceOf[Expr], stmt1(0), stmt2)
	}

	override def visitFor_statement(ctx :BKOOLParser.For_statementContext) = {
	  var stmt:List[Stmt] = List()
	  if(ctx.block_statement() != null)
	  {
	    stmt = List(ctx.getChild(7).accept(this).asInstanceOf[Stmt])
	  }
	  else
	    stmt = ctx.getChild(7).accept(this).asInstanceOf[List[Stmt]]

	  var compare:Boolean = {
	    if(ctx.getChild(4).getText.asInstanceOf[String] == "to")
	      true
	    else
	      false
	  }
	  //for_statement : (FOR IDENTIFIER ASSIGNMENT_OPERATOR expression (TO | DOWNTO) expression DO (statement | block_statement ))
	  var expr1:Expr = ctx.expression(0).accept(this).asInstanceOf[Expr]
	  var expr2:Expr = ctx.expression(1).accept(this).asInstanceOf[Expr]
	  For(ctx.IDENTIFIER.getText.asInstanceOf[String], expr1, compare, expr2, stmt(0))
	}

	override def visitBreak_statement(ctx :BKOOLParser.Break_statementContext) = {
	  Break
	}

	override def visitContinue_statement(ctx :BKOOLParser.Continue_statementContext) = {
	  Continue
	}

	//return_statement : RETURN expression SC ;
	override def visitReturn_statement(ctx :BKOOLParser.Return_statementContext) = {
		Return(ctx.expression().accept(this).asInstanceOf[Expr])
	}

	override def visitAssignment_statement(ctx :BKOOLParser.Assignment_statementContext) = {
	  //assignment_statement : lhs ASSIGNMENT_OPERATOR expression SC ;
	  var leftHandSide:LHS = ctx.lhs().accept(this).asInstanceOf[LHS]
	  Assign(leftHandSide, ctx.expression().accept(this).asInstanceOf[Expr])
	}

	override def visitMethod_statement(ctx :BKOOLParser.Method_statementContext) = {
	  //expression DOT IDENTIFIER (LB (expression (COMMA expression)*)? RB)? SC ;
	  val expr:Expr = ctx.expression(0).accept(this).asInstanceOf[Expr]
	  var list_expr:List[Expr] = List()
	  val len = ctx.expression().size
	  if(len > 1){
	    for(i <-1 until len) {
	    list_expr = list_expr:::List(ctx.expression(i).accept(this).asInstanceOf[Expr])
	   }
	  }
	  Call(expr, Id(ctx.IDENTIFIER.getText), list_expr)
	}

	override def visitLhs(ctx :BKOOLParser.LhsContext) = {
	  /*lhs : expression DOT IDENTIFIER
    | IDENTIFIER
    | expression LSB expression RSB;*/
	  var res:LHS = null
	  if(ctx.expression().size == 2)
	    res = ArrayCell(ctx.expression(0).accept(this).asInstanceOf[Expr], ctx.expression(1).accept(this).asInstanceOf[Expr])
	  else if(ctx.expression().size == 1)
	    res = FieldAccess(ctx.expression(0).accept(this).asInstanceOf[Expr], Id(ctx.IDENTIFIER.getText))
	  else
	    res = Id(ctx.IDENTIFIER.getText)
	  res
	}

  override def visitMember_access(ctx :BKOOLParser.Member_accessContext) = {
    var list_expr:List[Expr] = List()
	  val len = ctx.expression().size
	  var res:Expr = null
	  if(len > 1){
	    for(i <-1 until len) {
	    list_expr = list_expr:::List(ctx.expression(i).accept(this).asInstanceOf[Expr])
	   }
	   res = CallExpr(ctx.expression(0).accept(this).asInstanceOf[Expr], Id(ctx.IDENTIFIER.getText), list_expr)
	  }

    if(len == 1){
      if(ctx.LB() != null)
      {
        res = CallExpr(ctx.expression(0).accept(this).asInstanceOf[Expr], Id(ctx.IDENTIFIER.getText), list_expr)
      }
      else
        res = FieldAccess(ctx.expression(0).accept(this).asInstanceOf[Expr], Id(ctx.IDENTIFIER.getText))
    }
    res
  }

	override def visitTerm_expression(ctx : BKOOLParser.Term_expressionContext) = {
	  ctx.expression().accept(this).asInstanceOf[Expr]
	}

	override def visitNew_expression(ctx : BKOOLParser.New_expressionContext) = {
	  var list_expr:List[Expr] = List()
	  val len = ctx.expression().size
	  if(len > 0){
	    for(i <-0 until len) {
	    list_expr = list_expr:::List(ctx.expression(i).accept(this).asInstanceOf[Expr])
	   }
	  }
	  NewExpr(Id(ctx.IDENTIFIER.getText), list_expr)
	}

	override def visitUnary_expression(ctx :BKOOLParser.Unary_expressionContext) = {
	  UnaryOp(ctx.getChild(0).getText, ctx.expression().accept(this).asInstanceOf[Expr])
	}

	override def visitLogic_expression(ctx :BKOOLParser.Logic_expressionContext) = {
	  val expr1:Expr = ctx.expression(0).accept(this).asInstanceOf[Expr]
	  val expr2:Expr = ctx.expression(1).accept(this).asInstanceOf[Expr]
	  BinaryOp(ctx.getChild(1).getText, expr1, expr2)
	}

	override def visitNegative_expression(ctx :BKOOLParser.Negative_expressionContext) = {
	  UnaryOp(ctx.LOGICAL_NOT.getText, ctx.expression().accept(this).asInstanceOf[Expr])
	}

	override def visitLiteral_expression(ctx :BKOOLParser.Literal_expressionContext) = {

	  if(ctx.INTEGER_LITERAL() != null)
	    IntLiteral(ctx.INTEGER_LITERAL().getText.toInt)
	  else if(ctx.STRING_LITERAL() != null)
	    StringLiteral(ctx.STRING_LITERAL().getText)
	  else if(ctx.FLOAT_LITERAL() != null)
	    FloatLiteral(ctx.FLOAT_LITERAL.getText.toFloat)
	  else if(ctx.BOOLEAN_LITERAL != null)
	    BooleanLiteral(ctx.BOOLEAN_LITERAL().getText.toBoolean)
	  else if(ctx.IDENTIFIER() != null)
	    Id(ctx.IDENTIFIER().getText)
	  else
	    NullLiteral

	}

override def visitIndex_operator(ctx :BKOOLParser.Index_operatorContext) = {
	  ArrayCell(ctx.expression(0).accept(this).asInstanceOf[Expr], ctx.expression(1).accept(this).asInstanceOf[Expr])
	}

	override def visitBinary_expression(ctx :BKOOLParser.Binary_expressionContext) = {
	  BinaryOp(ctx.getChild(1).getText, ctx.getChild(0).accept(this).asInstanceOf[Expr], ctx.getChild(2).accept(this).asInstanceOf[Expr])
	}

	override def visitArithmetic_expression(ctx :BKOOLParser.Arithmetic_expressionContext) = {
	  BinaryOp(ctx.getChild(1).getText, ctx.getChild(0).accept(this).asInstanceOf[Expr], ctx.getChild(2).accept(this).asInstanceOf[Expr])
	}

	override def visitSelf_expression(ctx :BKOOLParser.Self_expressionContext) = {
	  SelfLiteral
	}

	override def visitRelational_expression(ctx :BKOOLParser.Relational_expressionContext) = {
	  BinaryOp(ctx.getChild(1).getText, ctx.getChild(0).accept(this).asInstanceOf[Expr], ctx.getChild(2).accept(this).asInstanceOf[Expr])
  }

	override def visitString_expression(ctx :BKOOLParser.String_expressionContext) = {
	  BinaryOp(ctx.getChild(1).getText, ctx.getChild(0).accept(this).asInstanceOf[Expr], ctx.getChild(2).accept(this).asInstanceOf[Expr])
	}

	override def visitCondition_expression(ctx :BKOOLParser.Condition_expressionContext) = {
	  BinaryOp(ctx.getChild(1).getText, ctx.getChild(0).accept(this).asInstanceOf[Expr], ctx.getChild(2).accept(this).asInstanceOf[Expr])
  }

  override def visitTerminal(node:TerminalNode) = node.getText

}
