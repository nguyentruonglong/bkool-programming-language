cd ..
if [ ! -d ./bin ]; then 
	mkdir ./bin
fi
echo Scala compile
scalac -d ./bin -cp ./bin:/home/dragon/Desktop/PPLAssignment/script/antlr-4.5-complete.jar -unchecked  ./src/bkool/*.scala ./src/grammar/target/generated-sources/antlr4/*.java ./src/bkool/parser/*.scala ./src/bkool/utils/*.scala ./src/bkool/astgen/*.scala ./src/bkool/checker/*.scala ./src/bkool/codegen/*.scala
cd ./src/grammar
echo Java compile
javac -d ../../bin -cp /home/dragon/Desktop/PPLAssignment/script/scala-library.jar:/home/dragon/Desktop/PPLAssignment/script/antlr-4.5-complete.jar:../../bin:../../bin/bkool/parser -Xlint:deprecation ./target/generated-sources/antlr4/*.java
cd ../../script
